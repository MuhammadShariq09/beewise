/*
SQLyog Ultimate v9.20 
MySQL - 5.5.5-10.1.36-MariaDB : Database - beewise_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`beewise_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `coupens` */

DROP TABLE IF EXISTS `coupens`;

CREATE TABLE `coupens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0 for Percentage, 1 for Fixed Amount',
  `discount_amount` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `uses` int(10) unsigned DEFAULT NULL,
  `max_uses` int(10) unsigned DEFAULT NULL,
  `max_uses_user` int(10) unsigned DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `valid_from_date` timestamp NULL DEFAULT NULL,
  `valid_to_date` timestamp NULL DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0 for In Active, 1 for Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupens_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `coupens` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_04_25_141111_create_quotations_table',1),(4,'2019_04_26_091928_create_quotation_images_table',2),(5,'2019_05_20_010520_create_orders_table',3),(7,'2019_05_20_054404_create_order_details_table',4),(8,'2019_05_22_054129_create_vouchers_table',4),(11,'2019_05_23_005757_create_coupens_table',5),(12,'2019_05_23_011246_create_user_coupens_table',5);

/*Table structure for table `order_details` */

DROP TABLE IF EXISTS `order_details`;

CREATE TABLE `order_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `item` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `price` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_details_order_id_foreign` (`order_id`),
  CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `order_details` */

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_id` bigint(20) unsigned NOT NULL,
  `total` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0 for Pending, 1 for Paid, 2 for Cancelled',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_quotation_id_foreign` (`quotation_id`),
  CONSTRAINT `orders_quotation_id_foreign` FOREIGN KEY (`quotation_id`) REFERENCES `quotations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `orders` */

insert  into `orders`(`id`,`quotation_id`,`total`,`status`,`created_at`,`updated_at`) values (1,39,'$800','0','2019-05-20 06:30:58','2019-05-22 01:49:56'),(2,38,'$500','0','2019-05-22 01:51:29','2019-05-22 01:52:13');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `quotation_images` */

DROP TABLE IF EXISTS `quotation_images`;

CREATE TABLE `quotation_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_id` bigint(20) unsigned NOT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quotation_images_quotation_id_foreign` (`quotation_id`),
  CONSTRAINT `quotation_images_quotation_id_foreign` FOREIGN KEY (`quotation_id`) REFERENCES `quotations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `quotation_images` */

insert  into `quotation_images`(`id`,`quotation_id`,`image`,`created_at`,`updated_at`) values (1,38,'uploads/quotations/1556797572-1.png','2019-05-02 05:46:12','2019-05-02 05:46:12'),(2,38,'uploads/quotations/1556797572-2.png','2019-05-02 05:46:12','2019-05-02 05:46:12'),(3,38,'uploads/quotations/1556797572-3.png','2019-05-02 05:46:12','2019-05-02 05:46:12'),(4,39,'uploads/quotations/1558335368-1.png','2019-05-20 00:56:08','2019-05-20 00:56:08'),(5,39,'uploads/quotations/1558335368-2.png','2019-05-20 00:56:08','2019-05-20 00:56:08'),(6,39,'uploads/quotations/1558335368-3.png','2019-05-20 00:56:08','2019-05-20 00:56:08');

/*Table structure for table `quotations` */

DROP TABLE IF EXISTS `quotations`;

CREATE TABLE `quotations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `address_from` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `city_from` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `country_from` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `zipcode_from` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `latitude_from` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `longitude_from` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `property_type_from` enum('0','1','2','3','4') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '0 for House, 1 for Cottage, 2 for Bunglow, 3 for Mansion, 4 for Appartment',
  `floors_from` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `rooms_from` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `address_to` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `city_to` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `country_to` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `zipcode_to` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `latitude_to` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `longitude_to` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `property_type_to` enum('0','1','2','3','4') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '0 for House, 1 for Cottage, 2 for Bunglow, 3 for Mansion, 4 for Appartment',
  `floors_to` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `rooms_to` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `moving_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `packaging_items` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `fragile_items` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quotations_user_id_foreign` (`user_id`),
  CONSTRAINT `quotations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `quotations` */

insert  into `quotations`(`id`,`user_id`,`address_from`,`city_from`,`country_from`,`zipcode_from`,`latitude_from`,`longitude_from`,`property_type_from`,`floors_from`,`rooms_from`,`address_to`,`city_to`,`country_to`,`zipcode_to`,`latitude_to`,`longitude_to`,`property_type_to`,`floors_to`,`rooms_to`,`moving_date`,`packaging_items`,`fragile_items`,`note`,`created_at`,`updated_at`) values (38,15,'7894 Washington Boulevard, Elkridge, MD, USA','Elkridge','United States','21075','39.17254599999999','-76.784762','2','3','5','7900 North Milwaukee Avenue, Niles, IL, USA','Niles','United States','60714','42.0246335','-87.8146357',NULL,'10','4','2019-05-12 05:46:12','20','50',NULL,'2019-05-02 05:46:12','2019-05-02 05:46:12'),(39,17,'987, Brooklyn Road','NY','United States','64564','118.054345564','-78.9797897','2','3','5','5601 Collins Avenue, Miami Beach, FL, USA','Miami Beach','United States','33140','25.8368729','-80.1203074','0','0','6','2019-05-22 14:30:00','50','20',NULL,'2019-05-20 00:56:08','2019-05-20 00:56:08');

/*Table structure for table `user_coupens` */

DROP TABLE IF EXISTS `user_coupens`;

CREATE TABLE `user_coupens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `coupen_id` bigint(20) unsigned NOT NULL,
  `redeemed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_coupens_user_id_foreign` (`user_id`),
  KEY `user_coupens_coupen_id_foreign` (`coupen_id`),
  CONSTRAINT `user_coupens_coupen_id_foreign` FOREIGN KEY (`coupen_id`) REFERENCES `coupens` (`id`),
  CONSTRAINT `user_coupens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_coupens` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0 for normal user, 1 for admin',
  `is_active` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0 for Inactive, 1 for active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`image`,`email`,`password`,`phone`,`address`,`latitude`,`longitude`,`country`,`state`,`city`,`postal_code`,`email_verified_at`,`remember_token`,`is_admin`,`is_active`,`created_at`,`updated_at`) values (1,'Muhammad','Sohaib',NULL,'abdul.fahim@salsoft.net','$2y$10$ah9Y6KB0.JkeS3C.JdQLJeljaLOKALMOlp2gP7P3b6I5GvTK2sSzS','(808) 789-5896','Newyork',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2019-04-26 07:58:04','2019-04-26 07:58:04'),(15,'Fahim','Siddiqui',NULL,'fsiddiqui.dev1@gmail.com','$2y$10$mPQ5PgMOcBnqPLbBn45s7u4llg1BksOWXw3zrh9Hp4ZBD9ZAx9yeu','(808) 789-5896','7900 North Milwaukee Avenue, Niles, IL, USA','42.0246335','-87.8146357','United States','IL','Niles','60714',NULL,NULL,'0','1','2019-05-02 05:32:50','2019-05-02 05:32:50'),(17,'Fahim','Siddiqui',NULL,'fsiddiqui.dev@gmail.com','$2y$10$XDxXXVafTPV0.0PnX6CrYe4TpxmkeNz4CUSFkIebXGDZcs.bQBu7i','(808) 789-5896','7900 North Milwaukee Avenue, Niles, IL, USA','42.0246335','-87.8146357','United States','IL','Niles','60714',NULL,NULL,'0','1','2019-05-02 07:57:33','2019-05-02 07:57:33');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
