<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('inspection_id');
            $table->string('total', 10);
            $table->enum('status', array('0', '1', '2'))->default('0')->comment = "0 for Pending, 1 for active, 2 for Completed";
            $table->enum('is_paid', array('0', '1'))->default('0')->comment = "0 for un paid, 1 for Paid";
            $table->timestamps();
            $table->foreign('inspection_id')->references('id')->on('inspection_payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
