<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('image', 500);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone',25);
            $table->string('address',500);
            $table->string('latitude',500);
            $table->string('longitude',500);
            $table->string('country',100);
            $table->string('state',100);
            $table->string('city',200);
            $table->string('postal_code',50);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->enum('is_admin', array('0', '1'))->default('0')->comment = "0 for normal user, 1 for admin";
            $table->enum('is_active', array('0', '1'))->default('1')->comment = "0 for Inactive, 1 for active";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
