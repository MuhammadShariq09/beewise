<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoupensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 32)->unique();
            $table->enum('type', array('0', '1'))->default('0')->comment = "0 for Percentage, 1 for Fixed Amount";
            $table->string('discount_amount', 10);
            $table->text('description')->nullable();
            $table->integer('uses')->unsigned()->nullable();
            $table->integer('max_uses')->unsigned()->nullable();
            $table->integer('max_uses_user')->unsigned()->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('valid_from_date')->nullable();
            $table->timestamp('valid_to_date')->nullable();
            $table->enum('status', array('0', '1'))->default('0')->comment = "0 for In Active, 1 for Active";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupens');
    }
}
