<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectionRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invoice_order_id');
            $table->string('date', 200);
            $table->string('time', 200);
            $table->string('description', 200);
            $table->enum('status', array('0', '1'))->default('0')->comment = "0 for pending, 1 for Approved";
            $table->timestamps();
            $table->foreign('invoice_order_id')->references('id')->on('invoice_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspection_requests');
    }
}
