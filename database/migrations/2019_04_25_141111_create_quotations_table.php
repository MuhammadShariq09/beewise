<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('address_from', 500)->default('none');
            $table->string('city_from', 100)->default('none');
            $table->string('country_from', 100)->default('none');
            $table->string('zipcode_from', 50)->default('none');
            $table->string('latitude_from', 100)->default('none');
            $table->string('longitude_from', 100)->default('none');
            $table->enum('property_type_from', array('0', '1','2','3','4'))->comment = "0 for House, 1 for Cottage, 2 for Bunglow, 3 for Mansion, 4 for Appartment";
            $table->string('floors_from', 50)->default('none');
            $table->string('rooms_from', 50)->default('none');
            $table->string('address_to', 500)->default('none');
            $table->string('city_to', 100)->default('none');
            $table->string('country_to', 100)->default('none');
            $table->string('zipcode_to', 50)->default('none');
            $table->string('latitude_to', 100)->default('none');
            $table->string('longitude_to', 100)->default('none');
            $table->enum('property_type_to', array('0', '1','2','3','4'))->comment = "0 for House, 1 for Cottage, 2 for Bunglow, 3 for Mansion, 4 for Appartment";
            $table->string('floors_to', 50)->default('none');
            $table->string('rooms_to', 50)->default('none');
            $table->string('moving_date', 100)->default('none');
            $table->string('packaging_items', 50)->default('none');
            $table->string('fragile_items', 50)->default('none');
            $table->longText('note');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
