@extends('layouts.master')
@section('title', env('APP_NAME').' - Edit Coupon')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns") 


@section('content')  
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body"> 
                    <section id="configuration" class="u-c-p categorie create-job">
                        <div class="row profile-frm">
                            <div class="col-12">
                                <div class="card pro-main">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success">
                                            <strong>{{ Session::get('message')  }}</strong>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Edit Coupon</h1>
                                        </div>
                                    </div>
                                    <form  action="{{ route('update-coupon') }}" enctype="multipart/form-data" method="post" novalidate class="form">
                                    @csrf
                                        <div class="create-job-inner">
                                            <div class="row">
                                                <div class="col-lg-7 col-md-12 col-sm-12">
                                                    <input type="hidden" name="coupon_id" id="coupon_id" value="{{ $coupon->id }}">

                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Title</label>
                                                        <input type="text" name="title" id="title" class="form-control" value="{{ $coupon->title }}">
                                                        @if ($errors->has('title'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('title') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Description</label>
                                                        <textarea class="form-control" id="description" name="description" placeholder="Coupon Description">{{ $coupon->description }}</textarea>
                                                        @if ($errors->has('description'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('description') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Code</label>
                                                        <input type="text" name="code" id="code" class="form-control" value="{{ $coupon->code }}">
                                                        @if ($errors->has('code'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('code') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Discount Type</label>
                                                        <select class="form-control" name="type" id="type">
                                                            <option {{ $coupon->type == 0 ? "selected" : "" }}  value="0">Percentage (%)</option>
                                                            <option {{ $coupon->type == 1 ? "selected" : "" }}  value="1">Fixed</option>
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Discount Amount</label>
                                                        <input type="text" name="discount_amount" id="discount_amount" class="form-control" value="{{ $coupon->discount_amount }}">
                                                        @if ($errors->has('discount_amount'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('discount_amount') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Coupon Status</label>
                                                        <select class="form-control" name="status" id="status">
                                                            <option {{ $coupon->status == 0 ? "selected" : "" }}  value="0">In Active</option>
                                                            <option {{ $coupon->status == 1 ? "selected" : "" }}  value="1">Active</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Max. Usage</label>
                                                        <input type="text" name="max_uses" id="max_uses" class="form-control" value="{{ $coupon->max_uses }}">
                                                        @if ($errors->has('max_uses'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('max_uses') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Start Date</label>
                                                        <div class="input-group date" id="startDate">
                                                            <input type="text" name="start_date" id="start_date" class="form-control" value="{{ $coupon->start_date }}">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        @if ($errors->has('start_date'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('start_date') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> End Date</label>
                                                        <div class="input-group date" id="endDate">
                                                            <input type="text" name="end_date" id="end_date" class="form-control" value="{{ $coupon->end_date }}">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        @if ($errors->has('end_date'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('end_date') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>


                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <button type="submit" class="save"> Update Coupon</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>


@endsection('content')  

@section('js')
<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>

<script src="{{ asset('assets/admin/app-assets/js/scripts/pickers/dateTime/bootstrap-datetime.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

   <script src="{{ asset('assets/admin/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.min.js') }}"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css"/>
    
<script>
    $(document).ready(function () {    
        $("#code").prop("readonly", true);            
        $('#startDate, #endDate').datetimepicker({format: 'YYYY-MM-DD'});
        
        $('#endDate').datetimepicker({
          useCurrent: false //Important! See issue #1075
        });
        
        $("#startDate").on("dp.change", function (e) {
          $('#endDate').data("DateTimePicker").minDate(e.date);
        });      
        
        /*$("#endDate").on("dp.change", function (e) {
            $('#startDate').data("DateTimePicker").maxDate(e.date);
        });*/
    });
</script> 

@endsection