
@extends('layouts.master')

@section('title',env('APP_NAME').' - Admin Dashboard')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-body"> 
      <!-- Basic form layout section start -->
      <section id="combination-charts">
        <div class="row">
          <div class="col-12">
            <div class="card pro-main rounded overview">
              <div class="row">
                <div class="col-12">
                  <h1>Overview</h1>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-sm-3">
                  <div class="overview-card clearfix p1">
                    <h5>posted<br>
                      jobs</h5>
                    <img src="images/bag.png" class="img-full" alt="bag"> </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="overview-card clearfix p2">
                    <h5>total <br>
                      users</h5>
                    <img src="images/users.png" class="img-full" alt="lock"> </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="overview-card clearfix p3">
                    <h5>Available <br>
                      Jobs </h5>
                    <img src="images/lcd.png" class="img-full" alt="img"> </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="overview-card clearfix p4">
                    <h5>Closed <br>
                      Jobs</h5>
                    <img src="images/lock.png" class="img-full" alt="lock"> </div>
                </div>
              </div>
              <div class="overview-bottom">
              <div class="row">
                <div class="col-lg-9">
                  <h3>Registration user per month</h3>
                  <div class="card-content collapse show">
                    
                      <div id="column-line3" class="height-400 echart-container"></div>
                     
                  </div> 
                </div>
                <!--col left end-->
                
                <div class="col-lg-3"> 
                  
                  <!--col left end-->
                  
                  <div class="card-content card-case-2">
                    <h2 class="right-h2">Recently Register Users</h2>
                    <div class="overview-card clearfix"> <img src="images/Profile_03.png" alt="" />
                      <h5>Jason Wright <br>
                        <span>Technical Engineer</span> </h5>
                    </div>
                  </div>
                   <div class="card-content card-case-2"> 
                    <div class="overview-card clearfix"> <img src="images/Profile_03.png" alt="" />
                      <h5>Jason Wright <br>
                        <span>Technical Engineer</span> </h5>
                    </div>
                  </div>
                  <div class="card-content card-case-2"> 
                    <div class="overview-card clearfix"> <img src="images/Profile_03.png" alt="" />
                      <h5>Jason Wright <br>
                        <span>Technical Engineer</span> </h5>
                    </div>
                  </div>
                  <div class="card-content card-case-2"> 
                    <div class="overview-card clearfix"> <img src="images/Profile_03.png" alt="" />
                      <h5>Jason Wright <br>
                        <span>Technical Engineer</span> </h5>
                    </div>
                  </div>
                  
                  
                  
                </div>
                </div>
                <!--row end--> 
                
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection('content')

@section('js')

<script type="text/javascript">
 /*require.config({
        paths: {
            echarts: 'app-assets/vendors/js/charts/echarts'
        }
    });


require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],


        // Charts setup
        function (ec) {

            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('basic-column'));

            // Chart Options
            // ------------------------------
            chartOptions = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 40,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['Featured', 'Sold','Trending']
                },

                // Add custom colors
                color: ['#FF7588','#00B5B8', '#556AF3'],

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],


                // Add series
                series: [
                    {
                        name: 'Featured',
                        type: 'bar',
                        data: [],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    },
                    {
                        name: 'Sold',
                        type: 'bar',
                        data: [],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    },
                    {
                        name: 'Trending',
                        type: 'bar',
                        data: [],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    }
                ]
            };

            // Apply options
            // ------------------------------

            myChart.setOption(chartOptions);


            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChart.resize();
                    }, 200);
                }
            });
        }
    );*/
    
/*=========================================================================================
    File Name: column.js
    Description: Chartjs column chart
    ----------------------------------------------------------------------------------------
    Item Name: Stack - Responsive Admin Theme
    Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Column chart
// ------------------------------
/*$(window).on("load", function(){

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#column-chart");

    // Chart Options
    var chartOptions = {
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each bar to be 2px wide and green
        elements: {
            rectangle: {
                borderWidth: 2,
                borderColor: 'rgb(0, 255, 0)',
                borderSkipped: 'bottom'
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration:500,
        legend: {
            position: 'top',
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                }
            }]
        }
    };

    // Chart Data
    var chartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
        datasets: [{
            label: "Featured Properties",
            data: [],
            backgroundColor: "#873D6B",
            hoverBackgroundColor: "rgba(135,61,107,.9)",
            borderColor: "transparent"
        }, {
            label: "Sold Properties",
            data: [],
            backgroundColor: "#13184C",
            hoverBackgroundColor: "rgba(19,24,76,.9)",
            borderColor: "transparent"
        },
        {
            label: "Trending Properties",
            data: [],
            backgroundColor: "#D8A439",
            hoverBackgroundColor: "rgba(216,164,57,.9)",
            borderColor: "transparent"
        }]
    };

    var config = {
        type: 'bar',

        // Chart Options
        options : chartOptions,

        data : chartData
    };

    // Create the chart
    var lineChart = new Chart(ctx, config);
});*/
	</script>
  <!-- END STACK JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <!-- END PAGE LEVEL JS-->

@endsection('js')