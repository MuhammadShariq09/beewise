<div class="payment-modal-inner">
        <div class="row">
            <div class="col-12 text-center mb-4">
            <h1>User profile</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="attached">
                <img src="@if($user->image != ''){{ asset($user->image) }} @else {{ asset('assets/admin/images/Profile_03.png') }} @endif" class="img-full" alt="">
                <!--  <button name="file" class="change-cover" onclick="document.getElementById('upload').click()">
                    <div class="ca"><i class="fa fa-camera"></i></div>
                    </button>-->
                <input type="file" id="upload" name="file">
            </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 col-lg-3">
            <label for="">
                Name
                <div class="text-danger">*</div>
            </label>
            </div>
            <div class="col-lg-8 col-12">
            <p>{{ $user->name }}</p>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 col-lg-3">
            <label for="">
                phone
                <div class="text-danger">*</div>
            </label>
            </div>
            <div class="col-lg-8 col-12">
            <p>{{ $user->phone }}</p>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 col-lg-3">
            <label for="">Address </label>
            </div>
            <div class="col-lg-8 col-12">
            <p>{{ $user->address }}</p>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 col-lg-3">
            <label for="">city </label>
            </div>
            <div class="col-lg-8 col-12">
            <p>{{ $user->city }}</p>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 col-lg-3">
            <label for="">postcode </label>
            </div>
            <div class="col-lg-8 col-12">
            <p>{{ $user->postal_code }}</p>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 col-lg-3">
            <label for="">
                E-mail
                <div class="text-danger">*</div>
            </label>
            </div>
            <div class="col-lg-8 col-12">
            <p>{{ $user->email }}</p>
            </div>
        </div>
    </div>
