@extends('layouts.master')
@section('title',env('APP_NAME').' - Add User')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('body-col',"2-columns")


@section('css')
<style>
    label i.fa {
        padding-right: 8px;
    }
</style>
@endsection
@section('content')
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="">
        <div class="content-body">
          <!-- Basic form layout section start -->
          <section id="configuration" class="u-c-p u-profile ad-user">
            <div class="row">
              <div class="col-12">
                <div class="card pro-main">
                  @if(Session::has('message'))
                      <div class="alert alert-success">
                          <strong>{{ Session::get('message')  }}</strong>
                      </div>
                  @endif
                  @if(Session::has('error'))
                      <div class="alert alert-danger">
                        <strong>{{ Session::get('error')  }}</strong>
                      </div>
                  @endif
                  <div class="row">
                    <div class="col-12">
                      <h1>Add user </h1>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-9 col-md-12 col-12">
                      <div class="profile-frm">
                        <form action="{{ route('submit-user') }}" enctype="multipart/form-data" method="post" novalidate class="form">
                        @csrf
                            <div class="row">
                                <div class="col-12">

                                    <div class="attached">
                                        <img src="{{ asset('/assets/admin/images/Profile_03.png') }}" id="user_image" alt="">
                                        <!--<button name="file" class="change-cover" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>-->
                                        <button type="button" class="change-cover" name="file" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i>
                                        </button>
                                        <input type="file"  accept="image/*" required onchange="readURL(this, 'user_image')" id="upload" name="user_image" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--col end-->
                            <div class="row">
                            <div class="col-12">
                              <div class="row">
                                <div class="col-md-12">
                                  <h2>Personal Detials</h2>
                                </div>
                              </div>
                              <div class="cat-text">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txt">
                                      <label><i class="fa fa-user-circle"></i>First Name</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    <input type="text"  value="{{ old('first_name') }}" id="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="" name="first_name">
                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                                </div>
                              </div>
                              <div class="cat-text">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txt">
                                      <label><i class="fa fa-user-circle"></i>Last Name</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    <input type="text"  value="{{ old('last_name') }}" id="last_name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="" name="last_name">
                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                                </div>
                              </div>


                              <div class="cat-text">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txt">
                                      <label><i class="fa fa-envelope"></i> Email Address</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    <input type="email"  value="{{ old('email') }}" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="" name="email">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="cat-text">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                        <div class="txt">
                                            <label><i class="fa fa-phone"></i> Phone</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                        <input type="text"  value="{{ old('phone') }}" id="phone" class="phone form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="" name="phone"  maxlength="14">
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                              <div class="row">
                                <div class="col-md-12">
                                  <h2>Contact Details</h2>
                                </div>
                              </div>
                              <!--pro inner row end-->

                              <!--pro inner row end-->
                              <div class="cat-text">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txt">
                                      <label><i class="fa fa-map-marker"></i> Address</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    <input type="text"  value="{{ old('address') }}" id="autocomplete" onfocus="geolocate()" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="Enter your address" name="address">
                                    <input type="hidden" id="latitude" class="form-control" name="latitude"  value="{{ old('latitude') }}">
                                    <input type="hidden" id="longitude" class="form-control" name="longitude" value="{{ old('longitude') }}">
                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                                </div>
                              </div>
                              <!--pro inner row end-->
                              <div class="cat-text">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txt">
                                      <label><i class="fa fa-map-marker"></i> Country</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    <input type="text"  value="{{ old('country') }}" id="country" class="form-control" placeholder="" name="country">
                                  </div>
                                </div>
                              </div>
                              <!--pro inner row end-->
                              <div class="cat-text">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txt">
                                      <label><i class="fa fa-map-marker"></i> City</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    <input type="text"  value="{{ old('city') }}" id="locality" class="form-control" placeholder="" name="city">
                                  </div>
                                </div>
                              </div>
                              <!--pro inner row end-->
                              <div class="cat-text">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txt">
                                      <label><i class="fa fa-map-marker"></i> State</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    <input type="text"  value="{{ old('state') }}" id="administrative_area_level_1" class="form-control" placeholder="" name="state">
                                  </div>
                                </div>
                              </div>
                              <!--pro inner row end-->
                              <div class="cat-text">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txt">
                                      <label><i class="fa fa-map-marker"></i> Zip Code</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    <input type="text"  value="{{ old('postal_code') }}" id="postal_code" class="form-control" placeholder="" name="postal_code">

                                    <button type="submit" class="save form-control">Add User</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('js')

<script type="text/javascript">

    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+target).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    function phoneFormatter() {
      $('.phone').on('input', function() {
        var number = $(this).val().replace(/[^\d]/g, '')
        if (number.length == 7) {
          number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
        } else if (number.length == 10) {
          number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        }
        $(this).val(number)
      });
    };

    $(phoneFormatter);

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        /*locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'*/

        /*street_number: 'short_name',
        route: 'long_name',*/
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.setComponentRestrictions(
            {'country': ['us']});
        autocomplete.addListener('place_changed', fillInAddress);
        /*
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
        });*/
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        console.log(place.geometry.location.lat());
        console.log(place.geometry.location.lng());
        document.getElementById("latitude").value = place.geometry.location.lat();
        document.getElementById("longitude").value = place.geometry.location.lng();
        for (var component in componentForm) {
            try{
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }catch(e){

            }
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            console.log(addressType)
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
@endsection
