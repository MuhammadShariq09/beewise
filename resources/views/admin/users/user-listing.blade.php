@extends('layouts.master')
@section('title',env('APP_NAME').' - Users Listing')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')
<div class="app-content content view user">
	<div class="content-wrapper">
		<div class="content-body">
			<!-- Basic form layout section start -->
			<section id="configuration">
				<div class="row">
					<div class="col-12">
						<div class="card rounded pad-20">
							<div class="card-content collapse show">
								<div class="card-body table-responsive card-dashboard">
									<div class="row">
										<div class="col-12">
						                    @if(Session::has('message'))
						                        <div class="alert alert-success">
						                            <strong>{{ Session::get('message')  }}</strong>
						                        </div>
						                    @endif
						                    @if(Session::has('error'))
						                        <div class="alert alert-danger">
						                          <strong>{{ Session::get('error')  }}</strong>
						                        </div>
						                    @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <h1 class="pull-left">users</h1>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 d-block d-sm-flex justify-content-between">
                                            <div class="left">
                                                <h3>Date</h3>
                                                @php
                                                    $todayDate = date("l, j F Y");
                                                @endphp
                                                <h2>{{$todayDate}}</h2>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#addUserModal"><i class="fa fa-plus-circle" ></i> Add New User</a> </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-6">
                                        <h1>Users</h1>
                                    </div>
                                    <div class="col-6">
                                    <div class="row">
                                    	<div class="col-12"><a href="{{ route('add-user')}}" class="green-btn-project"><i class="fa fa-plus-circle"></i>Create User</a></div>
                                    </div>



                                    </div>
                                </div>
									<div class="clearfix"></div> --}}
									<div class="maain-tabble">
										<table class="table table-striped table-bordered zero-configuration">
											<thead>
												<tr>
													<th>ID</th>
													<th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone Number</th>
                                                    <th style="width:13%;">Status</th>
                                                    <th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0;?>
												@foreach($users as $user)
                        						<?php $i++ ?>
												<tr id="row{{$user->id}}">
                                                	<td>{{$i}}</td>
													<td>
														@if($user->image == "")
														<span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{ucfirst(substr($user->name,0,1))}}</span>
														@else
														<span class="avatar avatar-online">
                                        					<img src="{{ asset($user->image) }}" alt="{{ucfirst($user->name)}}">
                               							</span>
                               							@endif
														{{ucfirst($user->name)}}
													</td>
													<td>{{ $user->email}}</td>
													<td>{{ $user->phone }}</td>
													<td>
														<select name="status" class="form-control statusButton"  data-id="{{$user->id}}" >
						                                    <option value="0" {{ $user->is_active == 0 ? 'selected' : '' }}>In-active</option>
						                                    <option value="1" {{ $user->is_active == 1 ? 'selected' : '' }}>Active</option>
						                                </select>
													</td>
                                                    <td>
						                                {{-- <div class="btn-group mr-1 mb-1">
						                                <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
						                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">

						                                  <a class="dropdown-item" href="{{ route('edit-user', ['id' => $user->id])}}"><i class="fa fa-pencil-square-o"></i>Edit</a>
						                                  <!--<a class="dropdown-item delButton"  data-id="{{$user->id}}" href="javascript:void(0)"><i class="fa fa-trash"></i>Delete</a>-->
						                                </div>
                                                      </div> --}}

                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                    <a class="dropdown-item userinfo" data-id="{{ $user->id }}" href="javascript:void(0);" data-target="#viewUserModal"><i class="fa fa-eye"></i>View </a>
                                                                    <a class="dropdown-item delButton"  data-id="{{$user->id}}" href="javascript:void(0)"><i class="fa fa-trash"></i>Delete</a>
                                                                </div>
                                                          </div>
						                            </td>

												</tr>
  												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- // Basic form layout section end -->
		</div>
	</div>
</div>

<!-- Start Add User Modal -->
<div class="login-fail-main user">
    <div class="featured inner">
        <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div class="alert alert-success" style="display:none"></div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="add-user-form" action="{{ route('submit-user') }}" method="post" novalidate class="form" enctype="multipart/form-data">
                    @csrf
                    <div class="payment-modal-main">
                        <div class="payment-modal-inner">
                            <div class="row">
                            <div class="col-12 text-center mb-4">
                                <h1>Add New User</h1>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="attached">
                                        <img src="{{ asset('/assets/admin/images/Profile_03.png') }}" id="user_image"  class="img-full" alt="">
                                        <button type="button" name="file" class="change-cover" onclick="document.getElementById('upload').click()">
                                            <div class="ca"><i class="fa fa-camera"></i></div>
                                        </button>
                                        <input type="file" accept="image/*" onchange="readURL(this, 'user_image')" id="upload" name="user_image" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 col-lg-3">
                                    <label>Name<div class="text-danger">*</div></label>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <input type="text" value="{{ old('name') }}" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Enter Full Name" name="name">
                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12 col-lg-3">
                                    <label><div class="text-danger">*</div> Email Address</label>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <input type="email"  value="{{ old('email') }}" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter Email" name="email">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 col-lg-3">
                                    <label><div class="text-danger">*</div> Phone</label>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <input type="text"  value="{{ old('phone') }}" id="phone" class="phone form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="" name="phone"  maxlength="14">
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 col-lg-3">
                                    <label for=""><div class="text-danger">*</div> Address </label>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <input type="text"  value="{{ old('address') }}" id="autocomplete" onfocus="geolocate()" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="Enter your address" name="address">
                                    <input type="hidden" id="latitude" class="form-control" name="latitude"  value="{{ old('latitude') }}">
                                    <input type="hidden" id="longitude" class="form-control" name="longitude" value="{{ old('longitude') }}">
                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 col-lg-3">
                                    <label>Country</label>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <input type="text" value="{{ old('country') }}" id="country" class="form-control" placeholder="" name="country" />
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 col-lg-3">
                                    <label>City</label>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <input type="text" value="{{ old('city') }}" id="locality" class="form-control" placeholder="" name="city" />
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 col-lg-3">
                                    <label>State</label>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <input type="text" value="{{ old('state') }}" id="administrative_area_level_1" class="form-control" placeholder="" name="state" />
                                </div>
                            </div>


                            <div class="row form-group">
                                <div class="col-md-12 col-lg-3">
                                    <label for="">Postal Code </label>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <input type="text"  value="{{ old('postal_code') }}" id="postal_code" class="form-control" placeholder="" name="postal_code">
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-12 text-center">
                                <button type="submit" id="ajaxSubmit" class="con">Add User</button>
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Add User Modal -->

<!-- Load User View Modal -->
<div class="login-fail-main user" id="load-view-modal">
        <div class="featured inner">
        <div class="modal fade bd-example-modal-lg" id="viewUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lgg">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div id="vusr" class="payment-modal-main">

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>





@endsection

@section('js')

<script>
    $(document).ready(function(){
        $('#ajaxSubmit').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let fd = new FormData(document.getElementById('add-user-form'));
            fd.append('name' , $('#name').val());
            fd.append('email' , $('#email').val());
            fd.append('phone' , $('#phone').val());
            fd.append('address' , $('#address').val());
            fd.append('country' , $('#country').val());
            fd.append('city' , $('#city').val());
            fd.append('state' , $('#state').val());
            fd.append('postal_code' , $('#postal_code').val());
            fd.append('address' , $('userfield').val());

            fd.append('files', $('#upload')[0].files[0]);

            $.ajax({
                url: "{{ route('submit-user') }}",
                method: 'post',
                data: fd,
                processData: false,
                contentType: false,
                //data: $('#add-user-form').serialize(),
                success: function(result){
                    console.log(result);
                    if(result.errors)
                    {

                        $('.alert-danger').html('');

                        $.each(result.errors, function(key, value){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>'+value+'</li>');
                        });
                    }
                    else
                    {
                        $('.alert-success').html('');
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.alert-success').append('<li>'+result.success+'</li>');
                         window.location.reload(true);

                    }
                },
                error: function(e){
                    $('.alert-danger').html('');
                    //console.log(e);
                    var keys = Object.keys(e.responseJSON.errors);

                    $.each(keys, function(index, key){
                        $('.alert-danger').show();
                        $('.alert-danger').append(`<li>${e.responseJSON.errors[key][0]}</li>`);
                    });
                }
            });
        });
    });
</script>

<script>

        $('.userinfo').on('click', function(){
            var id = $(this).attr('data-id');
            //alert(id);
            $.ajax({
                url: base_url + '/admin/users/view-user/'+id,
                data: {id: id},
                type: 'get',
                success: function(response){
                    $("#viewUserModal").modal("toggle");
                    $('#vusr').html(response);
                }
            });
        });

</script>


<script type="text/javascript">


    $('.statusButton').on('change', function(){
        var value = $(this).val();
        var target = $(this).data("id");
        $.ajax({
            url: '{{ route("update-user-status")}}',
            data: {value: value, target:target },
            type: 'get',
            success: function(response){
                if(response.status == 200){
                     toastr.success(response.msg, 'Success');
                }
                else{
                     toastr.error('Error');
                     $('.statusButton').val(!value);
                }
            }
        });
    });


    $("#status").change(function(){
        var status = this.value;
        toastr.success(status);
        if(status=="0") {
            toastr.success(status);
        } else {
            toastr.success(status);
        }
    });


    $(".delButton").on('click', function () {
	    var id = $(this).data("id");
	    $.confirm({
	        theme: 'material',
	        closeIcon: true,
	        animation: 'scale',
	        type: 'red',
	        title: 'Confirm!',
	        content: 'Are you sure want to delete this user ?',
	        buttons: {
	            confirm: function () {
	                $.ajax({
	                    url: base_url + '/admin/users/delete-user/'+id,
	                    success: function(response){
	                        if(response.status == 200){
	                            $('#row'+id).remove();
	                            //alert(response.msg);
	                            toastr.success(response.msg, 'Success');
	                        }else{
	                            toastr.error(response.msg, 'Error');
	                        }
	                    },
	                    error:function (response) {
	                            toastr.error('Some error occurred', 'Error');
	                    }
	                });
	            },
	            cancel: function () {
	                //console.log('sdvsdv');
	            }
	        }
	    });
	});

    $('.my-sort').DataTable({
        "order": [[ 6, "desc" ]]
    });

</script>

<script type="text/javascript">

    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+target).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    function phoneFormatter() {
      $('.phone').on('input', function() {
        var number = $(this).val().replace(/[^\d]/g, '')
        if (number.length == 7) {
          number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
        } else if (number.length == 10) {
          number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        }
        $(this).val(number)
      });
    };

    $(phoneFormatter);

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        /*locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'*/

        /*street_number: 'short_name',
        route: 'long_name',*/
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.setComponentRestrictions(
            {'country': ['uk']});
        autocomplete.addListener('place_changed', fillInAddress);
        /*
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
        });*/
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        console.log(place.geometry.location.lat());
        console.log(place.geometry.location.lng());
        document.getElementById("latitude").value = place.geometry.location.lat();
        document.getElementById("longitude").value = place.geometry.location.lng();
        for (var component in componentForm) {
            try{
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }catch(e){

            }
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            console.log(addressType)
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
@endsection
