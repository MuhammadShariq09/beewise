@extends('layouts.master')
@section('title',env('APP_NAME').' - Inspection Paylog Listing')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-body"> 
			<!-- Basic form layout section start -->
			<section id="configuration">
				<div class="row">
					<div class="col-12">
						<div class="card rounded pad-20">
							<div class="card-content collapse show">
								<div class="card-body table-responsive card-dashboard">
									<div class="row">
										<div class="col-12">
						                    @if(Session::has('message'))
						                        <div class="alert alert-success">
						                            <strong>{{ Session::get('message')  }}</strong>
						                        </div>
						                    @endif
						                    @if(Session::has('error'))
						                        <div class="alert alert-danger">
						                          <strong>{{ Session::get('error')  }}</strong>
						                        </div>
						                    @endif
					                    </div>
                                    <div class="col-6">
                                        <h1>Orders</h1>
                                    </div>
                                    <div class="col-6">
                                    	
                                        
                                    </div>
                                </div>
									<div class="clearfix"></div>
									<div class="maain-tabble">
										<table class="table table-striped table-bordered zero-configuration">
											<thead>
												<tr>
													<th>ID</th>
													<th>User Name</th>
                                                    <th>Inspection Amount</th>
                                                    <th>Transaction ID</th>
													<th>Created On</th>
                                                    
												</tr>
											</thead>
											<tbody>
												<?php $i = 0;
												?>
												@foreach($payments as $payment) 
                        						<?php $i++ ?>
													<tr id="row{{$payment->id}}">
	                                                	<td>{{$i}}</td>
														<td>
															@if($payment->user->image == "")
															<span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($payment->user->name,0,1)}}</span>
															@else
															<span class="avatar avatar-online">
	                                        					<img src="{{ asset($payment->user->image) }}" alt="{{ucfirst($payment->user->name)}}">
	                               							</span>
	                               							@endif
															{{ucfirst($payment->user->name)}}
														</td>
														<td>${{ $payment->amount }}</td>
														<td>{{ $payment->transaction_id }}</td>
														
														<td>{{ \Carbon\Carbon::parse($payment->created_at)->format('d/m/Y')}}</td>
														
													</tr>
  												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- // Basic form layout section end --> 
		</div>
	</div>
</div>
@endsection('content')  

@section('js')

<script type="text/javascript">      
    $('.my-sort').DataTable({
        "order": [[ 4, "desc" ]]        
    });

</script>
@endsection('js')