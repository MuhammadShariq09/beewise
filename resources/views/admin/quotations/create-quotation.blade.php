@extends('layouts.master')
@section('title', env('APP_NAME').' - Create Quotation Invoice')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")

@section("content")

<div class="app-content content view order-re create-qu">
        <div class="content-wrapper">
          <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration" class="search view-cause">
              <div class="row">
                <div class="col-12">
                  <div class="card rounded pad-20">
                    <div class="card-content collapse show">
                      <div class="card-body table-responsive card-dashboard">
                        <div class="row">
                          <div class="col-md-6 col-12">
                            <h1 class="pull-left"><a href="{{ route('quotations') }}"><i class="fa fa-angle-left"></i></a>Create quotation</h1>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12">
                            <h2>Order ID: <span>{{$quotation->order_num}}</span></h2>
                          </div>
                        </div>
                         <div class="row">
                          <div class="col-xl-10 col-12">
                            <div class="row">
                                @foreach($quotation->images as $image)
                                    <div class="col-sm-auto col-12"> <img src="{{ asset($image->image) }}" class="img-fluid" alt=""> </div>
                                @endforeach

                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xl-9 col-12">
                            <div class="order-inspection-top">
                              <div class="row">
                                <div class="col-12 d-sm-flex d-block align-items-center justify-content-between">
                                    <div>
                                        <p>Moving Date:</p>
                                        <h6>{{ Carbon\Carbon::parse($quotation->moving_date)->toDayDateTimeString() }}</h6>
                                    </div>
                                    @if($quotation->status == "1") 
                                        <div class="text-md-right">
                                            <div class="status">
                                                <h6>status: <span>Waiting For Inspection Request</span></h6>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                              </div>



                            </div>
                            <div class="row">
                              <div class="col-12">
                                <div class="order-inspection-middle-main">
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Username:</p>
                                      <h6>{{ ucfirst($quotation->user->name) }}</h6>
                                      <p>Contact:</p>
                                      <h6>{{$quotation->user->phone}}</h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Floor</p>
                                          <p>Levels</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$quotation->floors_from}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Email:</p>
                                      <h6>{{$quotation->user->email}}</h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Flagile</p>
                                          <p>Items</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$quotation->fragile_items}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Property Type:</p>
                                      <h6>
                                            @if($quotation->property_type_from == "1")
                                                Bungalow
                                            @elseif($quotation->property_type_from == "2")
                                                Apartment
                                            @elseif($quotation->property_type_from == "3")
                                                Cottage
                                            @elseif($quotation->property_type_from == "4")
                                                House
                                            @else
                                                Mansion
                                            @endif
                                      </h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Packaging</p>
                                          <p>Items</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$quotation->packaging_items}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <form  action="{{url('/admin/quotations/submit-quotation-invoice/')}}" method="post" id="invoice-form">
                                    @csrf
                                    <input type="hidden" id="quotation_id" name="quotation_id" value="{{$quotation->id}}">
                                    <input type="hidden" id="status" name="status" value="1">
                                <div class="order-inspection-bottom">
                                  <div class="row">
                                    <div class="col-12">
                                      <h4>Item List &amp; Costing</h4>
                                    </div>
                                  </div>
                                  <div class="maain-tabble" id="#tbl_posts_body">
                                       
                                    <table class="table table-striped table-bordered zero-configuration">
                                      <thead>
                                        <tr>
                                          <th>Items</th>
                                          <th>Quantity</th>
                                          <th>Cost Item <span>(Per Time)</span></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                            @php 
                                                $invoiceItems =  Helper::getQuotationItems($quotation->id);
                                                //dd($invoiceItems);
                                                $i = 0; 
                                            @endphp
                                            @if(count($invoiceItems) > 0)
                                                @foreach($invoiceItems as $invoiceItem)
                                                    @foreach($invoiceItem->items as $inv)   
                                                        <tr id="hello">
                                                            <td><input required="true" type="text" name="items[{{$i}}][item]" class="form-control" value="{{$inv->item}}"></td>
                                                            <td><input required="true" type="number" name="items[{{$i}}][quantity]" class="form-control" value="{{$inv->quantity}}"> </td>
                                                            <td class="">
                                                                <input required="true" name="items[{{$i}}][price]" type="number" class="price"  value="{{$inv->price}}">
                                                                <span class="text-success float-right">
                                                                <i class="fa fa-check-circle "></i></span>
                                                            </td>
                                                        </tr>
                                                        @php 
                                                            $i++;
                                                        @endphp
                                                    @endforeach    
                                                @endforeach
                                            @else
                                                <tr id="hello">
                                                    <td><input required="true" name="items[0][item]" type="text" class="form-control" placeholder="dinning table "></td>
                                                    <td><input required="true" name="items[0][quantity]" type="number" class="form-control" placeholder="2"></td>
                                                    <td class="">
                                                        <input required="true" name="items[0][price]" type="number" class="price form-control" placeholder="£80">
                                                        <span class="text-success float-right">
                                                        <i class="fa fa-check-circle "></i></span>
                                                    </td>
                                                </tr>
                                            @endif    
                                      </tbody>
                                    </table>
                                            </form>
                                      <button type="button" id="btn"><i class="fa fa ft-plus-circle"></i> Add New</button>
                                   </div>


                                  <div class="row">
                                    <div class="col-12">
                                      <div class="items-main">
                                            @php 
                                            $invoiceDetails =  Helper::getQuotationDetails($quotation->id);
                                            $i = 0; 
                                        @endphp
                                        @if(count($invoiceDetails) > 0)
                                            @foreach($invoiceDetails as $invoiceDetail)
                                                @foreach($invoiceDetail->invoiceOrderDetails as $inv)  
                                                    <div class="item-top ">
                                                        <p>{{$inv->item}}:</p>
                                                        <input type="hidden" name="details[{{$i}}][item]" value="{{$inv->item}}" />
                                                        <input name="details[{{$i}}][quantity]" type="number" placeholder="item quantity" value="{{$inv->quantity}}">
                                                        <input name="details[{{$i}}][price]" type="number" class="price" placeholder="cost" value="{{$inv->price}}">
                                                    </div>
                                                    @php 
                                                        $i++;
                                                    @endphp
                                                @endforeach    
                                            @endforeach    
                                        @else        
                                            <div class="item-top ">
                                                <p>Flagile Items:</p>
                                                <input type="hidden" name="details[0][item]" value="Flagile Items" />
                                                <input name="details[0][quantity]" type="number" placeholder="item quantity">
                                                <input name="details[0][price]" type="number" class="price" placeholder="cost">
                                            </div>
                                            <div class="item-top ">
                                                <p>packaging Items: </p>
                                                <input type="hidden"  name="details[1][item]" value="Packaging Items" />
                                                <input name="details[1][quantity]" type="number" placeholder="item quantity">
                                                <input name="details[1][price]" type="number" class="price" placeholder="cost">

                                            </div>
                                        @endif



                                        <div class="item-bottom d-flex justify-content-between">
                                          <div class="">
                                            <h4>Estimated Cost:</h4>
                                            @php 
                                                $invoices =  Helper::getQuotationInvoice($quotation->id);
                                                $i = 1; 
                                            @endphp
                                            @if(count($invoices) > 0)
                                                <div class="cost-box">
                                                    <input name="estimated_to" type="number" placeholder="Cost" value="{{$invoices[0]->estimated_to}}">
                                                    <p>to</p>
                                                    <input  name="estimated_from" type="number" placeholder="Cost" value="{{$invoices[0]->estimated_from}}">
                                                </div>   
                                            @else    
                                                <div class="cost-box">
                                                    <input name="estimated_to" type="number" placeholder="Cost">
                                                    <p>to</p>
                                                    <input  name="estimated_from" type="number" placeholder="Cost">
                                                </div>
                                              @endif  
                                          </div>
                                          <div class="">

                                            <h6>Total Cost: <input readonly class="netTotal" name="total" /></h6>
                                          </div>
                                        </div>
                                           @if($quotation->status == "0") 
                                          <div class="row">
                                              <div class="col-12 text-left">

                                              <button type="submit">Send quotation</button>
                                              </div>
                                          </div>
                                          @endif
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- Basic Column Chart -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>

    @endsection

    @section('js')

    <script src="{{ asset('assets/admin/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/app-assets/js/scripts/forms/form-repeater.js') }}" type="text/javascript"></script>
    <script>
        var counter = 0;
        $(document).ready(function(){  
            $("#btn").click(function(){   
                counter++;
                $("#hello").after(`<tr>
                                        <td><input name="items[${counter}][item]" type='text' class='' placeholder='dinning table '></td> 
                                        <td><input name="items[${counter}][quantity]" type='number' class='' placeholder='2'></td> 
                                        <td><input name="items[${counter}][price]" type='number' class='price' placeholder='£80'></td> 
                                </tr>`);  
            });  

            calculateInvoicePrice();
        });

        function calculateInvoicePrice() {
            var netTotal = $('.price').map(function(index, item){ 
                return $(item).val();
            }).get().reduce((a, b) => Number(a) + Number(b), 0);

            // var netTotal = $("#tbl_posts_body").find(".price").map(function() {
            //     console.log(Number(this.value));
            //     return Number(this.value);
            // }).get().reduce((a, b) => a + b, 0);
            $(".netTotal").val("£" + netTotal);
        }

        $(document).on("change", ".price", function() {
            calculateInvoicePrice();
        })
</script>

    @endsection
