@extends('layouts.master')
@section('title',env('APP_NAME').' - Quotations Listing')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')
<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-body">
			<!-- Basic form layout section start -->
			<section id="configuration">
				<div class="row">
					<div class="col-12">
						<div class="card rounded pad-20">
							<div class="card-content collapse show">
								<div class="card-body table-responsive card-dashboard">
									<div class="row">
										<div class="col-12">
						                    @if(Session::has('message'))
						                        <div class="alert alert-success">
						                            <strong>{{ Session::get('message')  }}</strong>
						                        </div>
						                    @endif
						                    @if(Session::has('error'))
						                        <div class="alert alert-danger">
						                          <strong>{{ Session::get('error')  }}</strong>
						                        </div>
						                    @endif
					                    </div>
                                    <div class="col-6">
                                        <h1>Quotations</h1>
                                    </div>
                                    <div class="col-6">


                                    </div>
                                </div>
									<div class="clearfix"></div>
									<div class="maain-tabble">
										<table class="table table-striped table-bordered zero-configuration">
											<thead>
												<tr>
                                                    <th>ID</th>
                                                    <th>Order #</th>
													<th>User Name</th>
                                                    <th>Moving From</th>
                                                    <th>Moving To</th>
                                                    <th>Moving Date</th>
                                                    <th>Fragile Items</th>
                                                    <th>Packaging Items</th>
													<th>Created On</th>
                                                    <th style="width:10%;">Status</th>
                                                    <th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0;?>
												@foreach($quotations as $quotation)
                        						<?php $i++ ?>
												<tr id="row{{$quotation->id}}">
                                                    <td>{{$i}}</td>
                                                    <td>{{$quotation->order_num}}</td>
													<td>
														@if($quotation->user->image == "")
														<span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($quotation->user->name,0,1)}}</span>
														@else
														<span class="avatar avatar-online">
                                        					<img src="{{ asset($quotation->user->image) }}" class="circle" alt="{{ucfirst($quotation->user->name)}}">
                               							</span>
                               							@endif
														{{ucfirst($quotation->user->name)}}
													</td>
													<td><a href="#" data-toggle="tooltip" title="{{ $quotation->address_from }}">{{ str_limit($quotation->address_from, $limit = 15, $end = '  ...') }}</a></td>
													<td><a href="#" data-toggle="tooltip" title="{{ $quotation->address_to }}">{{ str_limit($quotation->address_to, $limit = 15, $end = '  ...') }}</a></td>
													<td>{{ Carbon\Carbon::parse($quotation->moving_date)->toDayDateTimeString() }}</td>
													<td>{{ $quotation->fragile_items }}</td>
													<td>{{ $quotation->packaging_items }}</td>
													<td>{{ \Carbon\Carbon::parse($quotation->created_at)->format('d/m/Y')}}</td>
													<td>
													    @if($quotation->status == "0")
													        <span>Pending</span>
													    @else
													        <span>Approved</span>
													    @endif
													</td>
                                                    <td>
						                                {{-- <div class="btn-group mr-1 mb-1">
						                                <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
						                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">


						                                  <a class="dropdown-item" href="{{ route('edit-quotation', ['id' => $quotation->id])}}"><i class="fa fa-pencil-square-o"></i>Edit</a>
						                                  <a class="dropdown-item delButton"  data-id="{{$quotation->id}}" href="javascript:void(0)"><i class="fa fa-trash"></i>Delete</a>
						                                </div>
                                                      </div> --}}
                                                        <div class="btn-group mr-1 mb-1">
                                                            <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                    <a class="dropdown-item" href="{{ route('view-quotation', ['id' => $quotation->id])}}"><i class="fa fa-eye"></i>View </a>
                                                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-trash"></i>delete </a>
                                                                </div>
                                                        </div>
						                            </td>
												</tr>
  												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- // Basic form layout section end -->
		</div>
	</div>
</div>
@endsection('content')

@section('js')

<script type="text/javascript">

    $('.statusButton').on('change', function(){
        var value = $(this).val();
        var target = $(this).data("id");
        $.ajax({
            url: '{{ route("update-quotation-status")}}',
            data: {value: value, target:target },
            type: 'get',
            success: function(response){
                if(response.status == 200){
                     toastr.success(response.msg, 'Success');
                }
                else{
                     toastr.error('Error');
                     $('.statusButton').val(!value);
                }
            }
        });
    });


	$(".delButton").on('click', function () {
	    var id = $(this).data("id");
	    $.confirm({
	        theme: 'material',
	        closeIcon: true,
	        animation: 'scale',
	        type: 'red',
	        title: 'Confirm!',
	        content: 'Are you sure want to delete this quotation ?',
	        buttons: {
	            confirm: function () {
	                $.ajax({
	                    url: base_url + '/admin/quote/delete-quotation/'+id,
	                    success: function(response){
	                        if(response.status == 200){
	                            $('#row'+id).remove();
	                            //alert(response.msg);
	                            toastr.success(response.msg, 'Success');
	                        }else{
	                            toastr.error(response.msg, 'Error');
	                        }
	                    },
	                    error:function (response) {
	                            toastr.error('Some error occurred', 'Error');
	                    }
	                });
	            },
	            cancel: function () {
	                //console.log('sdvsdv');
	            }
	        }
	    });
	});

    $("#status").change(function(){
        var status = this.value;
        toastr.success(status);
        if(status=="0") {
            toastr.success(status);
        } else {
            toastr.success(status);
        }
    });

    $('.my-sort').DataTable({
        "order": [[ 6, "desc" ]]
    });

</script>
@endsection('js')
