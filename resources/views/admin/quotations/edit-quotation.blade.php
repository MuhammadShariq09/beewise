@extends('layouts.master')
@section('title', env('APP_NAME').' - Edit Quotation')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns") 


@section('content')  
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body"> 
                    <section id="configuration" class="u-c-p categorie create-job">
                        <div class="row profile-frm">
                            <div class="col-12">
                                <div class="card pro-main">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success">
                                            <strong>{{ Session::get('message')  }}</strong>
                                        </div>
                                    @endif
                                    @foreach ($errors->all() as $message)
                                        <div class="alert alert-danger">
                                          <strong>{{ $message  }}</strong>
                                        </div>

                                    @endforeach
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Edit Quotation</h1>
                                        </div>
                                    </div>
                                    <form  action="{{ route('update-quotation') }}" enctype="multipart/form-data" method="post" novalidate class="form">
                                    @csrf
                                        <div class="create-job-inner">
                                            <div class="row">
                                                <input type="hidden" name="quotation_id" id="quotation_id" value="{{$quotation->id}}" />
                                                <div class="col-lg-7 col-md-12 col-sm-12">
                                                    <div class="pro-inner-row">
                                                            <label><i class="fa fa-user-circle"></i> User Name</label>
                                                            <p class="editable">{{ ucfirst($quotation->user->first_name).' '.ucfirst($quotation->user->last_name) }}</p>
                                                    </div>
                                                    
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-user-circle"></i> User Contact #</label>
                                                        <p class="editable">{{$quotation->user->phone}}</p>
                                                    </div>
                                                    
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-envelope"></i> User Email Address</label>
                                                        <p class="editable">{{$quotation->user->email}}</p>
                                                    </div>
                                                    
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-phone"></i> Moving From</label>
                                                        <p class="editable">{{$quotation->address_from}}</p>
                                                    </div>
                                                    
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-calendar"></i> Moving To</label>
                                                        <p class="editable">{{$quotation->address_to}}</p>
                                                    </div>
                                                    
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-th-large"></i> Moving Date</label>
                                                        <p class="editable">{{ Carbon\Carbon::parse($quotation->moving_date)->toDayDateTimeString() }}</p>
                                                    </div>
                                                    
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-th-large"></i> Packaging Items</label>
                                                        <p class="editable">{{$quotation->packaging_items}}</p>
                                                    </div>
                                                    
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-th-large"></i> Fragile Items</label>
                                                        <p class="editable">{{$quotation->fragile_items}}</p>
                                                    </div> 

                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-th-large"></i> Invoice Details</label>
                                                        <button type="button"  data-id="{{$quotation->id}}"  class="view-invoice edit-invoice" data-toggle="modal" data-target="#edit-invoice">Generate Invoice</button>
                                                    </div>
                                                    
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-th-large"></i> Items Details</label>
                                                        <button type="button"  data-id="{{$quotation->id}}"  class="view-invoice edit-item" data-toggle="modal" data-target="#edit-item">Generate Item List</button>
                                                    </div>

                                                   <!--  <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                                <label><i class="fa fa-calendar"></i> Inspection Date</label>
                                                            </div>
                                                        </div>
                                                        <div  id="datetimepicker2" class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                            
                                                            <input data-format="MM/dd/yyyy HH:mm:ss PP" type="text" />
                                                            <span class="add-on">
                                                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                              </i>
                                                            </span>
  
                                                            @if ($errors->has('inspection_date'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('inspection_date') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div> -->
                                                    <!--<div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Inspection Date</label>
                                                        <div class="input-group date" id="datetimepicker1">
                                                            <input type="text" name="inspection_date" id="inspection_date" class="form-control">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>-->


                                              
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-12">         
                                                        
                                                        <div class="previous_image">
                                                         @foreach($quotation->images as $image)
                                                          <div class="col-md-3 col-3">
                                                            <img style="width:200px;" src="{{ asset($image->image) }}" alt=""  />
                                                          </div>
                                                         @endforeach   
                                                        </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>


    <!-- Invoice Modal -->
    <div id="edit-invoice" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{url('/admin/quotations/quotation-invoice/')}}" method="post" id="invoice-form">
                @csrf
                    <input type="hidden" id="quotation_id" name="quotation_id" value="{{$quotation->id}}">
                    <input type="hidden" id="status" name="status" value="1">
                    <div class="modal-body invice-modal">
                        <img src="{{asset('assets/admin/images/invice-modal-img.png')}}" class="top-img" alt="">
                        <h2>Create Invoice</h2>
                        <div class="row">
                            @foreach ($errors->all() as $message)
                                        <div class="alert alert-danger">
                                          <strong>{{ $message  }}</strong>
                                        </div>

                                    @endforeach
                            <div class="col-lg-12">
                                <div class="well clearfix">
                                    <a style="padding: 5px;color: white;" class="pull-right add-record">
                                        <b>Add</b>
                                    </a>
                                      
                                </div>
                                <table class="" id="tbl_posts">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_posts_body">
                                    @php 
                                        $invoices =  Helper::getQuotationInvoice($quotation->id);
                                        $i = 1; 
                                    @endphp
                                    @if(count($invoices) > 0)
                                        @foreach($invoices as $invoice)
                                            @foreach($invoice->items as $inv)   
                                                <tr id="rec-{{$i}}">
                                                    <td><input required="true" type="text" name="item[{{$i}}][item]" class="form-control" value="{{$inv->item}}"></td>
                                                    <td><input required="true" type="number" step="any" name="item[{{$i}}][price]" class="price form-control" value="{{$inv->price}}"> </td>
                                                    <td>
                                                        <a style="display: block; padding: 7px; color: white;margin: 0px;margin-left: 10px;" class="btn btn-danger delete-record" data-id="1"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                @php 
                                                    $i++;
                                                @endphp
                                            @endforeach    
                                        @endforeach
                                    @else
                                        <tr id="rec-1">
                                            <td><input required="true" type="text" name="item[0][item]" class="form-control"></td>
                                            <td><input required="true" type="number" step="any" name="item[0][price]" class="price form-control" value=""> </td>
                                            <td>
                                                <a style="display: block; padding: 7px; color: white;margin: 0px;margin-left: 10px;" class="btn btn-danger delete-record" data-id="1"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endif    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--row end-->
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="pull-right"><span>Total: $</span>&nbsp;<input name="total" id="total"class="netTotal form-control" value="0"></h4>
                            </div>
                        </div>
                        @php 
                            $invoices =  Helper::getQuotationInvoice($quotation->id);
                            $i = 1; 
                        @endphp
                        @if(count($invoices) > 0)
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4 class="pull-right"><span>Estimated Amount:</span>&nbsp;<input name="estimated_amount" id="estimated_amount"class="form-control" value="{{$invoices[0]->estimated_amount}}"></h4>
                                </div>
                            </div>   
                        @else    
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4 class="pull-right"><span>Estimated Amount:</span>&nbsp;<input name="estimated_amount" id="estimated_amount"class="form-control" value=""></h4>
                                </div>
                            </div>
                        @endif
                        <div class="clearfix"></div>
                        <hr>

                        <div class="text-center mt-2">
                            <button class="btn btn-info" type="submit">Generate Invoice</button>
                        </div>
                    </div>    
                </form>  
            <!--invice modal end-->
            </div>
        </div>
    </div>

    <!-- Item Modal -->
    <div id="edit-item" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{url('/admin/quotations/quotation-item/')}}" method="post" id="invoice-form">
                @csrf
                    <input type="hidden" id="quotation_id" name="quotation_id" value="{{$quotation->id}}">
                    <input type="hidden" id="status" name="status" value="1">
                    <div class="modal-body invice-modal">
                        <img src="{{asset('assets/admin/images/invice-modal-img.png')}}" class="top-img" alt="">
                        <h2>Create Invoice</h2>
                        <div class="row">
                            @foreach ($errors->all() as $message)
                                        <div class="alert alert-danger">
                                          <strong>{{ $message  }}</strong>
                                        </div>

                                    @endforeach
                            <div class="col-lg-12">
                                <div class="well clearfix">
                                    <a style="padding: 5px;color: white;" class="pull-right add-record">
                                        <b>Add</b>
                                    </a>
                                      
                                </div>
                                <table class="" id="tbl_items">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_items_body">
                                    @php 
                                        $items =  Helper::getQuotationItems($quotation->id);
                                        $i = 1; 
                                    @endphp
                                    @if(count($items) > 0)
                                        @foreach($items as $item)
                                            @foreach($invoice->items as $inv)   
                                                <tr id="rec-{{$i}}">
                                                    <td><input required="true" type="text" name="item[{{$i}}][item]" class="form-control" value="{{$inv->item}}"></td>
                                                    <td><input required="true" type="number" step="any" name="item[{{$i}}][quantity]" class="price form-control" value="{{$inv->quantity}}"> </td>
                                                    <td>
                                                        <a style="display: block; padding: 7px; color: white;margin: 0px;margin-left: 10px;" class="btn btn-danger delete-item" data-id="1"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                @php 
                                                    $i++;
                                                @endphp
                                            @endforeach    
                                        @endforeach
                                    @else
                                        <tr id="rec-1">
                                            <td><input required="true" type="text" name="item[0][item]" class="form-control"></td>
                                            <td><input required="true" type="number" step="any" name="item[0][quantity]" class="quantity form-control" value=""> </td>
                                            <td>
                                                <a style="display: block; padding: 7px; color: white;margin: 0px;margin-left: 10px;" class="btn btn-danger delete-item" data-id="1"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endif    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <hr>

                        <div class="text-center mt-2">
                            <button class="btn btn-info" type="submit">Generate Items List</button>
                        </div>
                    </div>    
                </form>  
            <!--invice modal end-->
            </div>
        </div>
    </div>

@endsection('content')  

@section('js')
<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
<script src="{{ asset('assets/admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>

<script src="{{ asset('assets/admin/app-assets/js/scripts/pickers/dateTime/bootstrap-datetime.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
 <!--    <script src="{{ asset('assets/admin/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.min.js') }}"></script> -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css"/>
    
<script>
    $(document).ready(function(){
        var date=new Date();    
        var date_input=$('input[name="inspection_date"]'); //our date input has the name "date"
        var container=$('.profile-frm').length>0 ? $('.profile-frm form').parent() : "body";
        var options={
        format: 'yyyy-mm-dd hh:ii:ss',
        container: container,
        todayHighlight: true,
        autoclose: true,
        startDate :  date,
        };
        date_input.datepicker(options);
    });
</script> -->
<script type="text/javascript">

$(function() {



    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="margin-left:10px; width:200px; height:auto;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#upload').on('change', function() {
        //imagesPreview(this, 'div.gallery');
        $('.previous_image').html('');
        imagesPreview(this, 'div.previous_image');
    });
});

</script>

<script type="text/javascript">

    $(document).ready(function() {
        
        jQuery(document).delegate('a.add-record', 'click', function(e) {
          e.preventDefault();
          var content = jQuery('#tbl_items'),
            size = jQuery('#tbl_items >tbody >tr').length + 1,
            element = null,
            element = $(`<tr id="rec-${size}">
                            <td><input type="text" required name="item[${size}][item]" class="form-control"></td>
                            <td><input type="number" required step="any" name="item[${size}][quantity]" class="quantity form-control" value=""> </td>
                            <td>
                                <a style="display: block; padding: 7px; color: white;margin: 0px;margin-left: 10px;" class="btn btn-danger delete-item" data-id="1"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>`);
          // element.find('.delete-record').attr('data-id', size);
          element.appendTo('#tbl_items_body');
          // element.find('.sn').html(size);
        });
        
        jQuery(document).delegate('a.delete-record', 'click', function(e) {
          
          $(this).parents('tr').fadeOut(function () {
              $(this).remove();
          });
        });

    });


    $(document).ready(function() {
        $("#total").prop("readonly", true);
        jQuery(document).delegate('a.add-record', 'click', function(e) {
          e.preventDefault();
          var content = jQuery('#tbl_posts'),
            size = jQuery('#tbl_posts >tbody >tr').length + 1,
            element = null,
            element = $(`<tr id="rec-${size}">
                            <td><input type="text" required name="item[${size}][item]" class="form-control"></td>
                            <td><input type="number" required step="any" name="item[${size}][price]" class="price form-control" value=""> </td>
                            <td>
                                <a style="display: block; padding: 7px; color: white;margin: 0px;margin-left: 10px;" class="btn btn-danger delete-record" data-id="1"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>`);
          // element.find('.delete-record').attr('data-id', size);
          element.appendTo('#tbl_posts_body');
          // element.find('.sn').html(size);
        });
        
        jQuery(document).delegate('a.delete-record', 'click', function(e) {
          
          $(this).parents('tr').fadeOut(function () {
              $(this).remove();
              calculateInvoicePrice();
          });
        });

        calculateInvoicePrice();
    });

  function calculateInvoicePrice() {

    var netTotal = $("#tbl_posts_body").find(".price").map(function() {
      console.log(Number(this.value));
      return Number(this.value);
    }).get().reduce((a, b) => a + b, 0);
    $(".netTotal").val("$" + netTotal);
  }

  $(document).on("change", ".price", function() {
    calculateInvoicePrice();
  })
</script>
@endsection