@extends('layouts.master')
@section('title', env('APP_NAME').' - View Quotation')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')
    <div class="app-content content view order-re quotation-p">
        <div class="content-wrapper">
          <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration" class="search view-cause">
              <div class="row">
                <div class="col-12">
                  <div class="card rounded pad-20">
                    <div class="card-content collapse show">
                      <div class="card-body table-responsive card-dashboard">
                        <div class="row">
                          <div class="col-12 col-md-6">
                            <h1 class="pull-left"><a href="{{ route('quotations') }}"><i class="fa fa-angle-left"></i></a>quotation details</h1>
                          </div>
                            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                            <div class="breadcrumb-wrapper col-12">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('quotations') }}">Quotation</a> </li>
                                <li class="breadcrumb-item active">Quotation Request </li>
                              </ol>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12">
                            <h2>Order ID: <span>{{$quotation->order_num}}</span></h2>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xl-10 col-12">
                             <div class="row">
                                @foreach($quotation->images as $image)

                                    <div class="col-sm-auto col-12 thumb">
                                    <a class="thumbnail" href="#" data-image-id="{{$image->id}}" data-toggle="modal" data-height="500" data-image="{{ asset($image->image) }}" data-target="#image-gallery{{$image->id}}">
                                            <img class="img-thumbnail" src="{{ asset($image->image) }}" alt="Another alt text">
                                        </a>
                                    </div>


                                    <div class="modal fade img-view" id="image-gallery{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <h4 class="modal-title" id="image-gallery-title"></h4>
                                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span> </button>
                                                </div>
                                                <div class="modal-body"> <img id="image-gallery-image" class="img-responsive col-md-12" src=""> </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i> </button>
                                                  <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i> </button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                @endforeach
                            </div>
                          </div>
                        </div>
                          <!--image viewer modal start here-->

                        <!--image viewer modal end here-->
                        <div class="row">
                          <div class="col-xl-12 ">
                            <div class="order-inspection-top order-inspection-middle">
                              <div class="row">
                                <div class="col-xl-3 col-md-6 col-12">
                                  <p>Moving Date:</p>
                                  <h6>{{ Carbon\Carbon::parse($quotation->moving_date)->toDayDateTimeString() }}</h6>
                                </div>
                                <div class="row">
                                  <div class="col-xl-3 col-md-6 col-12 text-left">
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Floor</p>
                                          <p>Levels</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$quotation->floors_from}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-xl-3 col-md-6 col-12 text-left">
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Number</p>
                                          <p>of Rooms</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$quotation->rooms_from}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-xl-3 col-md-6 col-12 text-left">
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Flagile</p>
                                          <p>Items</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$quotation->fragile_items}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-xl-3 col-md-6 col-12 text-left">
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Packaging</p>
                                          <p>Item</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$quotation->packaging_items}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-12">
                                  <p>Username:</p>
                                  <h6>{{ ucfirst($quotation->user->name) }}</h6>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xl-3 col-12">
                                  <p>contact:</p>
                                  <h6>{{$quotation->user->phone}}</h6>
                                  <p>Email:</p>
                                  <h6>{{$quotation->user->email}}</h6>
                                  <p>Property Type:</p>
                                    <h6>
                                        @if($quotation->property_type_from == "1")
                                            Bungalow
                                        @elseif($quotation->property_type_from == "2")
                                            Apartment
                                        @elseif($quotation->property_type_from == "3")
                                            Cottage
                                        @elseif($quotation->property_type_from == "4")
                                            House
                                        @else
                                            Mansion
                                        @endif
                                    </h6>
                                </div>

                              <div class="col-xl-3 col-md-6 col-12">
                                <div class="bottom-box">
                                  <h6>Moving From</h6>
                                  <ul>
                                    <li>
                                      <div class="media"> <i class="fa fa-bed mr-1"></i>
                                            <div class="media-body">
                                                <p>
                                                  4+ Bed
                                                    @if($quotation->property_type_from == "1")
                                                        Bungalow
                                                    @elseif($quotation->property_type_from == "2")
                                                        Apartment
                                                    @elseif($quotation->property_type_from == "3")
                                                        Cottage
                                                    @elseif($quotation->property_type_from == "4")
                                                        House
                                                    @else
                                                        Mansion
                                                    @endif
                                                </p>
                                            </div>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="media"> <i class="fa fa-map-marker mr-1"></i>
                                        <div class="media-body">
                                          <p>{{$quotation->address_from}} </p>
                                        </div>
                                      </div>
                                    </li>

                                    <li>
                                      <div class="media"> <i class="fa fa-map mr-1"></i>
                                        <div class="media-body">
                                          <p>{{$quotation->zipcode_from}}</p>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                                   </div>
                                   <div class="col-xl-3 col-md-6 col-12">
                                <div class="bottom-box">
                                  <h6>Moving to</h6>
                                  <ul>
                                    <li>
                                      <div class="media"> <i class="fa fa-bed mr-1"></i>
                                        <div class="media-body">
                                           <p>
                                               4+ Bed
                                               @if($quotation->property_type_from == "1")
                                                    Bungalow
                                                @elseif($quotation->property_type_from == "2")
                                                    Apartment
                                                @elseif($quotation->property_type_from == "3")
                                                    Cottage
                                                @elseif($quotation->property_type_from == "4")
                                                    House
                                                @else
                                                    Mansion
                                                @endif
                                            </p>
                                        </div>

                                      </div>
                                    </li>
                                    <li>
                                      <div class="media"> <i class="fa fa-map-marker mr-1"></i>
                                        <div class="media-body">
                                         <p>{{$quotation->address_to}} </p>
                                        </div>
                                      </div>
                                    </li>

                                    <li>
                                      <div class="media"> <i class="fa fa-map mr-1"></i>
                                        <div class="media-body">
                                          <p>{{$quotation->zipcode_to}}</p>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                                   </div>

                              </div>

                                <div class="row">
                                    <div class="col-12 text-center">

                                    <a href="{{ route('create-quotation-invoice', ['id' => $quotation->id])}}" class="gen-q"> Create quotation</a>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
    </div>
@endsection

@section('js')

<script src="{{ asset('assets/admin/assets/js/light.js') }}"></script>

@endsection
