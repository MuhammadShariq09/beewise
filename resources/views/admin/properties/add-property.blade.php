@extends('layouts.master')
@section('title',env('APP_NAME').' - Create Property')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body"> 
                    <section id="configuration" class="u-c-p categorie create-job">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pro-main">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success">
                                            <strong>{{ Session::get('message')  }}</strong>
                                        </div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="alert alert-danger">
                                          <strong>{{ Session::get('error')  }}</strong>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Create Property</h1>
                                        </div>
                                    </div>
                                    <form  action="{{ route('submit-property') }}" enctype="multipart/form-data" method="post" novalidate class="form">
                                    @csrf
                                        <div class="create-job-inner">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-7 col-md-12 col-12">
                                                            <div class="cat-text">

                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                                <label for="title"><i class="fa fa-address-card"></i> Property Title</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                            <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}">
                                                        </div> 
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                                <label for="status"><i class="fa fa-check-circle"></i> Choose Status</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                            <select class="form-control" name="status" id="status">
                                                                <option {{ (old('status') == 0) ? "selected" : "" }}  value="0">In-Active</option>
                                                                <option {{ (old('status') == 1) ? "selected" : "" }} value="1">Active</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                            
                                                            <button type="submit" class="save">Create Job</button>
                                                        </div>
                                                    </div>       
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-12 col-12">         
                                                    <button name="file" class="uplon-btn2" type="button" onclick="document.getElementById('upload').click()">
                                                        <div class="photo-div" id='photoBG'>
                                                            <div class="photo-div-inner">
                                                                <p><i class="fa fa-camera"></i>Add Image</p>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    <input type="file"  accept="image/*" required onchange="readURL(this, 'photoBG')" id="upload" name="property_image">
                                                
                                            </div>

                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection('content')  

@section('js')


<script type="text/javascript">
    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+target).css('background-image', 'url(' + e.target.result + ')');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
@endsection('js')