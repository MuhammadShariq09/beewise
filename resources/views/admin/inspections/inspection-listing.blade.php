@extends('layouts.master')
@section('title',env('APP_NAME').' - Inspection Request')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-body"> 
			<!-- Basic form layout section start -->
			<section id="configuration">
				<div class="row">
					<div class="col-12">
						<div class="card rounded pad-20">
							<div class="card-content collapse show">
								<div class="card-body table-responsive card-dashboard">
									<div class="row">
										<div class="col-12">
						                    @if(Session::has('message'))
						                        <div class="alert alert-success">
						                            <strong>{{ Session::get('message')  }}</strong>
						                        </div>
						                    @endif
						                    @if(Session::has('error'))
						                        <div class="alert alert-danger">
						                          <strong>{{ Session::get('error')  }}</strong>
						                        </div>
						                    @endif
					                    </div>
                                    <div class="col-6">
                                        <h1>Inspection Request</h1>
                                    </div>
                                    
                                    	
                                        
                                    </div>
                                </div>
									<div class="clearfix"></div>
									<div class="maain-tabble">
										<table class="table table-striped table-bordered zero-configuration">
											<thead>
												<tr>
													<th>ID</th>
													<th>User Name</th>
                                                    <th>Moving Date</th>
                                                    <th>Inspection Date & Time</th>
                                                    <th>Created On</th>
                                                    <th style="width:10%;">Status</th>
                                                    <th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0;?> 
												@foreach($inspections as $inspect) 
                        						<?php $i++ ?>
												<tr id="row{{$inspect->id}}">
                                                	<td>{{$i}}</td>
                                                	<td>
														@if($inspect->invoiceOrder->quotationInvoice->user->image == "")
														<span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($inspect->invoiceOrder->quotationInvoice->user->name,0,1)}}</span>
														@else
														<span class="avatar avatar-online">
                                        					<img src="{{ asset($inspect->invoiceOrder->quotationInvoice->user->image) }}" alt="{{ucfirst($inspect->invoiceOrder->quotationInvoice->user->name)}}">
                               							</span>
                               							@endif
														{{ucfirst($inspect->invoiceOrder->quotationInvoice->user->name)}}
													</td>
													<td>{{ Carbon\Carbon::parse($inspect->invoiceOrder->quotationInvoice->moving_date)->toDayDateTimeString() }}</td>
													<td>{{\Carbon\Carbon::parse($inspect->date)->format('d/m/Y').' '.$inspect->time}}</td>
													<td>{{ \Carbon\Carbon::parse($inspect->created_at)->format('d/m/Y')}}</td>
													<td>
														<select name="status" class="form-control statusButton"  data-id="{{$inspect->id}}" >
						                                    <option value="0" {{ $inspect->status == 0 ? 'selected' : '' }}>Pending</option>
						                                    <option value="1" {{ $inspect->status == 1 ? 'selected' : '' }}>Approved</option>
						                                </select>
													</td>
                                                    <td>
						                                <div class="btn-group mr-1 mb-1">
						                                <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
						                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
						                                  
						                                  @if($inspect->is_paid == "0")
						                                    <a class="dropdown-item" href="{{ route('edit-inspection-request', ['id' => $inspect->id])}}"><i class="fa fa-pencil-square-o"></i>Edit Inspection Request</a>
						                                  @endif
						                                  @if($inspect->is_paid == "1")
						                                    <a class="dropdown-item" data-id="{{$inspect->id}}" data-toggle="modal" data-target="#edit-invoice"><i class="fa fa-pencil-square-o"></i>Generate Final Invoice</a>
						                                  @endif
						                                  <!--<a class="dropdown-item delButton"  data-id="{{$inspect->id}}" href="javascript:void(0)"><i class="fa fa-trash"></i>Delete</a>-->
						                                </div>
						                              </div>
						                            </td>
													
												</tr> 
  												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- // Basic form layout section end --> 
		</div>
	</div>
</div>

 <!-- Invoice Modal -->
    <div id="edit-invoice" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{url('/admin/quotations/quotation-invoice/')}}" method="post" id="invoice-form">
                @csrf
                    <input type="hidden" id="quotation_id" name="quotation_id" value="">
                    <input type="hidden" id="status" name="status" value="1">
                    <div class="modal-body invice-modal">
                        <img src="{{asset('assets/admin/images/invice-modal-img.png')}}" class="top-img" alt="">
                        <h2>Create Invoice</h2>
                        <div class="row">
                            @foreach ($errors->all() as $message)
                                        <div class="alert alert-danger">
                                          <strong>{{ $message  }}</strong>
                                        </div>

                                    @endforeach
                            <div class="col-lg-12">
                                <div class="well clearfix">
                                    <a style="padding: 5px;color: white;" class="pull-right add-record">
                                        <b>Add</b>
                                    </a>
                                      
                                </div>
                                <table class="" id="tbl_posts">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_posts_body">
                                    
                                        <tr id="rec-1">
                                            <td><input required="true" type="text" name="item[0][item]" class="form-control"></td>
                                            <td><input required="true" type="number" step="any" name="item[0][price]" class="price form-control" value=""> </td>
                                            <td>
                                                <a style="display: block; padding: 7px; color: white;margin: 0px;margin-left: 10px;" class="btn btn-danger delete-record" data-id="1"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--row end-->
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="pull-right"><span>Total: $</span>&nbsp;<input name="total" id="total"class="netTotal form-control" value="0"></h4>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>

                        <div class="text-center mt-2">
                            <button class="btn btn-info" type="submit">Generate Invoice</button>
                        </div>
                    </div>    
                </form>  
            <!--invice modal end-->
            </div>
        </div>
    </div>

@endsection('content')  

@section('js')

<script type="text/javascript">

    $('.statusButton').on('change', function(){
        var value = $(this).val();
        var target = $(this).data("id");
        $.ajax({
            url: '{{ route("update-inspection-status")}}',
            data: {value: value, target:target },
            type: 'get',
            success: function(response){
                if(response.status == 200){
                     toastr.success(response.msg, 'Success');
                }
                else{
                     toastr.error('Error');
                     $('.statusButton').val(!value);
                }
            }
        });
    });

        
    $("#status").change(function(){
        var status = this.value;
        toastr.success(status);
        if(status=="0") {
            toastr.success(status);
        } else {
            toastr.success(status);
        }
    });
        
    $('.my-sort').DataTable({
        "order": [[ 4, "desc" ]]        
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#total").prop("readonly", true);
        jQuery(document).delegate('a.add-record', 'click', function(e) {
          e.preventDefault();
          var content = jQuery('#tbl_posts'),
            size = jQuery('#tbl_posts >tbody >tr').length + 1,
            element = null,
            element = $(`<tr id="rec-${size}">
                            <td><input type="text" required name="item[${size}][item]" class="form-control"></td>
                            <td><input type="number" required step="any" name="item[${size}][price]" class="price form-control" value=""> </td>
                            <td>
                                <a style="display: block; padding: 7px; color: white;margin: 0px;margin-left: 10px;" class="btn btn-danger delete-record" data-id="1"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>`);
          // element.find('.delete-record').attr('data-id', size);
          element.appendTo('#tbl_posts_body');
          // element.find('.sn').html(size);
        });
        
        jQuery(document).delegate('a.delete-record', 'click', function(e) {
          
          $(this).parents('tr').fadeOut(function () {
              $(this).remove();
              calculateInvoicePrice();
          });
        });

        calculateInvoicePrice();
    });

  function calculateInvoicePrice() {

    var netTotal = $("#tbl_posts_body").find(".price").map(function() {
      console.log(Number(this.value));
      return Number(this.value);
    }).get().reduce((a, b) => a + b, 0);
    $(".netTotal").val(netTotal);
  }

  $(document).on("change", ".price", function() {
    calculateInvoicePrice();
  })
</script>
@endsection('js')