@extends('layouts.master')
@section('title','Air Craft Works - Edit Job')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns") 


@section('content')  
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body"> 
                    <section id="configuration" class="u-c-p categorie create-job">
                        <div class="row profile-frm">
                            <div class="col-12">
                                <div class="card pro-main">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success">
                                            <strong>{{ Session::get('message')  }}</strong>
                                        </div>
                                    @endif
                                    @foreach ($errors->all() as $message)
                                        <div class="alert alert-danger">
                                          <strong>{{ $message  }}</strong>
                                        </div>

                                    @endforeach
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Edit Inspection Request</h1>
                                        </div>
                                    </div>
                                    <form  action="{{ route('update-inspection') }}" enctype="multipart/form-data" method="post" novalidate class="form">
                                    @csrf
                                        <div class="create-job-inner">
                                            <div class="row">
                                                <input type="hidden" name="inspection_id" id="inspection_id" value="{{$inspection->id}}" />
                                                <div class="col-lg-12 col-md-12 col-sm-12">

                                                    <!--<div class="form-group">
                                                        <label><i class="fa fa-th-large"></i> Inspection Date</label>
                                                        <div class="input-group date" id="datetimepicker1">
                                                            <input type="text" name="inspection_date" id="inspection_date" class="form-control">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>-->

                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-12 ">
                                                            <div class="txt">
                                                                <label for="date"><i class="fa fa-th-large"></i> Inspection Date</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-12 form-group date" id="datetimepicker1"> 
                                                            <input type="text" name="date" id="inspection_date" class="form-control" value="{{$inspection->date}}">
                                                             
                                                        </div> 
                                                    </div>
                                                    
                                                    
                                                  
                                                    
                                                      <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                                <label for="date"><i class="fa fa-th-large"></i> Inspection Time</label>
                                                            </div>
                                                        </div>
                                                          <div class="col-lg-9 col-md-8 col-sm-8 col-12 form-group time"  > 
                                                        <div class="col-12 form-group time" id="inspection_time"> 
                                                            <input type="text" name="time" id="timepicker" class="form-control" value="{{$inspection->time}}">
                                                             </diV>
                                                        </div> 
                                                    </div>
                                                    

                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                                <label for="title"><i class="fa fa-address-card"></i> Inspection Amount <strong>(&pound;)</strong></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                            <input type="text" id="amount" name="amount" class="form-control" value="{{$inspection->amount}}">
                                                        </div> 
                                                    </div>
                                                    
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                                <label for=""><i class="fa fa-commenting-o"></i> Inspection Description</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                            <textarea class="form-control" id="description" name="description" placeholder="Inspection Description">{{ $inspection->description }}</textarea>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                                <label for="status"><i class="fa fa-check-circle"></i> Inspection Status</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                            <select class="form-control" name="status" id="status">
                                                                <option {{ $inspection->status == 0 ? "selected" : "" }} value="0">Pending</option>
                                                                <option {{ $inspection->status == 1 ? "selected" : "" }} value="1">Approved</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                            
                                                            <button type="submit" class="save">Update Inspection Request</button>
                                                        </div>
                                                    </div>     
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection('content')  

@section('js')
 date
  
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />  
    <script>
        $(document).ready(function(){
            var date=new Date();
            $('#inspection_date').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd',
            });
        });
    </script>
     <script>
        $(document).ready(function(){
            $('#timepicker').timepicker({
                uiLibrary: 'bootstrap4'
            });
        });
    </script>
@endsection('js')