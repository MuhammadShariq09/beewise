@extends('layouts.master')
@section('title','Air Craft Works - Edit Job Category')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="configuration" class="u-c-p categorie">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pro-main">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success">
                                            <strong>{{ Session::get('message')  }}</strong>
                                        </div>
                                    @endif

                                    @foreach ($errors->all() as $message)
                                        <div class="alert alert-danger">
                                          <strong>{{ $message  }}</strong>
                                        </div>

                                    @endforeach
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Edit Categories </h1>
                                        </div>
                                    </div>
                                    <form  action="{{ route('update-category') }}" enctype="multipart/form-data" method="post" novalidate class="form">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10 col-sm-12">
                                            <input type="hidden" name="category_id" id="category_id" value="{{$category->id}}" />

                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-12">
                                                    <button name="file" type="button" class="uplon-btn2" onclick="document.getElementById('upload').click()">
                                                        <div class="photo-div" id='photoBG' style="background-image: url('{{ asset($category->image) }}')">
                                                            <div class="photo-div-inner">
                                                                <p><i class="fa fa-camera"></i>Add Image</p>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    <input type="file"  accept="image/*" required onchange="readURL(this, 'photoBG')" id="upload" name="category_image">
                                                </div>

                                                <div class="col-lg-7 col-md-7 col-12">
                                                    <div class="cat-text">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for="title"><i class="fa fa-th-large"></i> Category Title</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <input type="text" id="title" name="title" class="form-control" value="{{ $category->title }}">
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for="status"><i class="fa fa-check-circle"></i>Status</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <select class="form-control" name="status" id="status">
                                                                    <option {{ $category->status == 0 ? "selected" : "" }}  value="0">In-Active</option>
                                                                    <option {{ $category->status == 1 ? "selected" : "" }} value="1">Active</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for="is_featured"><i class="fa fa-check-circle"></i>Is Featured ?</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <select class="form-control" name="is_featured" id="is_featured">
                                                                    <option {{ $category->is_featured == 1 ? "selected" : "" }}  value="1">Yes</option>
                                                                    <option {{ $category->is_featured == 0 ? "selected" : "" }} value="0">No</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for=""><i class="fa fa-commenting-o"></i> Category Description</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <textarea class="form-control" id="description" name="description" placeholder="Category Description">{{ $category->description }}</textarea>
                                                                <button type="submit" class="save">Update Category</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>


@endsection('content')  

@section('js')
<!-- <script type="text/javascript">
    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+target).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script> -->
<script type="text/javascript">
    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                //$('#'+target).attr('src', e.target.result);
                $('#'+target).css('background-image', 'url(' + e.target.result + ')');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
@endsection('js')