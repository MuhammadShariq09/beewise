@extends('layouts.master')
@section('title','Job Category Listing')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
<!-- <div class="app-content content">
	<div class="content-wrapper">
		<div class="content-body"> 
			
			<section id="configuration">
				<div class="row">
					<div class="col-12">
						<div class="card rounded pad-20">
							<div class="card-content collapse show">
								<div class="card-body table-responsive card-dashboard">
									<div class="row">
										<div class="col-7">
						                    @if(Session::has('message'))
						                        <div class="alert alert-success">
						                            <strong>{{ Session::get('message')  }}</strong>
						                        </div>
						                    @endif
						                    @if(Session::has('error'))
						                        <div class="alert alert-danger">
						                          <strong>{{ Session::get('error')  }}</strong>
						                        </div>
						                    @endif
					                    </div>
                                    <div class="col-6">
                                        <h1>Job Category</h1>
                                    </div>
                                    <div class="col-6">
                                    <div class="row">
                                    	<div class="col-12"><a href="{{ route('add-category')}}" class="green-btn-project"><i class="fa fa-plus-circle"></i>Create Category</a></div>
                                        <div class="col-12">
                                        </div>
                                    </div>
                                    	
                                        
                                    </div>
                                </div>
									<div class="clearfix"></div>
									<div class="maain-tabble">
										<table class="table table-striped table-bordered zero-configuration">
											<thead>
												<tr>
													<th>ID</th>
													<th>Title</th>
                                                    <th>Description</th>
                                                    <th>Added By</th>
													<th>Added On</th>
                                                    <th style="width:13%;">Status</th>
                                                    <th>Action</th>
                                                    
												</tr>
											</thead>
											<tbody>
												<?php $i = 0;?> 
												@foreach($categories as $category) 
                        						<?php $i++ ?>
												<tr id="row{{$category->id}}">
                                                	<td>{{$i}}</td>
													<td>
                               							@if($category->image == "")
														<span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($category->title,0,1)}}</span>
														@else
														<span class="avatar avatar-online">
                                        					<img src="{{ asset($category->image) }}" alt="{{$category->title}}">
                               							</span>
                               							@endif
														{{ str_limit($category->title, $limit = 40, $end = '  ...') }}
													</td>
													<td>{{ str_limit($category->description, $limit = 60, $end = '  ...') }}</td>
													<td>{{ucfirst($category->firstName).' '.ucfirst($category->lastName)}}</td>
													<td>{{ \Carbon\Carbon::parse($category->created_at)->format('d/m/Y')}}</td>
													<td>
														<select name="status" class="form-control statusButton"  data-id="{{$category->id}}" >
						                                    <option value="0" {{ $category->status == 0 ? 'selected' : '' }}>Inactive</option>
						                                    <option value="1" {{ $category->status == 1 ? 'selected' : '' }}>Active</option>
						                                </select>
													</td>
                                                    <td>
						                                <div class="btn-group mr-1 mb-1">
						                                <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
						                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
						                                  
						                                  
						                                  <a class="dropdown-item" href="{{ route('edit-category', ['id' => $category->id])}}"><i class="fa fa-pencil-square-o"></i>Edit</a>
						                                  <a class="dropdown-item delButton"  data-id="{{$category->id}}" href="javascript:void(0)"><i class="fa fa-trash"></i>Delete</a>
						                                </div>
						                              </div>
						                            </td>
													
												</tr> 
  												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div> -->

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user category">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                        	<div class="col-12">
	                                        	@if(Session::has('message'))
							                        <div class="alert alert-success">
							                            <strong>{{ Session::get('message')  }}</strong>
							                        </div>
							                    @endif
							                    @if(Session::has('error'))
							                        <div class="alert alert-danger">
							                          <strong>{{ Session::get('error')  }}</strong>
							                        </div>
							                    @endif
						                	</div>
                                            <div class="col-sm-6 col-12">
                                                <h1 class="pull-left" style="padding-left:15px;">Categories</h1>
                                            </div>
                                            <div class="col-sm-6 col-12">
                                                <div class="add-user text-right">
                                                    <a href="{{ route('add-category')}}" class="save"><i class="fa fa-plus-circle"></i> Add category</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <!--category start here-->

                                        <div class="category-main">
                                        	<div class="row" id="load-data">
											@foreach($categories as $category) 
                                                <div class="col-lg-12 col-xl-6 col-12" id="row{{$category->id}}">
                                                    <div class="cate-box">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-12">
                                                                <img src="{{ asset($category->image) }}" alt="{{$category->title}}" class="img-full">
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-12">
                                                                <div class="cont">
                                                                    <h2>{{ $category->title }}</h2>
                                                                    <p>{{ str_limit($category->description, $limit = 300, $end = '  ...') }}</p>
                                                                    <div class="text-right ">

                                                                        <a class="delButton" data-id="{{$category->id}}" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                                                                        <a href="{{ route('edit-category', ['id' => $category->id])}}"><i class="fa fa-edit"></i></a>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach    
                                            </div>
                                            <div class="row">
											    <div id="remove-row" class="col-12">
									                <button id="btn-more" data-id="{{ $category->id }}" class="save"> Load More </button>
									            </div>
											</div>
                                        </div>
                                    </div>
                                    <!--category end here-->


                                </div>
                            </div>
                            <!--main card end-->

                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>

@endsection('content')  

@section('js')

<script type="text/javascript">

	$(document).ready(function(){
	   $(document).on('click','#btn-more',function(){
	       var id = $(this).data('id');
	       $("#btn-more").html("Loading....");
	       $.ajax({
	           url : '{{ route("ajax-categories")}}',
	           method : "POST",
	           data : {id:id, _token:"{{csrf_token()}}"},
	           dataType : "text",
	           success : function (data)
	           {
	              if(data != '') 
	              {
	                  $('#remove-row').remove();
	                  $('#load-data').append(data);
	              }
	              else
	              {
	                  $('#btn-more').html("No Data");
	              }
	           }
	       });
	   });  
	}); 

    $('.statusButton').on('change', function(){
        var value = $(this).val();
        var target = $(this).data("id");
        $.ajax({
            url: '{{ route("update-category-status")}}',
            data: {value: value, target:target },
            type: 'get',
            success: function(response){
                if(response.status == 200){
                     toastr.success(response.msg, 'Success');
                }
                else{
                     toastr.error('Error');
                     $('.statusButton').val(!value);
                }
            }
        });
    });

   
	$("#load-data").on('click', '.delButton', function () {
	    var id = $(this).data("id");
	    $.confirm({
	        theme: 'material',
	        closeIcon: true,
	        animation: 'scale',
	        type: 'red',
	        title: 'Confirm!',
	        content: 'Are you sure want to delete this category ?',
	        buttons: {
	            confirm: function () {
	                $.ajax({
	                    url: base_url + '/admin/category/delete-category/'+id,
	                    success: function(response){
	                        if(response.status == 200){
	                            $('#row'+id).remove();
	                            //alert(response.msg);
	                            toastr.success(response.msg, 'Success');
	                            //location.reload();
	                        }else{
	                            toastr.error(response.msg, 'Error');
	                        }
	                    },
	                    error:function (response) {
	                    		console.log(response);
	                            toastr.error(response.responseJSON.message, 'Error');
	                    }
	                });
	            },
	            cancel: function () {
	                //console.log('sdvsdv');
	            }
	        }
	    });
	});
        
    $("#status").change(function(){
        var status = this.value;
        toastr.success(status);
        if(status=="0") {
            toastr.success(status);
        } else {
            toastr.success(status);
        }
    });
        
    $('.my-sort').DataTable({
        "order": [[ 4, "desc" ]]        
    });

</script>
@endsection('js')