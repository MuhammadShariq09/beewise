@extends('layouts.master')
@section('title',env('APP_NAME').' - Page Listing')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-body"> 
			<!-- Basic form layout section start -->
			<section id="configuration">
				<div class="row">
					<div class="col-12">
						<div class="card rounded pad-20">
							<div class="card-content collapse show">
								<div class="card-body table-responsive card-dashboard">
									<div class="row">
										<div class="col-12">
						                    @if(Session::has('message'))
						                        <div class="alert alert-success">
						                            <strong>{{ Session::get('message')  }}</strong>
						                        </div>
						                    @endif
						                    @if(Session::has('error'))
						                        <div class="alert alert-danger">
						                          <strong>{{ Session::get('error')  }}</strong>
						                        </div>
						                    @endif
					                    </div>
                                    <div class="col-6">
                                        <h1>Orders</h1>
                                    </div>
                                    <div class="col-6">
                                    	
                                        
                                    </div>
                                </div>
									<div class="clearfix"></div>
									<div class="maain-tabble">
										<table class="table table-striped table-bordered zero-configuration">
											<thead>
												<tr>
													<th>ID</th>
													<th>Title</th>
													<th>Image</th>
                                                    <th>Description</th>
													<th>Created On</th>
                                                    <th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0;
												?>
												@foreach($pages as $page) 
                        						<?php $i++ ?>
													<tr id="row{{$page->id}}">
	                                                	<td>{{$i}}</td>
														<td>
															{{$page->title}}
														</td>
														<td><img src="{{ asset($page->image) }}" style="width:200px; height:auto" /></td>
														<td>{!! str_limit($page->description, $limit = 150, $end = '  ...') !!}</td>
														
														<td>{{ \Carbon\Carbon::parse($page->created_at)->format('d/m/Y')}}</td>
														<td>
    						                                <div class="btn-group mr-1 mb-1">
        						                                <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
        						                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
        						                                  <a class="dropdown-item" href="{{ route('edit-page', ['id' => $page->id])}}"><i class="fa fa-pencil-square-o"></i>Edit Page Data</a>
        						                                </div>
    						                                </div>
    						                            </td>
													</tr>
  												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- // Basic form layout section end --> 
		</div>
	</div>
</div>
@endsection('content')  

@section('js')

<script type="text/javascript">      
    $('.my-sort').DataTable({
        "order": [[ 4, "desc" ]]        
    });

</script>
@endsection('js')