@extends('layouts.master')
@section('title',env('APP_NAME').' - Edit Page')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="configuration" class="u-c-p categorie">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pro-main">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success">
                                            <strong>{{ Session::get('message')  }}</strong>
                                        </div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="alert alert-danger">
                                          <strong>{{ Session::get('error')  }}</strong>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Edit Company</h1>
                                        </div>
                                    </div>
                                    <form  action="{{ route('update-page') }}" enctype="multipart/form-data" method="post" novalidate class="form">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10 col-sm-12">
                                            <input type="hidden" name="page_id" id="page_id" value="{{$page->id}}" />

                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-12">
                                                    <button name="file" type="button" class="uplon-btn2" onclick="document.getElementById('upload').click()">
                                                        <div class="photo-div" id='photoBG' style="background-image: url('{{ asset($page->image) }}')">
                                                            <div class="photo-div-inner">
                                                                <p><i class="fa fa-camera"></i>Add Image</p>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    <input type="file"  accept="image/*" required onchange="readURL(this, 'photoBG')" id="upload" name="page_image">
                                                </div>

                                                <div class="col-lg-7 col-md-7 col-12">
                                                    <div class="cat-text">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for="title"><i class="fa fa-th-large"></i> Page Title</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <input type="text" id="title" name="title" class="form-control" value="{{ $page->title }}">
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for=""><i class="fa fa-commenting-o"></i> Page Description</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <textarea class="form-control" id="description" name="description" placeholder="Page Description">{{ $page->description }}</textarea>
                                                                <button type="submit" class="save">Update Page</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>


@endsection('content')  

@section('js')
<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<script type="text/javascript">CKEDITOR.replace( 'description' );</script>

<script type="text/javascript">
    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                //$('#'+target).attr('src', e.target.result);
                $('#'+target).css('background-image', 'url(' + e.target.result + ')');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection('js')