@extends('layouts.master')
@section('title',env('APP_NAME').' - Edit Profile')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('body-col',"2-columns")


@section('css')
<style>
    label i.fa {
        padding-right: 8px;
    }
</style>
@endsection
@section('content')   
   <div class="app-content content">
   <div class="content-wrapper">
      <div class="u-profile-main u-c-p-main">
         <div class="content-body">

            <!-- Basic form layout section start -->
            <section id="configuration" class="u-c-p u-profile">
               <div class="row">

                  <div class="col-7">
                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            <strong>{{ Session::get('message')  }}</strong>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                          <strong>{{ Session::get('error')  }}</strong>
                        </div>
                    @endif

                     <div class="card pro-main">
                        <div class="row">
          
                           <div class="col-md-6 col-10">
                              <h1>MY Profile </h1>
                           </div>
                            <div class="col-md-6 col-2"><div class="icon"><a class="editme" href="javascript:;"><i class="fa fa-edit"></i></a></div></div>
                        </div>
                        <div class="row">
                           <div class="col-lg-12 col-12">
                              <div class="profile-frm">
                                    <form action="{{ route('update-profile') }}"
                                          enctype="multipart/form-data" method="post" novalidate class="form">
                                        @csrf
                                 <div class="row">
                                    <div class="col-xl-3 col-lg-12 col-12">
                                       <div class="pro-img-main">
                                          <img src="{{ ($user->image) ? asset($user->image) : asset('/assets/admin/images/Profile_03.png')}}" id="user_image" alt="">
                                          <button type="button" style="margin-top: 0;display: none;" name="file" class="change-cover" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i>
                                          </button>
                                          <input type="file"  accept="image/*" required onchange="readURL(this, 'user_image')" id="upload" name="user_image">                                         
                                       </div>
                                       <p class="m">{{ucfirst($user->first_name).' '.ucfirst($user->last_name)}}</p>
                                    </div>
                                    <!--col end-->
                                    <div class="col-xl-9 col-lg-12 col-12">


                                       <div class="col-9">
                                          <!--pro inner row end-->
                                          <div >
                                              <label><i class="fa fa-user-circle"></i>First Name</label>
                                              <input disabled type="text" value="{{$user->first_name}}" id="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="" name="first_name">
                                              @if ($errors->has('first_name'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('first_name') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                          <div >
                                             <label><i class="fa fa-user-circle"></i>Last Name</label>
                                            <input disabled type="text" value="{{$user->last_name}}" id="last_name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="" name="last_name">
                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                          </div>
                                          <div>
                                             <label><i class="fa fa-envelope"></i> Email Address</label>
                                             <input type="email" disabled value="{{$user->email}}" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="" name="email">
                                             @if ($errors->has('email'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('email') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                          <!--pro inner row end-->
                                          <div>
                                             <label><i class="fa fa-key"></i>Update Password or Leave Empty</label>
                                             <input disabled type="password" id="password" class="form-control" placeholder="" name="password">
                                          </div>
                                          
                                          <div> 
                                             <label><i class="fa fa-calendar "></i>Phone number:</label>
                                             <input disabled type="text" id="phone" value="{{$user->phone}}" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }} phone" placeholder="" name="phone" maxlength="14">
                                             @if ($errors->has('phone'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('phone') }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                          <div>
                                             <label><i class="fa fa-map-marker"></i> Address</label>
                                             <input disabled type="text" value="{{ $user->address }}" id="autocomplete" onfocus="geolocate()" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="Enter your address" name="address">
                                             @if ($errors->has('address'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                              <input type="hidden" id="latitude" class="form-control" name="latitude"  value="{{$user->latitude}}">
                                              <input type="hidden" id="longitude" class="form-control" name="longitude" value="{{$user->longitude}}">
                                          </div>
                                          
                                          <div>
                                             <label><i class="fa fa-map-marker"></i> Country</label>
                                             <input disabled type="text" value="{{$user->country}}" id="country" class="form-control" placeholder="" name="country">
                                          </div>
                                          
                                          <div>
                                             <label><i class="fa fa-calendar "></i> City </label>
                                             <input disabled type="text" value="{{$user->city}}" id="locality" class="form-control" placeholder="" name="city">
                                          </div>
                                          <div>
                                             <label><i class="fa fa-calendar "></i> State </label>
                                             <input disabled type="text" value="{{$user->state}}" id="administrative_area_level_1" class="form-control" placeholder="" name="state">
                                          </div>
                                          <div>
                                             <label><i class="fa fa-calendar "></i> Zip Code </label>
                                             <input disabled type="text" value="{{$user->postal_code}}" id="postal_code" class="form-control" placeholder="" name="postal_code">
                                          </div>
                                          

                                          <!--pro inner row end-->
                                          <div class="row">
                                             <div class="col-md-12">
                                                <button style="display:none" type="submit" class="save">Save Changes</button>
                                                <button style="display:none" type="button" class="cancel">Cancel</button>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                                    </div>
                                    <!--pro inner row end-->
                                    <br>
                                 </div>
                              </div>
                              <!--row end-->
                           </div>
                           <!--profile form end-->
                        </div>
                        <!--col left end-->
                        <!-- <div class="col-lg-6 col-12"> 
                           <a class="pro-editt" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                           
                           </div>left col end-->
                     </div>
                  </div>
               </div>
         </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>
</div>

@endsection
@section('js')
    <script type="text/javascript">
        $('.change-cover').hide();
        $('.editme').on('click', function(){
           $('input').attr('disabled',false); 
           $('select').attr('disabled',false); 
           $('.save').show();
           $('.cancel').show();
          $('.change-cover').show();
        });
        
        $('.cancel').on('click', function(){
           $('input').attr('disabled',true); 
           $('select').attr('disabled',true); 
           $('.save').hide();
           $('.cancel').hide();
           $('.change-cover').hide();
        });
    
      function readURL(input, target) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  $('#'+target).attr('src', e.target.result);
              }
              reader.readAsDataURL(input.files[0]);
          }
      }

    function phoneFormatter() {
      $('.phone').on('input', function() {
        var number = $(this).val().replace(/[^\d]/g, '')
        if (number.length == 7) {
          number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
        } else if (number.length == 10) {
          number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        }
        $(this).val(number)
      });
    };
        
    $(phoneFormatter);

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        /*locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'*/

        /*street_number: 'short_name',
        route: 'long_name',*/
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.setComponentRestrictions(
            {'country': ['us']});
        autocomplete.addListener('place_changed', fillInAddress);
        /*
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
        });*/
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        console.log(place.geometry.location.lat());
        console.log(place.geometry.location.lng());
        document.getElementById("latitude").value = place.geometry.location.lat();
        document.getElementById("longitude").value = place.geometry.location.lng();
        for (var component in componentForm) {
            try{
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }catch(e){

            }
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            console.log(addressType)
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
@endsection
