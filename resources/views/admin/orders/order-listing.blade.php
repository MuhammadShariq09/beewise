@extends('layouts.master')
@section('title',env('APP_NAME').' - Orders Listing')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")

@section('content')  
    <div class="app-content content view add-operator operator-list orders">
        <div class="content-wrapper">
           <div class="content-body">
              <!-- Basic form layout section start -->
              <section id="configuration" class="search view-cause">
                 <div class="row">
                    <div class="col-12">
                       <div class="card rounded pad-20">
                          <div class="card-content collapse show">
                             <div class="card-body table-responsive card-dashboard">
                                <div class="row">
                                   <div class="col-md-6 col-12">
                                      <h1 class="pull-left">orders</h1>
                                   </div>
                                   {{-- <div class="col-xl-4 offset-xl-2 col-md-6 col-12">
                                      <div class="sort">
                                         <form action="">
                                            <div class="row">
                                               <div class="col-4 p-0">
                                                  <p>Sort by</p>
                                               </div>
                                               <div class="col-8 pl-0">
                                                  <select name="" id="" class="form-control">
                                                     <option value=""> pending</option>
                                                     <option value=""> pending</option>
                                                     <option value=""> pending</option>
                                                  </select>
                                               </div>
                                            </div>
                                         </form>
                                      </div>
                                   </div> --}}
                                </div>
                                <!--tab start here--> 
                                <ul class="nav nav-tabs nav-underline no-hover-bg">
                                   <li class="nav-item "> 
                                      <a class="nav-link active" id="base-tab31" data-toggle="tab" aria-controls="tab31" href="#tab31" aria-expanded="true">Inspection Requests</a>{{--<span class="noti">6</span>--}} 
                                   </li>
                                   <li class="nav-item"> <a class="nav-link  second-tab" id="base-tab36" data-toggle="tab" aria-controls="tab36" href="#tab36" aria-expanded="false">Pending Inspections</a> </li>
                                   <li class="nav-item"> <a class="nav-link  second-tab" id="base-tab37" data-toggle="tab" aria-controls="tab37" href="#tab37" aria-expanded="false">Active Orders</a> </li>
                                   <li class="nav-item"> <a class="nav-link  second-tab" id="base-tab38" data-toggle="tab" aria-controls="tab38" href="#tab38" aria-expanded="false">Order History</a>{{--<span class="noti">6</span>--}} </li>
                                </ul>
                                <div class="tab-content ">
                                   <div role="tabpanel" class="tab-pane active" id="tab31" aria-expanded="true" aria-labelledby="base-tab31">
                                      <div class="charges">
                                         <h4>Inspection Charges: <a href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-pencil" ></i></a></h4>
                                         @php 
                                                $amount =  Helper::getInspectionAmount();
                                            @endphp
                                         <h3>£{{$amount[0]->amount}}</h3>
                                      </div>
                                      <div class="clearfix"></div>
                                      <div class="maain-tabble">
                                         <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                               <tr>
                                                    <th>ID</th>
													<th>User Name</th>
                                                    <th>Moving From</th>
                                                    <th>Moving To</th>
                                                    <th>Moving Date</th>
                                                    <th>Inspection Date &amp; Time</th>
                                                    <th>Total Amount</th>
                                                    <th>Estimated Amount</th>
													<th>Created On</th>
                                                    <th>Action</th>
                                               </tr>
                                            </thead>
                                            <tbody>
                                                @php 
                                                    $i = 0;
                                                @endphp
                                                @foreach($inspections as $inspection) 
                                                @php 
                                                    $i++
                                                @endphp
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>
                                                            @if($inspection->invoiceOrder->quotationInvoice->user->image == "")
                                                            <span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($inspection->invoiceOrder->quotationInvoice->user->name,0,1)}}</span>
                                                            @else
                                                            <span class="avatar avatar-online">
                                                                <img src="{{ asset($inspection->invoiceOrder->quotationInvoice->user->image) }}" alt="{{ucfirst($inspection->invoiceOrder->quotationInvoice->user->name)}}">
                                                                </span>
                                                                @endif
                                                            {{ucfirst($inspection->invoiceOrder->quotationInvoice->user->name)}}
                                                        </td>
                                                        <td><a href="#" data-toggle="tooltip" title="{{ $inspection->invoiceOrder->quotationInvoice->address_from }}">{{ str_limit($inspection->invoiceOrder->quotationInvoice->address_from, $limit = 15, $end = '  ...') }}</a></td>
														<td><a href="#" data-toggle="tooltip" title="{{ $inspection->invoiceOrder->quotationInvoice->address_to }}">{{ str_limit($inspection->invoiceOrder->quotationInvoice->address_to, $limit = 15, $end = '  ...') }}</a></td>
                                                        <td>{{ Carbon\Carbon::parse($inspection->invoiceOrder->quotationInvoice->moving_date)->toDayDateTimeString() }}</td>
                                                        <td>{{\Carbon\Carbon::parse($inspection->date)->format('d/m/Y').' '.$inspection->time}}</td>
                                                        <td>£{{ $inspection->invoiceOrder->total }}</td>
                                                        <td>£{{ $inspection->invoiceOrder->estimated_to }} - £{{ $inspection->invoiceOrder->estimated_from }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($inspection->created_at)->format('d/m/Y')}}</td>
                                                        <td>
                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;"> 
                                                                    <a class="dropdown-item" href="{{ route('edit-inspection-request', ['id' => $inspection->id])}}"><i class="fa fa-eye"></i>View </a>
                                                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-trash"></i>delete </a>  
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                         </table>
                                      </div>
                                   </div>
                                   

                                    <!--tab 2 START-->
                                    <div class="tab-pane " id="tab36" aria-labelledby="base-tab36">
                                        <div class="clearfix"></div>
                                        <div class="maain-tabble">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>User Name</th>
                                                        <th>Moving From</th>
                                                        <th>Moving To</th>
                                                        <th>Moving Date</th>
                                                        <th>Inspection Date &amp; Time</th>
                                                        <th>Inspection Charges</th>
                                                        <th>Total Amount</th>
                                                        <th>Estimated Amount</th>
                                                        <th>Created On</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php 
                                                        $i = 0;
                                                    @endphp
                                                    @foreach($pendingInspections as $pendingInspection) 
                                                    @php 
                                                        $i++
                                                    @endphp
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>
                                                                @if($pendingInspection->invoiceOrder->quotationInvoice->user->image == "")
                                                                <span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($pendingInspection->invoiceOrder->quotationInvoice->user->name,0,1)}}</span>
                                                                @else
                                                                <span class="avatar avatar-online">
                                                                    <img src="{{ asset($pendingInspection->invoiceOrder->quotationInvoice->user->image) }}" alt="{{ucfirst($pendingInspection->invoiceOrder->quotationInvoice->user->name)}}">
                                                                    </span>
                                                                    @endif
                                                                {{ucfirst($pendingInspection->invoiceOrder->quotationInvoice->user->name)}}
                                                            </td>
                                                            <td><a href="#" data-toggle="tooltip" title="{{ $pendingInspection->invoiceOrder->quotationInvoice->address_from }}">{{ str_limit($pendingInspection->invoiceOrder->quotationInvoice->address_from, $limit = 15, $end = '  ...') }}</a></td>
                                                            <td><a href="#" data-toggle="tooltip" title="{{ $pendingInspection->invoiceOrder->quotationInvoice->address_to }}">{{ str_limit($pendingInspection->invoiceOrder->quotationInvoice->address_to, $limit = 15, $end = '  ...') }}</a></td>
                                                            <td>{{ Carbon\Carbon::parse($pendingInspection->invoiceOrder->quotationInvoice->moving_date)->toDayDateTimeString() }}</td>
                                                            <td>{{\Carbon\Carbon::parse($pendingInspection->date)->format('d/m/Y').' '.$pendingInspection->time}}</td>
                                                            <td>
                                                                @if($pendingInspection->is_paid == "1")
                                                                    Paid
                                                                @endif
                                                            </td>
                                                            <td>£{{ $pendingInspection->invoiceOrder->total }}</td>
                                                            <td>£{{ $pendingInspection->invoiceOrder->estimated_to }} - £{{ $inspection->invoiceOrder->estimated_from }}</td>
                                                            <td>{{ \Carbon\Carbon::parse($pendingInspection->created_at)->format('d/m/Y')}}</td>
                                                            <td>
                                                                <div class="btn-group mr-1 mb-1">
                                                                    <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;"> 
                                                                        <a class="dropdown-item" href="{{ route('view-pending-inspection', ['id' => $pendingInspection->id])}}"><i class="fa fa-eye"></i>View </a>
                                                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-trash"></i>delete </a>  
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                     
                                   <!--tab 2 end-->
                                   <div class="tab-pane " id="tab37" aria-labelledby="base-tab37">
                                      <div class="clearfix"></div>
                                      <div class="maain-tabble">
                                         <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                               <tr>
                                                  <th>ID</th>
                                                  <th>User Name</th>
                                                  <th>Moving From</th>
                                                  <th>Moving To</th>
                                                  <th>Moving Date</th>
                                                  <th>total cost</th>
                                                  <th>user Payment status</th>
                                                  <th>Created On</th>
                                                  <th>action</th>
                                               </tr>
                                            </thead>
                                            <tbody>
                                                @php 
                                                    $i = 0;
                                                @endphp
                                                @foreach($orders as $order) 
                                                @php 
                                                    $i++
                                                @endphp
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>
                                                            @if($order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->user->image == "")
                                                            <span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->user->name,0,1)}}</span>
                                                            @else
                                                            <span class="avatar avatar-online">
                                                                <img src="{{ asset($order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->user->image) }}" alt="{{ucfirst($order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->user->name)}}">
                                                                </span>
                                                                @endif
                                                            {{ucfirst($order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->user->name)}}
                                                        </td>
                                                        <td><a href="#" data-toggle="tooltip" title="{{ $order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->address_from }}">{{ str_limit($order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->address_from, $limit = 15, $end = '  ...') }}</a></td>
                                                        <td><a href="#" data-toggle="tooltip" title="{{ $order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->address_to }}">{{ str_limit($order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->address_to, $limit = 15, $end = '  ...') }}</a></td>
                                                        <td>{{ Carbon\Carbon::parse($order->InspectionPayment->inspectionRequest->invoiceOrder->quotationInvoice->moving_date)->toDayDateTimeString() }}</td>
                                                        <td>£{{ $order->total }}</td>
                                                        <td>
                                                            @if($order->is_paid == "1")
                                                                Paid
                                                            @else
                                                                Pending
                                                            @endif
                                                        </td>
                                                        <td>{{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y')}}</td>
                                                        <td>
                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;"> 
                                                                <a class="dropdown-item" href="a-active-orders.html"><i class="fa fa-eye"></i>View </a> 
                                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-trash"></i>delete </a> 
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                               @endforeach
                                            </tbody>
                                         </table>
                                      </div>
                                   </div>
                                   <!--tab 3 end-->
                                   <div class="tab-pane " id="tab38" aria-labelledby="base-tab38">
                                      <div class="clearfix"></div>
                                      <div class="maain-tabble">
                                         <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                               <tr>
                                                  <th>Order id</th>
                                                  <th>User Name</th>
                                                  <th>date</th>
                                                  <th>contact</th>
                                                  <th>moving date</th>
                                                  <th>EST cost</th>
                                                  <th>status</th>
                                                  <th>action</th>
                                               </tr>
                                            </thead>
                                            <tbody>
                                               <tr>
                                                  <td>4132</td>
                                                  <td><span data-toggle="popover" data-content="johny" class="circle" style="background: #f61454;">T</span>John Williams</td>
                                                  <td>15 April 2019</td>
                                                  <td>123-6655-9874</td>
                                                  <td>12-August-2018</td>
                                                  <td>£3000</td>
                                                  <td>completed</td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                        <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;"> 
                                                           <a class="dropdown-item" href="a-order-history.html"><i class="fa fa-eye"></i>View </a> 
                                                           <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-trash"></i>delete </a> 
                                                        </div>
                                                     </div>
                                                  </td>
                                               </tr>
                                            </tbody>
                                         </table>
                                      </div>
                                   </div>
                                   <!--tab 4 end-->
                                   <!--delete modal start here-->
                                   <div class="login-fail-main user delete-modal">
                                      <div class="featured inner">
                                         <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                     <span aria-hidden="true">&times;</span>
                                                     </button>
                                                  </div>
                                                  <form action="">
                                                     <div class="payment-modal-main">
                                                        <div class="payment-modal-inner">
                                                           <div class="row">
                                                              <div class="col-12 text-center ">
                                                                 <h1>are you sure you want to delete?</h1>
                                                              </div>
                                                           </div>
                                                           <div class="row">
                                                              <div class="col-12 text-center">
                                                                 <button type="button" class="con" data-dismiss="modal">yes</button>
                                                                 <button type="button" class="can" data-dismiss="modal">no</button>
                                                              </div>
                                                           </div>
                                                        </div>
                                                     </div>
                                                  </form>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                   <!--delete modal end here-->
                                   <!--inspection charges modal start here-->
                                   <div class="login-fail-main delete-modal user">
                                      <div class="featured inner">
                                         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                               <div class="modal-content">
                                                    <div class="alert alert-success" style="display:none"></div>
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                     <span aria-hidden="true">&times;</span>
                                                     </button>
                                                  </div>
                                                    <form  id="edit-amount-form" action="{{ route('update-amount') }}" method="post" novalidate  class="form">
                                                     <div class="payment-modal-main" >
                                                        <div class="payment-modal-inner">
                                                           <div class="row">
                                                              <div class="col-12 text-center mb-4">
                                                                 <h1>Edit Inspection Amount</h1>
                                                              </div>
                                                           </div>
                                                           <div class="row form-group">
                                                              <div class="col-md-12 ">
                                                                @php 
                                                                    $amount =  Helper::getInspectionAmount();
                                                                @endphp 
                                                                 <label for="">inspection charges (in £) </label>
                                                              </div>
                                                              <div class="col-12">
                                                                <input type="hidden" value="{{$amount[0]->id}}" name="amount_id" id="amount_id" >
                                                                <input type="number" value="{{$amount[0]->amount}}" name="amount" id="amount" placeholder="Enter inspection charges" class="form-control">
                                                              </div>
                                                           </div>
                                                           <div class="row">
                                                              <div class="col-12 text-center">
                                                                 <button type="button" class="con" data-dismiss="modal">cancel</button>
                                                                 <button  type="submit" id="ajaxSubmit" class="can" data-dismiss="modal">update</button>
                                                              </div>
                                                           </div>
                                                        </div>
                                                     </div>
                                                    </form>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                   <!--add user modal end here--> 
                                   <!--inspection charges modal end here-->
                                </div>
                                <!--tab end here--> 
                             </div>
                          </div>
                          <!--main card end--> 
                       </div>
                    </div>
                 </div>
              </section>
              <!-- // Basic form layout section end --> 
           </div>
        </div>
     </div>
@endsection('content')

@section('js')

<script>
    $(document).ready(function(){
        $('#ajaxSubmit').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let fd = new FormData(document.getElementById('edit-amount-form'));
            fd.append('amount' , $('#amount').val());
            fd.append('id' , $('#amount_id').val());

            $.ajax({
                url: "{{ route('update-amount') }}",
                method: 'post',
                data: fd,
                processData: false,
                contentType: false,
                //data: $('#add-user-form').serialize(),
                success: function(result){
                    console.log(result);
                    if(result.errors)
                    {

                        $('.alert-danger').html('');

                        $.each(result.errors, function(key, value){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>'+value+'</li>');
                        });
                    }
                    else
                    {
                        $('.alert-success').html('');
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.alert-success').append('<li>'+result.success+'</li>');
                         window.location.reload(true);

                    }
                },
                error: function(e){
                    $('.alert-danger').html('');
                    //console.log(e);
                    var keys = Object.keys(e.responseJSON.errors);

                    $.each(keys, function(index, key){
                        $('.alert-danger').show();
                        $('.alert-danger').append(`<li>${e.responseJSON.errors[key][0]}</li>`);
                    });
                }
            });
        });
    });
</script>
@endsection('js')