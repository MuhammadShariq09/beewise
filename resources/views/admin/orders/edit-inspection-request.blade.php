@extends('layouts.master')
@section('title', env('APP_NAME').' - Edit Inspection Request')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")
@section('css')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection
@section("content")

<div class="app-content content view order-re create-qu">
        <div class="content-wrapper">
          <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration" class="search view-cause">
              <div class="row">
                <div class="col-12">
                  <div class="card rounded pad-20">
                    <div class="card-content collapse show">
                      <div class="card-body table-responsive card-dashboard">
                        <div class="row">
                          <div class="col-md-6 col-12">
                            <h1 class="pull-left"><a href="{{ route('invoice-orders') }}#tab31"><i class="fa fa-angle-left"></i></a>INSPECTION REQUEST DETAILS</h1>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12">
                            <h2>Order ID: <span>{{$inspection[0]->invoiceOrder->quotationInvoice->order_num}}</span></h2>
                          </div>
                        </div>
                         {{-- <div class="row">
                          <div class="col-xl-10 col-12">
                            <div class="row">
                                @foreach($inspection[0]->invoiceOrder->quotationInvoice->images as $image)
                                    <div class="col-sm-auto col-12"> <img src="{{ asset($image->image) }}" class="img-fluid" alt=""> </div>
                                @endforeach

                            </div>
                          </div>
                        </div> --}}

                        <div class="row">
                            <div class="col-xl-10 col-12">
                                <div class="row">
                                    @foreach($inspection[0]->invoiceOrder->quotationInvoice->images as $image)

                                        <div class="col-sm-auto col-12 thumb">
                                        <a class="thumbnail" href="#" data-image-id="{{$image->id}}" data-toggle="modal" data-height="500" data-image="{{ asset($image->image) }}" data-target="#image-gallery{{$image->id}}">
                                              <img class="img-thumbnail" src="{{ asset($image->image) }}" alt="Another alt text">
                                            </a>
                                        </div>


                                        <div class="modal fade img-view" id="image-gallery{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                              <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h4 class="modal-title" id="image-gallery-title"></h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span> </button>
                                                    </div>
                                                    <div class="modal-body"> <img id="image-gallery-image" class="img-responsive col-md-12" src=""> </div>
                                                    <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i> </button>
                                                    <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i> </button>
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                                <div class="col-xl-10 col-12">
                                    <div class="order-inspection-top">
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6 col-12">
                                                <p>Inspection Date &amp; Time</p>
                                                <h6>{{\Carbon\Carbon::parse($inspection[0]->date)->format('d/m/Y').' '.$inspection[0]->time}}</h6>
                                            </div>
                                            <div class="col-xl-3 col-md-6 col-12">
                                                <p>Moving Date </p>
                                                <h6>{{ Carbon\Carbon::parse($inspection[0]->invoiceOrder->quotationInvoice->moving_date)->toDayDateTimeString() }}</h6>
                                            </div>
                                            <div class="col-xl-3 col-md-6 col-12 text-left">
                                                <button data-toggle="modal" data-target="#rescheduleModal"><i class="fa fa-repeat"></i>Reschedule</button>
                                            </div>
                                        </div>
                                    </div>
                                
                            <div class="row">
                              <div class="col-12">
                                <div class="order-inspection-middle-main">
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Username:</p>
                                      <h6>{{ ucfirst($inspection[0]->invoiceOrder->quotationInvoice->user->name) }}</h6>
                                      <p>Contact:</p>
                                      <h6>{{$inspection[0]->invoiceOrder->quotationInvoice->user->phone}}</h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Floor</p>
                                          <p>Levels</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$inspection[0]->invoiceOrder->quotationInvoice->floors_from}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Email:</p>
                                      <h6>{{$inspection[0]->invoiceOrder->quotationInvoice->user->email}}</h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Flagile</p>
                                          <p>Items</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$inspection[0]->invoiceOrder->quotationInvoice->fragile_items}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Property Type:</p>
                                      <h6>
                                            @if($inspection[0]->invoiceOrder->quotationInvoice->property_type_from == "1")
                                                Bungalow
                                            @elseif($inspection[0]->invoiceOrder->quotationInvoice->property_type_from == "2")
                                                Apartment
                                            @elseif($inspection[0]->invoiceOrder->quotationInvoice->property_type_from == "3")
                                                Cottage
                                            @elseif($inspection[0]->invoiceOrder->quotationInvoice->property_type_from == "4")
                                                House
                                            @else
                                                Mansion
                                            @endif
                                      </h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Packaging</p>
                                          <p>Items</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$inspection[0]->invoiceOrder->quotationInvoice->packaging_items}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <form  action="{{url('/admin/quotations/submit-quotation-invoice/')}}" method="post" id="invoice-form">
                                    @csrf
                                    <input type="hidden" id="quotation_id" name="quotation_id" value="{{$inspection[0]->invoiceOrder->quotationInvoice->id}}">
                                    <input type="hidden" id="status" name="status" value="1">
                                <div class="order-inspection-bottom">
                                  <div class="row">
                                    <div class="col-12">
                                      <h4>Item List &amp; Costing</h4>
                                    </div>
                                  </div>
                                  <div class="maain-tabble" id="#tbl_posts_body">
                                       
                                    <table class="table table-striped table-bordered zero-configuration">
                                      <thead>
                                        <tr>
                                          <th>Items</th>
                                          <th>Quantity</th>
                                          <th>Cost Item <span>(Per Time)</span></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                            @php 
                                                $invoiceItems =  Helper::getQuotationItems($inspection[0]->invoiceOrder->quotationInvoice->id);
                                                //dd($invoiceItems);
                                                $i = 0; 
                                            @endphp
                                            @if(count($invoiceItems) > 0)
                                                @foreach($invoiceItems as $invoiceItem)
                                                    @foreach($invoiceItem->items as $inv)   
                                                        <tr id="hello">
                                                            <td><input readonly type="text" name="items[{{$i}}][item]" class="form-control" value="{{$inv->item}}"></td>
                                                            <td><input readonly type="number" name="items[{{$i}}][quantity]" class="form-control" value="{{$inv->quantity}}"> </td>
                                                            <td class="">
                                                                <input readonly name="items[{{$i}}][price]" type="number" class="price"  value="{{$inv->price}}">
                                                                <span class="text-success float-right">
                                                                <i class="fa fa-check-circle "></i></span>
                                                            </td>
                                                        </tr>
                                                        @php 
                                                            $i++;
                                                        @endphp
                                                    @endforeach    
                                                @endforeach
                                            @endif    
                                      </tbody>
                                    </table>
                                        @if($inspection[0]->invoiceOrder->quotationInvoice->status == "0")     
                                            <button type="button" id="btn"><i class="fa fa ft-plus-circle"></i> Add New</button>
                                        @endif
                                   </div>


                                  <div class="row">
                                    <div class="col-12">
                                      <div class="items-main">
                                            @php 
                                            $invoiceDetails =  Helper::getQuotationDetails($inspection[0]->invoiceOrder->quotationInvoice->id);
                                            $i = 0; 
                                        @endphp
                                        @if(count($invoiceDetails) > 0)
                                            @foreach($invoiceDetails as $invoiceDetail)
                                                @foreach($invoiceDetail->invoiceOrderDetails as $inv)  
                                                    <div class="item-top ">
                                                        <p>{{$inv->item}}:</p>
                                                        <input type="hidden" name="details[{{$i}}][item]" value="{{$inv->item}}" />
                                                        <input readonly name="details[{{$i}}][quantity]" type="number" placeholder="item quantity" value="{{$inv->quantity}}">
                                                        <input readonly name="details[{{$i}}][price]" type="number" class="price" placeholder="cost" value="{{$inv->price}}">
                                                    </div>
                                                    @php 
                                                        $i++;
                                                    @endphp
                                                @endforeach    
                                            @endforeach    
                                        @endif



                                        <div class="item-bottom d-flex justify-content-between">
                                          <div class="">
                                            <h4>Estimated Cost:</h4>
                                            @php 
                                                $invoices =  Helper::getQuotationInvoice($inspection[0]->invoiceOrder->quotationInvoice->id);
                                                $i = 1; 
                                            @endphp
                                            @if(count($invoices) > 0)
                                                <div class="cost-box">
                                                    <input readonly name="estimated_to" type="number" placeholder="Cost" value="{{$invoices[0]->estimated_to}}">
                                                    <p>to</p>
                                                    <input readonly name="estimated_from" type="number" placeholder="Cost" value="{{$invoices[0]->estimated_from}}">
                                                </div>   
                                            @else    
                                                <div class="cost-box">
                                                    <input name="estimated_to" type="number" placeholder="Cost">
                                                    <p>to</p>
                                                    <input  name="estimated_from" type="number" placeholder="Cost">
                                                </div>
                                              @endif  
                                          </div>
                                          <div class="">

                                            <h6>Total Cost: <input readonly class="netTotal" name="total" /></h6>
                                          </div>
                                        </div>
                                           @if($inspection[0]->invoiceOrder->quotationInvoice->status == "0") 
                                          <div class="row">
                                              <div class="col-12 text-left">

                                              <button type="submit">Send quotation</button>
                                              </div>
                                          </div>
                                          @endif
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- Basic Column Chart -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>



<!-- Modal -->
<div class="modal fade" id="rescheduleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" style="padding: 0px; !important">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="alert alert-danger" style="display:none"></div>
                <div class="alert alert-success" style="display:none"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <h2>Reschedule Time &amp; Date</h2>
                <form   id="reschedule-inspection-form" action="{{ route('update-inspection') }}" method="post" novalidate  class="form">
                    @csrf
                @php 
                    $amount =  Helper::getInspectionAmount();
                @endphp 
                    <input type="hidden" value="{{$amount[0]->amount}}" name="amount" id="amount" >
                    <input type="hidden" value="{{$inspection[0]->id}}" name="inspection_id" id="inspection_id" />
                    <input type="hidden" value="1" name="status" id="status" />
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="">Inspection Date</label>
                        <input type="text" id="datepicker-6" class="form-control" placeholder="" name="date" value="{{$inspection[0]->date}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="">Inspection time</label>
                        <div class="row">
                            <div class="col-9 pr-0">
                            <input  type="text" name="time" id="timepicker" value="{{$inspection[0]->time}}"  class="form-control">
                            </div>
                            <div class="col-3 pl-0">
                            <select class="form-control">
                                <option value="">AM</option>
                                <option value="">PM</option>
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="">Inspection Decription</label>
                        <textarea id="description" class="form-control" placeholder="" name="description" >{{$inspection[0]->description}}</textarea>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="ajaxSubmit" class="btn btn-secondary" data-dismiss="modal">Reschedule Inspection Time</button>
            </div>
        </div>
    </div>
</div>
<!--modal end here--> 
@endsection
        
    @section('js')
    <script src="{{ asset('assets/admin/assets/js/light.js') }}"></script>
    <script src="{{ asset('assets/admin/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/app-assets/js/scripts/forms/form-repeater.js') }}" type="text/javascript"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> 

    <script src="{{ asset('assets/admin/assets/js/function.js') }}" type="text/javascript"></script>
    <script>

    $(document).ready(function(){
        $('#ajaxSubmit').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let fd = new FormData(document.getElementById('reschedule-inspection-form'));
            fd.append('amount' , $('#amount').val());
            fd.append('date' , $('#datepicker-6').val());
            fd.append('time' , $('#timepicker').val());
            fd.append('description' , $('#description').val());

            $.ajax({
                url: "{{ route('update-inspection') }}",
                method: 'post',
                data: fd,
                processData: false,
                contentType: false,
                //data: $('#add-user-form').serialize(),
                success: function(result){
                    console.log(result);
                    if(result.errors)
                    {

                        $('.alert-danger').html('');

                        $.each(result.errors, function(key, value){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>'+value+'</li>');
                        });
                    }
                    else
                    {
                        $('.alert-success').html('');
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.alert-success').append('<li>'+result.success+'</li>');
                         window.location.reload(true);

                    }
                },
                error: function(e){
                    $('.alert-danger').html('');
                    //console.log(e);
                    var keys = Object.keys(e.responseJSON.errors);

                    $.each(keys, function(index, key){
                        $('.alert-danger').show();
                        $('.alert-danger').append(`<li>${e.responseJSON.errors[key][0]}</li>`);
                    });
                }
            });
        });
    });
    </script>

@endsection
