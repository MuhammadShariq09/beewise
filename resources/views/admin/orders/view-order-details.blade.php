@extends('layouts.master')
@section('title', env('APP_NAME').' - View Quotation Invoice Details')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns") 


@section('content')  
<div class="app-content content view order-re ">
        <div class="content-wrapper">
          <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration" class="search view-cause">
              <div class="row">
                <div class="col-12">
                  <div class="card rounded pad-20">
                    <div class="card-content collapse show">
                      <div class="card-body table-responsive card-dashboard">
                        <div class="row">
                          <div class="col-md-6 col-12">
                            <h1 class="pull-left"><a href="{{ route('invoice-orders') }}#tab31"><i class="fa fa-angle-left"></i></a>INSPECTION REQUEST DETAILS</h1>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12">
                            <h2>Order ID: <span>{{$inspection[0]->invoiceOrder->quotationInvoice->order_num}}</span></h2>
                          </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-10 col-12">
                                <div class="row">
                                    @foreach($inspection[0]->invoiceOrder->quotationInvoice->images as $image)

                                        <div class="col-sm-auto col-12 thumb">
                                        <a class="thumbnail" href="#" data-image-id="{{$image->id}}" data-toggle="modal" data-height="500" data-image="{{ asset($image->image) }}" data-target="#image-gallery{{$image->id}}">
                                              <img class="img-thumbnail" src="{{ asset($image->image) }}" alt="Another alt text">
                                            </a>
                                        </div>


                                        <div class="modal fade img-view" id="image-gallery{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h4 class="modal-title" id="image-gallery-title"></h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span> </button>
                                                    </div>
                                                    <div class="modal-body"> <img id="image-gallery-image" class="img-responsive col-md-12" src=""> </div>
                                                    <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i> </button>
                                                    <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i> </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        
                        
                        <!--image viewer modal end here-->
                      <div class="main-pending">
                        <div class="row">
                          <div class="col-xl-9 col-12">
                             <div class="row">
                                <div class="col-xl-3 col-md-6 col-12">
                                    <p>Inspection Date &amp; Time</p>
                                  <h6>{{\Carbon\Carbon::parse($inspection[0]->date)->format('d/m/Y').' '.$inspection[0]->time}}</h6>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12">
                                  <p>Moving Date</p>
                                  <h6>{{ Carbon\Carbon::parse($inspection[0]->invoiceOrder->quotationInvoice->moving_date)->toDayDateTimeString() }}</h6>
                                </div>
                                {{-- <div class="col-xl-3 col-md-6 col-12">
                                  <p>Moving date </p>
                                  <h6>20-May-2019</h6>
                                </div> --}}
                                 
                              </div>
                                 
                            <div class="row">
                                
                              <div class="col-12">
                                <div class="order-inspection-middle-main">
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Username:</p>
                                      <h6>{{ ucfirst($inspection[0]->invoiceOrder->quotationInvoice->user->name) }}</h6>
                                      <p>Contact:</p>
                                      <h6>{{$inspection[0]->invoiceOrder->quotationInvoice->user->phone}}</h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Floor</p>
                                          <p>Levels</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$inspection[0]->invoiceOrder->quotationInvoice->floors_from}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Email:</p>
                                      <h6>{{$inspection[0]->invoiceOrder->quotationInvoice->user->email}}</h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Flagile</p>
                                          <p>Items</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$inspection[0]->invoiceOrder->quotationInvoice->fragile_items}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="order-inspection-middle d-sm-flex d-block justify-content-between align-items-center">
                                    <div class="left">
                                      <p>Property Type:</p>
                                      <h6>
                                        @if($inspection[0]->invoiceOrder->quotationInvoice->property_type_from == "1")
                                            Bungalow
                                        @elseif($inspection[0]->invoiceOrder->quotationInvoice->property_type_from == "2")
                                            Apartment
                                        @elseif($inspection[0]->invoiceOrder->quotationInvoice->property_type_from == "3")
                                            Cottage
                                        @elseif($inspection[0]->invoiceOrder->quotationInvoice->property_type_from == "4")
                                            House
                                        @else
                                            Mansion
                                        @endif
                                      </h6>
                                    </div>
                                    <div class="right">
                                      <div class="box justify-content-between d-flex align-items-center">
                                        <div class="">
                                          <p>Packaging</p>
                                          <p>Items</p>
                                        </div>
                                        <div class="">
                                          <h3>{{$inspection[0]->invoiceOrder->quotationInvoice->packaging_items}}</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="order-inspection-bottom">
                                 
                                 
                                  <div class="row">
                                    <div class="col-12">
                                      <div class="items-main">
                                        @foreach($inspection[0]->invoiceOrder->invoiceOrderDetails as $details) 
                                            <div class="item-top d-flex justify-content-between align-items-center">                                     
                                                <p>{{$details->item}}: <span>{{$details->quantity}}</span></p>  
                                                <p>cost: <span>£{{$details->price}}</span></p>
                                            </div>
                                        @endforeach
                                        {{-- <div class="item-top d-flex justify-content-between align-items-center">
                                          <p>Packaging: <span>15</span></p>
                                          <p>cost: <span>£150</span></p>
                                        </div> --}}
                                        <div class="item-bottom d-flex justify-content-between align-items-center">
                                          <div class="">
                                            <h4>Estimated Cost:</h4>
                                            @php
                                                $invoices =  Helper::getQuotationInvoice($inspection[0]->invoiceOrder->quotationInvoice->id);
                                            @endphp
                                            <h5>£{{$invoices[0]->estimated_to}} - £{{$invoices[0]->estimated_from}}</h5>
                                          </div>
                                         
                                        </div>
                                        {{-- <a href="a-create-invoice.html">Create Invoice</a> --}}
                                      </div>

                                      <form  action="{{url('/admin/invoice-orders/generate-order/')}}" method="post" id="invoice-form">
                                        @csrf
                                        <input type="hidden" id="inspection_payment_id" name="inspection_payment_id" value="{{$inspection[0]->payment->id}}">
                                        <input type="hidden" id="status" name="status" value="1">

                                      <div class="order-inspection-bottom">
                                            <div class="row">
                                              <div class="col-12">
                                                 <h4>Item List &amp; Costing</h4>
                                              </div>
                                            </div>
                                            <div class="maain-tabble">
                                                <form action="">
                                                    <table class="table table-striped table-bordered zero-configuration">
                                                        <thead>
                                                            <tr>
                                                                <th>Items</th>
                                                                <th>Quantity</th>
                                                                <th>Cost Item <span>(Per Item)</span></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr id="hello">
                                                                <td><input required="true" name="items[0][item]" type="text" class="form-control" placeholder="dinning table "></td>
                                                                <td><input required="true" name="items[0][quantity]" type="number" class="form-control" placeholder="2"></td>
                                                                <td class="">
                                                                    <input required="true" name="items[0][price]" type="number" class="price form-control" placeholder="£80">
                                                                    <span class="text-success float-right">
                                                                    <i class="fa fa-check-circle "></i></span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </form>
                                                <button id="btn"><i class="fa fa ft-plus-circle"></i> Add New</button>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="items-main">
                                                        <div class="item-top ">
                                                            <p>Flagile Items:</p>
                                                            <input type="hidden" name="details[0][item]" value="Flagile Items" />
                                                            <input name="details[0][quantity]" type="number" placeholder="item quantity">
                                                            <input name="details[0][price]" type="number" class="price" placeholder="cost">
                                                        </div>
                                                        <div class="item-top ">
                                                            <p>packaging Items: </p>
                                                            <input type="hidden"  name="details[1][item]" value="Packaging Items" />
                                                            <input name="details[1][quantity]" type="number" placeholder="item quantity">
                                                            <input name="details[1][price]" type="number" class="price" placeholder="cost">
            
                                                        </div>
                                                        <div class="item-bottom d-sm-flex d-block justify-content-between">
                                                            <div class="">
                                                            </div>
                                                            <div class="">
                                                                <p>Subtotal: <input readonly class="netTotal" id=sub_total name="sub_total" /></p>
                                                                <p>Service Charges: <input class="" type="number" id=service_charges name="service_charges" /></p>
                                                                <h6>Total Cost: <input readonly class="" id="total" name="total" /></h6>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <button type="submit" data-toggle="modal" data-target="#exampleModalCenter">Send Invoice</button>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                        <!-- Basic Column Chart --> 
                      </div>
                    </div>
                  </div>
                </section>
              </div>
          </div>
          </div>
      

@endsection('content')  

@section('js')
<script src="{{ asset('assets/admin/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/app-assets/js/scripts/forms/form-repeater.js') }}" type="text/javascript"></script>
<script>
    var counter = 0;
    $(document).ready(function(){  
        $("#btn").click(function(){   
            counter++;
            $("#hello").after(`<tr>
                                    <td><input name="items[${counter}][item]" type='text' class='' placeholder='dinning table '></td> 
                                    <td><input name="items[${counter}][quantity]" type='number' class='' placeholder='2'></td> 
                                    <td><input name="items[${counter}][price]" type='number' class='price' placeholder='£80'></td> 
                            </tr>`);  
        });  

        calculateInvoicePrice();
    });

    function calculateInvoicePrice() {
        var netTotal = $('.price').map(function(index, item){ 
            return $(item).val();
        }).get().reduce((a, b) => Number(a) + Number(b), 0);

        // var netTotal = $("#tbl_posts_body").find(".price").map(function() {
        //     console.log(Number(this.value));
        //     return Number(this.value);
        // }).get().reduce((a, b) => a + b, 0);
        $(".netTotal").val("£" + netTotal);
    }

    $(document).on("change", ".price", function() {
        calculateInvoicePrice();
    })


    $('#service_charges').on('input', function(){
        let serviceCost = parseInt($(this).val().replace(/[^0-9.-]+/, ''));
        let subtotal = parseInt($('#sub_total').val().replace(/[^0-9.-]+/, ''));
        $('#total').val(subtotal + serviceCost);
    })

</script>
@endsection