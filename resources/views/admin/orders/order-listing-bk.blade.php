@extends('layouts.master')
@section('title',env('APP_NAME').' - Orders Listing')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-body"> 
			<!-- Basic form layout section start -->
			<section id="configuration">
				<div class="row">
					<div class="col-12">
						<div class="card rounded pad-20">
							<div class="card-content collapse show">
								<div class="card-body table-responsive card-dashboard">
									<div class="row">
										<div class="col-12">
						                    @if(Session::has('message'))
						                        <div class="alert alert-success">
						                            <strong>{{ Session::get('message')  }}</strong>
						                        </div>
						                    @endif
						                    @if(Session::has('error'))
						                        <div class="alert alert-danger">
						                          <strong>{{ Session::get('error')  }}</strong>
						                        </div>
						                    @endif
					                    </div>
                                    <div class="col-6">
                                        <h1>Orders</h1>
                                    </div>
                                    <div class="col-6">
                                    	
                                        
                                    </div>
                                </div>
									<div class="clearfix"></div>
									<div class="maain-tabble">
										<table class="table table-striped table-bordered zero-configuration">
											<thead>
												<tr>
													<th>ID</th>
													<th>User Name</th>
                                                    <th>Moving From</th>
                                                    <th>Moving To</th>
                                                    <th>Moving Date</th>
                                                    <th>Total Amount</th>
                                                    <th>Estimated Amount</th>
													<th>Created On</th>
                                                    <th style="width:10%;">Status</th>
                                                    <th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0;
												?>
												@foreach($orders as $order) 
                        						<?php $i++ ?>
													<tr id="row{{$order->id}}">
	                                                	<td>{{$i}}</td>
														<td>
															@if($order->quotationInvoice->user->image == "")
															<span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($order->quotationInvoice->user->name,0,1)}}</span>
															@else
															<span class="avatar avatar-online">
	                                        					<img src="{{ asset($order->quotationInvoice->user->image) }}" alt="{{ucfirst($order->quotationInvoice->user->name)}}">
	                               							</span>
	                               							@endif
															{{ucfirst($order->quotationInvoice->user->name)}}
														</td>
														<td><a href="#" data-toggle="tooltip" title="{{ $order->quotationInvoice->address_from }}">{{ str_limit($order->quotationInvoice->address_from, $limit = 15, $end = '  ...') }}</a></td>
														<td><a href="#" data-toggle="tooltip" title="{{ $order->quotationInvoice->address_to }}">{{ str_limit($order->quotationInvoice->address_to, $limit = 15, $end = '  ...') }}</a></td>
														<td>{{ Carbon\Carbon::parse($order->quotationInvoice->moving_date)->toDayDateTimeString() }}</td>
														<td>{{ $order->total }}</td>
														<td>{{ $order->estimated_amount }}</td>
														<td>{{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y')}}</td>
														<td>
															<select name="status" class="form-control statusButton"  data-id="{{$order->id}}" >
							                                    <option value="0" {{ $order->status == 0 ? 'selected' : '' }}>Pending</option>
							                                    <option value="1" {{ $order->status == 1 ? 'selected' : '' }}>Approved</option>
							                                    <option value="2" {{ $order->status == 2 ? 'selected' : '' }}>Rejected</option>
							                                </select>
														</td>
	                                                    <td>
							                                <div class="btn-group mr-1 mb-1">
							                                <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
							                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
							                                  
							                                  
							                                  <a class="dropdown-item" href="{{ route('edit-quotation', ['id' => $order->id])}}"><i class="fa fa-pencil-square-o"></i>Edit</a>
							                                  <a class="dropdown-item delButton"  data-id="{{$order->id}}" href="javascript:void(0)"><i class="fa fa-trash"></i>Delete</a>
							                                </div>
							                              </div>
							                            </td>	
													</tr>
  												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- // Basic form layout section end --> 
		</div>
	</div>
</div>
@endsection('content')  

@section('js')

<script type="text/javascript">

    $('.statusButton').on('change', function(){
        var value = $(this).val();
        var target = $(this).data("id");
        $.ajax({
            url: '{{ route("update-quotation-status")}}',
            data: {value: value, target:target },
            type: 'get',
            success: function(response){
                if(response.status == 200){
                     toastr.success(response.msg, 'Success');
                }
                else{
                     toastr.error('Error');
                     $('.statusButton').val(!value);
                }
            }
        });
    });

   
	$(".delButton").on('click', function () {
	    var id = $(this).data("id");
	    $.confirm({
	        theme: 'material',
	        closeIcon: true,
	        animation: 'scale',
	        type: 'red',
	        title: 'Confirm!',
	        content: 'Are you sure want to delete this quotation ?',
	        buttons: {
	            confirm: function () {
	                $.ajax({
	                    url: base_url + '/admin/quote/delete-quotation/'+id,
	                    success: function(response){
	                        if(response.status == 200){
	                            $('#row'+id).remove();
	                            //alert(response.msg);
	                            toastr.success(response.msg, 'Success');
	                        }else{
	                            toastr.error(response.msg, 'Error');
	                        }
	                    },
	                    error:function (response) {
	                            toastr.error('Some error occurred', 'Error');
	                    }
	                });
	            },
	            cancel: function () {
	                //console.log('sdvsdv');
	            }
	        }
	    });
	});
        
    $("#status").change(function(){
        var status = this.value;
        toastr.success(status);
        if(status=="0") {
            toastr.success(status);
        } else {
            toastr.success(status);
        }
    });
        
    $('.my-sort').DataTable({
        "order": [[ 6, "desc" ]]        
    });

</script>
@endsection('js')