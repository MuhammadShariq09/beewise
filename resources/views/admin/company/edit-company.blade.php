@extends('layouts.master')
@section('title','Air Craft Works - Edit Company')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="configuration" class="u-c-p categorie">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pro-main">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success">
                                            <strong>{{ Session::get('message')  }}</strong>
                                        </div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="alert alert-danger">
                                          <strong>{{ Session::get('error')  }}</strong>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Edit Company</h1>
                                        </div>
                                    </div>
                                    <form  action="{{ route('update-company') }}" enctype="multipart/form-data" method="post" novalidate class="form">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10 col-sm-12">
                                            <input type="hidden" name="company_id" id="company_id" value="{{$company->id}}" />

                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-12">
                                                    <button name="file" type="button" class="uplon-btn2" onclick="document.getElementById('upload').click()">
                                                        <div class="photo-div" id='photoBG' style="background-image: url('{{ asset($company->image) }}')">
                                                            <div class="photo-div-inner">
                                                                <p><i class="fa fa-camera"></i>Add Image</p>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    <input type="file"  accept="image/*" required onchange="readURL(this, 'photoBG')" id="upload" name="company_image">
                                                </div>

                                                <div class="col-lg-7 col-md-7 col-12">
                                                    <div class="cat-text">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for="title"><i class="fa fa-th-large"></i> Company Title</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <input type="text" id="title" name="title" class="form-control" value="{{ $company->title }}">
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for="status"><i class="fa fa-check-circle"></i>Status</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <select class="form-control" name="status" id="status">
                                                                    <option {{ $company->status == 0 ? "selected" : "" }}  value="0">In-Active</option>
                                                                    <option {{ $company->status == 1 ? "selected" : "" }} value="1">Active</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        
                                                        <div class="row"> 
                                                          <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                              <label><i class="fa fa-map-marker"></i> Address</label>
                                                            </div>
                                                          </div>
                                                          <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                            <input type="text"  value="{{ $company->address }}" id="autocomplete" onfocus="geolocate()" class="form-control" placeholder="Enter your address" name="address">
                                                          </div>
                                                        </div>

                                                        <div class="row">
                                                          <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                              <label><i class="fa fa-map-marker"></i> Country</label>
                                                            </div>
                                                          </div>
                                                          <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                            <input type="text"  value="{{ $company->country }}" id="country" class="form-control" placeholder="" name="country">
                                                          </div>
                                                        </div>

                                                        <div class="row">
                                                          <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                              <label><i class="fa fa-map-marker"></i> City</label>
                                                            </div>
                                                          </div>
                                                          <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                            <input type="text"  value="{{ $company->city }}" id="locality" class="form-control" placeholder="" name="city">
                                                          </div>
                                                        </div>

                                                        <div class="row">
                                                          <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                            <div class="txt">
                                                              <label><i class="fa fa-map-marker"></i> State</label>
                                                            </div>
                                                          </div>
                                                          <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                            <input type="text"  value="{{ $company->state }}" id="administrative_area_level_1" class="form-control" placeholder="" name="state">
                                                          </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label><i class="fa fa-map-marker"></i> Zip Code</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                                <input type="text"  value="{{ $company->postal_code }}" id="postal_code" class="form-control" placeholder="" name="postal_code">
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <div class="txt">
                                                                    <label for=""><i class="fa fa-commenting-o"></i> Company Description</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                                                <textarea class="form-control" id="description" name="description" placeholder="Company Description">{{ $company->description }}</textarea>
                                                                <button type="submit" class="save">Update Company</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>


@endsection('content')  

@section('js')
<script type="text/javascript">
    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                //$('#'+target).attr('src', e.target.result);
                $('#'+target).css('background-image', 'url(' + e.target.result + ')');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection('js')