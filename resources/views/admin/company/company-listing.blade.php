@extends('layouts.master')
@section('title','Air Craft Works - Companies Listing')
@section('body-class','vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar')
@section('body-col',"2-columns")


@section('content')  
<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-body"> 
			<!-- Basic form layout section start -->
			<section id="configuration">
				<div class="row">
					<div class="col-12">
						<div class="card rounded pad-20">
							<div class="card-content collapse show">
								<div class="card-body table-responsive card-dashboard">
									<div class="row">
										<div class="col-12">
						                    @if(Session::has('message'))
						                        <div class="alert alert-success">
						                            <strong>{{ Session::get('message')  }}</strong>
						                        </div>
						                    @endif
						                    @if(Session::has('error'))
						                        <div class="alert alert-danger">
						                          <strong>{{ Session::get('error')  }}</strong>
						                        </div>
						                    @endif
					                    </div>
                                    <div class="col-6">
                                        <h1>Companies</h1>
                                    </div>
                                    <div class="col-6">
                                    <div class="row">
                                    	<div class="col-12"><a href="{{ route('add-company')}}" class="green-btn-project"><i class="fa fa-plus-circle"></i>Create Company</a></div>
                                        <div class="col-12">
                                        </div>
                                    </div>
                                    	
                                        
                                    </div>
                                </div>
									<div class="clearfix"></div>
									<div class="maain-tabble">
										<table class="table table-striped table-bordered zero-configuration">
											<thead>
												<tr>
													<th>ID</th>
													<th>Title</th>
                                                    <th>Description</th>
                                                    <th>Added By</th>
													<th>Added On</th>
                                                    <th style="width:10%;">Status</th>
                                                    <th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0;?> 
												@foreach($companies as $company) 
                        						<?php $i++ ?>
												<tr id="row{{$company->id}}">
                                                	<td>{{$i}}</td>
													<td>
														@if($company->image == "")
														<span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{substr($company->title,0,1)}}</span>
														@else
														<span class="avatar avatar-online">
                                        					<img src="{{ asset($company->image) }}" alt="{{$company->title}}">
                               							</span>
                               							@endif
														{{ $company->title }}
													</td>
													<td>{{ str_limit($company->description, $limit = 50, $end = '  ...') }}</td>
													<td>{{ucfirst($company->firstName).' '.ucfirst($company->lastName)}}</td>
													<td>{{ \Carbon\Carbon::parse($company->created_at)->format('d/m/Y')}}</td>
													<td>
														<select name="status" class="form-control statusButton"  data-id="{{$company->id}}" >
						                                    <option value="0" {{ $company->status == 0 ? 'selected' : '' }}>In-active</option>
						                                    <option value="1" {{ $company->status == 1 ? 'selected' : '' }}>Active</option>
						                                </select>
													</td>
                                                    <td>
						                                <div class="btn-group mr-1 mb-1">
						                                <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
						                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
						                                  
						                                  
						                                  <a class="dropdown-item" href="{{ route('edit-company', ['id' => $company->id])}}"><i class="fa fa-pencil-square-o"></i>Edit</a>
						                                  <a class="dropdown-item delButton"  data-id="{{$company->id}}" href="javascript:void(0)"><i class="fa fa-trash"></i>Delete</a>
						                                </div>
						                              </div>
						                            </td>
													
												</tr> 
  												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- // Basic form layout section end --> 
		</div>
	</div>
</div>
@endsection('content')  

@section('js')

<script type="text/javascript">

    $('.statusButton').on('change', function(){
        var value = $(this).val();
        var target = $(this).data("id");
        $.ajax({
            url: '{{ route("update-company-status")}}',
            data: {value: value, target:target },
            type: 'get',
            success: function(response){
                if(response.status == 200){
                     toastr.success(response.msg, 'Success');
                }
                else{
                     toastr.error('Error');
                     $('.statusButton').val(!value);
                }
            }
        });
    });

   
	$(".delButton").on('click', function () {
	    var id = $(this).data("id");
	    $.confirm({
	        theme: 'material',
	        closeIcon: true,
	        animation: 'scale',
	        type: 'red',
	        title: 'Confirm!',
	        content: 'Are you sure want to delete this job ?',
	        buttons: {
	            confirm: function () {
	                $.ajax({
	                    url: base_url + '/admin/company/delete-company/'+id,
	                    success: function(response){
	                        if(response.status == 200){
	                            $('#row'+id).remove();
	                            //alert(response.msg);
	                            toastr.success(response.msg, 'Success');
	                        }else{
	                            toastr.error(response.msg, 'Error');
	                        }
	                    },
	                    error:function (response) {
	                            toastr.error('Some error occurred', 'Error');
	                    }
	                });
	            },
	            cancel: function () {
	                //console.log('sdvsdv');
	            }
	        }
	    });
	});
        
    $("#status").change(function(){
        var status = this.value;
        toastr.success(status);
        if(status=="0") {
            toastr.success(status);
        } else {
            toastr.success(status);
        }
    });
        
    $('.my-sort').DataTable({
        "order": [[ 4, "desc" ]]        
    });

</script>
@endsection('js')