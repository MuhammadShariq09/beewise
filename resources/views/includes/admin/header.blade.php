<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/css/vendors.css') }}">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/css/app.css') }}">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<!-- END Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/css/plugins/calendars/fullcalendar.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/vendors/css/calendars/fullcalendar.min.css') }}">
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/assets/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">

<!-- Toaster -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />

<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/vendors/css/pickers/daterange/daterangepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}"> -->



