<!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
        <div class="main-menu-content">
           <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item {{ (URL::current() ==  route('dashboard')) ? "active" : "" }}"><a href="{{ route('dashboard') }}"><i class="fa fa-info-circle"></i><span class="menu-title" data-i18n=""> Overview</span></a></li>
                <li class="nav-item {{ (Request::segment(2) ==  'users') ? "active" : "" }}"><a href="{{ route('users') }}"><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n=""> Users</span></a></li>
                <li class="nav-item {{ (Request::segment(2) ==  'properties') ? "active" : "" }}"><a href="{{ route('properties') }}"><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n=""> Property Type</span></a></li>
                <li class="nav-item {{ (Request::segment(2) ==  'quotations') ? "active" : "" }}"><a href="{{ route('quotations') }}"><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n=""> Quotations</span></a></li>
                <li class="nav-item {{ (Request::segment(2) ==  'invoice-orders') ? "active" : "" }}"><a href="{{ route('invoice-orders') }}"><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n="">Orders</span></a></li>
                <li class="nav-item {{ (Request::segment(2) ==  'inspection-requests') ? "active" : "" }}"><a href="{{ route('inspection-requests') }}"><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n=""> Inspection Requests</span></a></li>
                <li class="nav-item {{ (Request::segment(2) ==  'inspection-payments') ? "active" : "" }}"><a href="{{ route('inspection-payments') }}"><i class="fa fa-file-text"></i><span class="menu-title" data-i18n=""> Inspection Payments</span></a></li>    
                
                <li class="nav-item {{ (Request::segment(2) ==  'coupons') ? "active" : "" }}"><a href="{{ route('coupons') }}"><i class="fa fa-users"></i><span class="menu-title" data-i18n=""> Coupons</span></a></li>
                <li class="nav-item {{ (Request::segment(2) ==  'page') ? "active" : "" }}"><a href="{{ route('page') }}"><i class="fa fa-users"></i><span class="menu-title" data-i18n=""> Page</span></a></li>
                  
                <li class="nav-item"><a href="#"><i class="fa fa-comments"></i><span class="menu-title" data-i18n=""> Feedback</span></a></li>      
            </ul>
        </div>
    </div>