<!-- BEGIN VENDOR JS--> 
<script src="{{ asset('assets/admin/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('assets/admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('assets/admin/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}" type="text/javascript"></script> 
<!-- BEGIN VENDOR JS--> 
<!-- BEGIN PAGE VENDOR JS--> 
<!-- END PAGE VENDOR JS--> 
<!-- BEGIN STACK JS--> 
<script src="{{ asset('assets/admin/app-assets/vendors/js/extensions/moment.min.js') }}" type="text/javascript"></script> 
<!-- <script src="{{ asset('assets/admin/app-assets/vendors/js/extensions/fullcalendar.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('assets/admin/app-assets/js/scripts/extensions/fullcalendar.js') }}" type="text/javascript"></script>  -->
<script src="{{ asset('assets/admin/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script> 
<script src="{{ asset('assets/admin/app-assets/js/core/app.js') }}" type="text/javascript"></script> 
<script src="{{ asset('assets/admin/app-assets/js/scripts/customizer.js') }}" type="text/javascript"></script> 
<script src="{{ asset('assets/admin/app-assets/js/scripts/modal/components-modal.js') }}" type="text/javascript"></script> 
<!-- Toaster js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
