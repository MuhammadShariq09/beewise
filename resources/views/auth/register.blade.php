@extends('layouts.master')

@section('title',env('APP_NAME').' - User Registration')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded")
@section('body-col',"2-column")
@section('content')


<section class="register">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-xl-3"></div>
      <div class="col-md-8 col-xl-6 col-12">
        <div class="register-main">
          <form action="{{ route('register') }}" enctype="multipart/form-data" method="post" novalidate class="form">
             @csrf
            <img src="{{ asset('assets/admin/images/logo.png') }}" class="img-full" alt="logo">
            <div class="form-main">
            
            <!--fields start here-->
            
            <div class="fields">
              <div class="row">
                <div class="col-md-12"> <i class="fa fa-user"></i>
                  <i class="fa fa-user"></i>
                  <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" placeholder="{{ __('First Name') }}">
                    @if ($errors->has('first_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="col-md-12"> <i class="fa fa-user"></i>
                    <i class="fa fa-user"></i>
                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" placeholder="{{ __('Last Name') }}">
                    @if ($errors->has('last_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif  
                </div>
 
    
                <div class="col-md-12"> <i class="fa fa-envelope"></i>
                    <i class="fa fa-envelope"></i>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail Address') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="col-md-12"> <i class="fa fa-key"></i>
                    <i class="fa fa-key"></i>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="col-md-12"> <i class="fa fa-key"></i>
                    <i class="fa fa-key"></i>
                    <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" placeholder="{{ __('Confirm Password') }}">
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                
                
                <div class="col-md-12">  
                  <i class="fa fa-phone"></i>
                  <input type="text" name="phone" id="phone" class="phone form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"  value="{{ old('phone') }}" placeholder="Enter Your Phone Number" maxlength="14">
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="col-md-12">  
                    <i class="fa fa-map-marker"></i>
                    <input type="text" id="autocomplete" value="" name="address" onfocus="geolocate()" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"  value="{{ old('address') }}" placeholder="Enter Your Address">
                    <input type="hidden" id="latitude" class="form-control" name="latitude"  value="{{ old('latitude') }}">
                    <input type="hidden" id="longitude" class="form-control" name="longitude" value="{{ old('longitude') }}">
                    <input type="hidden" id="street_number" class="form-control" name="street_number"  value="{{ old('street_number') }}">
                    <input type="hidden" id="locality" class="form-control" name="city"  value="{{ old('city') }}">
                    <input type="hidden" id="administrative_area_level_1" class="form-control" name="state"  value="{{ old('state') }}">
                    <input type="hidden" id="country" class="form-control" name="country"  value="{{ old('country') }}">
                    <input type="hidden" id="postal_code" class="form-control" name="postal_code"  value="{{ old('postal_code') }}">
                    @if ($errors->has('address'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>
              </div>
              <button type="submit" class="form-control">{{ __('Register') }}</button>
            </div>
            <div class="new-user">
                <p>already a user ?</p>
                <a href="{{route('login')}}" class="login form-control">Login</a> </div>
        
        </form>
      </div>
        
    </div>
    <div class="col-md-2 col-xl-3"></div>
  </div>
  </div>
</section>

@endsection

@section('js')
<script type="text/javascript">
    function phoneFormatter() {
      $('.phone').on('input', function() {
        var number = $(this).val().replace(/[^\d]/g, '')
        if (number.length == 7) {
          number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
        } else if (number.length == 10) {
          number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        }
        $(this).val(number)
      });
    };
        
    $(phoneFormatter);
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        /*locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'*/

        /*street_number: 'short_name',
        route: 'long_name',*/
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.setComponentRestrictions(
            {'country': ['us']});
        autocomplete.addListener('place_changed', fillInAddress);
        /*
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
        });*/
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        console.log(place.geometry.location.lat());
        console.log(place.geometry.location.lng());
        document.getElementById("latitude").value = place.geometry.location.lat();
        document.getElementById("longitude").value = place.geometry.location.lng();
        for (var component in componentForm) {
            try{
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }catch(e){

            }
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            console.log(addressType)
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>

@endsection('js')