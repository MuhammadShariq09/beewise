@extends('layouts.master')

@section('title', env('APP_NAME').' - Administrator Login')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded")
@section('body-col',"2-column")
@section('content')

<section class="register loginn">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-xl-3"></div>
      <div class="col-md-8 col-xl-6 col-12">
        <div class="register-main">
           <img src="{{ asset('assets/admin/images/logo.png') }}" class="img-full" alt="logo">
            <div class="form-main">
                 <h1>login to your account</h1>
              <div class="row">
                <div class="col-md-12">
                  <div class="img-div">
                    <div class="img-div-inner">

                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{session()->get('success')}}
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{session()->get('error')}}
                            </div>
                        @endif
                    </div>
                  </div>
                </div>
              </div>
              <!--fields start here-->
              <form method="POST" action="{{ route('login') }}">
                @csrf

              <div class="fields">
                <div class="row">
                  <div class="col-md-12"> <i class="fa fa-user"></i>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="User Email" name="email" value="{{ old('email') }}" autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="col-md-12"> <i class="fa fa-key"></i>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-6">
                        <label class="check">{{ __('Remember Me') }}
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="col-md-6 col-6">
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                        @endif
                    </div>
                </div>
                <button type="submit" class="form-control">{{ __('Login') }}</button>

                <!-- <div class="new-user">
                <p>new user</p>
                <a href="u-register.html" class="login form-control">Register Account</a></div> -->
              <!--fields end here-->
              </div>
              </form>
            </div>
         </div>
      </div>
      <div class="col-md-2 col-xl-3"></div>
    </div>
  </div>
</section>
@endsection
