<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::post('/forgot', 'AuthController@forgot');
Route::post('/verify-code', 'AuthController@verifyCode');
Route::post('/reset', 'AuthController@resetPassword');
Route::post('/contact-us/', 'AuthController@contactUs');
Route::get('/get-page-data', 'AuthController@getPageData');
Route::post('/upload-quotation-image', 'QuotationImageController@uploadQuotationImage');
Route::apiResource('properties', 'PropertyTypeController');

//Route::delete('/delete-quotation-image/{url}', 'QuotationImageController@deleteQuotationImage');
//Route::post('create-quotation', 'QuotationController@store');

Route::group(['middleware' => 'jwt'], function(){
    
    Route::get('/logout', 'AuthController@logout');
	
	Route::apiResource('quotations', 'QuotationController');
	Route::post('quotations/{quotation}/', 'QuotationImageController@store');
    Route::put('/update-image/{id}', 'UserController@updateImage');
    Route::delete('/delete-invoice/{id}', 'InvoiceOrderController@destroy');
	/*Route::apiResource('invoice-orders', 'InvoiceOrderController');*/
	//Route::post('inspection-request/{invoice_order_id}/', 'InspectionRequestController@store');
	Route::post('/inspection-request/{id}', 'InspectionRequestController@store');
	Route::get('/inspection-request/{id}', 'InspectionRequestController@show');
    Route::post('/inspection-payment/{id}', 'InspectionPaymentController@store');
    Route::delete('/cancel-inspection/{id}', 'InspectionPaymentController@destroy');
	Route::get('/orders', 'OrderController@index');
	Route::get('/validate-order/{id}', 'OrderController@validateOrder');
	Route::post('/apply-coupon/{id}', 'PaymentController@applyCoupon');
	Route::post('/payments/{id}', 'PaymentController@store');
	Route::delete('/cancel-order/{id}', 'PaymentController@destroy');
	Route::apiResource('users', 'UserController');

	Route::put('/update-password/{id}', 'AuthController@changePasswordRequest');


});


/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
