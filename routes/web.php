<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function(){

	Route::get('/', 'AdminController@index');
	Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');

	//Admin profile route
	Route::get('/edit-profile', 'AdminController@editProfile')->name('edit-profile');
	Route::post('/update-profile', 'AdminController@updateProfile')->name('update-profile');

	// Users module route
	// Users route
	Route::get('/users', 'AdminController@getUsersListing')->name('users');
	Route::get('/users/add-user', 'AdminController@addUser')->name('add-user');
	Route::post('/users/submit-user', 'AdminController@submitUser')->name('submit-user');
	Route::get('/users/update-status', 'AdminController@updateUserStatus')->name('update-user-status');
	Route::get('/users/edit-user/{id}', 'AdminController@editUser')->name('edit-user');
    Route::post('/users/update-user', 'AdminController@updateUser')->name('update-user');
    Route::get('/users/view-user/{id}', 'AdminController@viewUser')->name('view-user');
    Route::get('/users/delete-user/{id}', 'AdminController@deleteUser')->name('delete-user');

    // Properties Route
	Route::get('/properties', 'AdminController@getPropertiesListing')->name('properties');
	Route::get('/properties/add-property', 'AdminController@addProperty')->name('add-property');
	Route::post('/properties/submit-property', 'AdminController@submitProperty')->name('submit-property');
	Route::get('/properties/edit-property/{id}', 'AdminController@editProperty')->name('edit-property');
	Route::post('/properties/update-property', 'AdminController@updateProperty')->name('update-property');
	Route::get('/properties/update-status', 'AdminController@propertyUpdateStatus')->name('update-property-status');
	Route::get('/properties/delete-property/{id}', 'AdminController@deleteProperty')->name('delete-property');


	// Quotation Route
	Route::get('/quotations', 'AdminController@getQuotationsListing')->name('quotations');
    Route::get('/quotations/edit-quotation/{id}', 'AdminController@editQuotation')->name('edit-quotation');
    Route::get('/quotations/view-quotation/{id}', 'AdminController@viewQuotation')->name('view-quotation');
	Route::get('/quotations/update-quotation', 'AdminController@updateQuotation')->name('update-quotation');
	Route::get('/quotations/update-status', 'AdminController@quotationUpdateStatus')->name('update-quotation-status');
    Route::get('/quotations/delete-quotation/{id}', 'AdminController@deleteQuotation')->name('delete-quotation');
    Route::get('/quotations/create-quotation-invoice/{id}', 'AdminController@createQuotationInvoice')->name('create-quotation-invoice');
	Route::post('/quotations/submit-quotation-invoice/', 'AdminController@submitQuotationInvoice')->name('submit-quotation-invoice');
	Route::post('/quotations/quotation-item/', 'AdminController@createQuotationItems');

    // Inspection Request
    Route::get('/inspection-requests', 'AdminController@inspectionRequestListing')->name('inspection-requests');
    //Route::get('/inspection-requests/edit-inspection-request/{id}', 'AdminController@editInspectionRequest')->name('edit-inspection-request');
    Route::post('/inspection-requests/update-inspection', 'AdminController@updateInspection')->name('update-inspection');
    Route::get('/inspection-requests/update-status', 'AdminController@inspectionUpdateStatus')->name('update-inspection-status');

    // Inspection Payment Route
	Route::get('/inspection-payments', 'AdminController@getInspectionPaymentListing')->name('inspection-payments');

	// Invoice Order Route
	Route::get('/invoice-orders', 'AdminController@getOrdersListing')->name('invoice-orders');
	Route::get('/invoice-orders/edit-inspection-request/{id}', 'AdminController@editInspectionRequest')->name('edit-inspection-request'); 
	Route::get('/invoice-orders/view-pending-inspection/{id}', 'AdminController@viewPendingInspectionRequest')->name('view-pending-inspection'); 

	Route::post('/invoice-orders/generate-order/', 'AdminController@generateOrderRequest')->name('generate-order'); 

	// Page Route
	Route::get('/page', 'AdminController@getPageData')->name('page');
	Route::get('/page/edit-page/{id}', 'AdminController@editPage')->name('edit-page');
    Route::post('/page/update-page', 'AdminController@updatePage')->name('update-page');

	// Coupen route
	Route::get('/coupons', 'AdminController@getCouponListing')->name('coupons');
	Route::get('/coupons/add-coupon', 'AdminController@addCoupon')->name('add-coupon');
	Route::post('/coupons/submit-coupon', 'AdminController@submitCoupon')->name('submit-coupon');
	Route::get('/coupons/edit-coupon/{id}', 'AdminController@editCoupon')->name('edit-coupon');
	Route::post('/coupons/update-coupon', 'AdminController@updateCoupon')->name('update-coupon');
	Route::get('/coupons/update-status', 'AdminController@couponUpdateStatus')->name('update-coupon-status');
	Route::get('/coupons/delete-coupon/{id}', 'AdminController@deleteCoupon')->name('delete-coupon');


	Route::post('/update-amount', 'AdminController@updateAmount')->name('update-amount');







});
