<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['message' => 'Token is Invalid','error' => true]);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                 /*$token = JWTAuth::refresh($token);
                  JWTAuth::setToken($token);
                  $user = JWTAuth::authenticate($token);*/
                 
                 /*try
                    {
                        $token = JWTAuth::refresh($token);
                        JWTAuth::setToken($token);
                        $user = JWTAuth::authenticate($token);
                    }
                    catch (JWTException $e)
                    {
                         return response()->json(['message' => 'Authorization Token expired','error' => true]);
                    }*/
                return response()->json(['message' => 'Authorization Token expired','error' => true]);
            } else{
                return response()->json(['message' => 'Authorization Token not found','error' => true]);
            }
        }
        return $next($request);
    }
    
/*    public function handle($request, Closure $next)
    {
        // caching the next action
        $response = $next($request);

        try
        {
            if (! $user = JWTAuth::parseToken()->authenticate() )
            {
                return ApiHelpers::ApiResponse(101, null);
            }
        }
        catch (TokenExpiredException $e)
        {
            // If the token is expired, then it will be refreshed and added to the headers
            try
            {
                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                $response->header('Authorization', 'Bearer ' . $refreshed);
            }
            catch (JWTException $e)
            {
                return ApiHelpers::ApiResponse(103, null);
            }
            $user = JWTAuth::setToken($refreshed)->toUser();
        }
        catch (JWTException $e)
        {
            return ApiHelpers::ApiResponse(101, null);
        }

        // Login the user instance for global usage
        Auth::login($user, false);

        return $response;
    }*/
}