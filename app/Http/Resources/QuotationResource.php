<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class QuotationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id' => $this->id,
        'address_from' => $this->address_from,
        'state_from' => $this->state_from,
        'city_from' => $this->city_from,
        'country_from' => $this->country_from,
        'zipcode_from' => $this->zipcode_from,
        'property_type_from' => $this->property_type_from,
        'rooms_from' => $this->rooms_from,
        'address_to' => $this->address_to,
        'state_to' => $this->state_to,
        'city_to' => $this->city_to,
        'country_to' => $this->country_to,
        'zipcode_to' => $this->zipcode_to,
        'property_type_to' => $this->property_type_to,
        'moving_date' => date("d F Y", strtotime($this->moving_date)),
        'fragile_items' => $this->fragile_items,
        'packaging_items' => $this->packaging_items,
        'note' => $this->note,
        'status' => $this->status,
        'created_at' => (string) $this->created_at,
        'updated_at' => (string) $this->updated_at,
        'user' => $this->user,
        'images' => $this->images,
        'invoice_order' => $this->quotationOrder,
      ];
    }
}
