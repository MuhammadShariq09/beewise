<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'total' => $this->total,
            'discount_amount' => $this->discount_amount,
            'items' => $this->items,
            'order_invoice' => $this->orderInvoice,
            'inspection' => $this->InspectionPayment,
            'status' => $this->status,
            'is_paid' => $this->is_paid,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,

        ];
    }
}
