<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotation;
use App\QuotationImage;
use App\Http\Resources\QuotationImageResource;
class QuotationImageController extends Controller
{


    public function index()
    {
        return QuotationImageResource::collection(QuotationImage::paginate(10));
    }

    public function store(Request $request, Quotation $quotation)
    {
      $quotationImage = QuotationImage::firstOrCreate(
        [
          'quotation_id' => $quotation->id,
        ],
        ['image' => $request->image]
      );

      return new QuotationImageResource($quotationImage);
    }

    public function show(QuotationImage $quotationImage)
    {
        return new QuotationImageResource($quotationImage);
    }
    
    public function uploadQuotationImage(Request $request) {
        if($request->quotation_image) {
            list($type, $quotation_images) = explode(',', $request->quotation_image);
            $imageName = sprintf("%s.%s", time(), 'png');
            $path = 'uploads/quotations/' . $imageName;
            
            // file_put_contents($path, base64_decode($request->quotation_image));
            \Storage::put($path, base64_decode($quotation_images));
            //dd($path);
            if($path) {
                return response()->json(['url' => $path, 'message' => 'Image uploaded successfully', 'error' => false]);
            } else {
                return response()->json(['message' => 'Failed to upload image', 'error' => true]);
            }
        }
    }
    /*
    public function deleteQuotationImage($url) {
        dd(parse_url($url, PHP_URL_PATH));
    }*/
}
