<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /* //return UserResource(request()->user());
        return UserResource::resource(User::find('id', request()->user()->id));*/
        $users = User::find(request()->user()->id);
        return new UserResource($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'min:14'],
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }

        $user = User::find($id);
        $user->fill($request->all());
        
        if($request->user_image) {
            /*
            $image = $request->user_image;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'.'.'png';
            $path = 'uploads/users/' . $imageName;
            //dd($path);
            \Storage::put($path, base64_decode($image));
            */
            list($type, $request->user_image) = explode(',', $request->user_image);
            $imageName = sprintf("%s.%s", time(), 'png');
            $path = 'uploads/users/' . $imageName;
            \Storage::put($path, base64_decode($request->user_image));
            $user->image = $path;
            //$user->save();
        }
        $user->save();

        return response()->json(['message' => 'User updated successfully', 'error' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function updateImage(Request $request, $id) {
        $user = User::find($id);
        if($request->user_image) {
            list($type, $request->user_image) = explode(',', $request->user_image);
            $imageName = sprintf("%s.%s", time(), 'png');
            $path = 'uploads/users/' . $imageName;
            \Storage::put($path, base64_decode($request->user_image));
            $user->image = $path;
            //$user->save();
        }
        $user->fill($request->all());
        $user->save();
        return response()->json(['message' => 'Image updated successfully', 'error' => false]);
    }
}
