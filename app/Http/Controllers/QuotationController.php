<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Quotation;
use App\Http\Resources\QuotationResource;
use Image;
use File;

class QuotationController extends Controller
{

    public function index()
    {
        $collections = Quotation::with(['user','images','quotationOrder' => function($q){ $q->with(['invoiceOrderDetails','items', 'inspection']); }])->whereHas('user', function ($q){
            $q->where('user_id', request()->user()->id);
        })->orderBy('id', 'DESC')->get();

        return QuotationResource::collection($collections);
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'address_from' => 'required',
            'property_type_from' => 'required',
            'floors_from' => 'required',
            'rooms_from' => 'required',
            'address_to' => 'required',
            'property_type_to' => 'required',
            'floors_to' => 'required',
            'moving_date' => 'required',
            'packaging_items' => 'required',
            'fragile_items' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }

        $quotation = Quotation::create([
            'user_id' => $request->user()->id,
            'address_from' => $request->address_from,
            'state_from' => $request->state_from,
            'city_from' => $request->city_from,
            'country_from' => $request->country_from,
            'zipcode_from' => $request->zipcode_from,
            'latitude_from' => $request->latitude_from,
            'longitude_from' => $request->longitude_from,
            'property_type_from' => $request->property_type_from,
            'floors_from' => $request->floors_from,
            'rooms_from' => $request->rooms_from,
            'address_to' => $request->address_to,
            'state_to' => $request->state_to,
            'city_to' => $request->city_to,
            'country_to' => $request->country_to,
            'zipcode_to' => $request->zipcode_to,
            'latitude_to' => $request->latitude_to,
            'longitude_to' => $request->longitude_to,
            'property_type_to' => $request->property_type_to,
            'floors_to' => $request->floors_to,
            'moving_date' => $request->moving_date,
            'packaging_items' => $request->packaging_items,
            'fragile_items' => $request->fragile_items,
            'note' => $request->note,
        ]);

        $quotation->order_num = '#'.str_pad($quotation->id + 1, 6, "0", STR_PAD_LEFT);
        //dd($quotation->order_num);
        if($request->approved_images) {
            foreach($request->approved_images as $approved_image) {

                $quotation->images()->save(new \App\QuotationImage(['image' => $approved_image]));
            }
        }

        if($request->rejected_images) {
            foreach($request->rejected_images as $rejected_image) {
                $path = public_path()."/".$rejected_image;
                if(File::exists($path)){  // check if the image indeed exists
                  File::delete($path);
                }
                //unlink($path);
            }
        }

        $quotation->save();
        return response()->json(['message' => 'Quotation created successfully.', 'error' => false]);
    }




    public function show(Quotation $quotation)
    {
        return new QuotationResource($quotation);
    }


}
