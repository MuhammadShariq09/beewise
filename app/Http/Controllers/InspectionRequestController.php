<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\InvoiceOrder;
use Carbon\Carbon;
use App\InspectionRequest;
use App\Http\Resources\InspectionRequestResource;
use Illuminate\Support\Facades\DB;

class InspectionRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'time' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }
        $inspection_amount = DB::table('inspection_amount')->get();
        //dd($inspection_amount[0]->amount);
        $inspectionRequest = InspectionRequest::create([
            'invoice_order_id' => $id,
            'date' => $request->date,
            'time' => $request->time,
            'amount' => $inspection_amount[0]->amount,
            'description' => '',
            'status' => "0",
        ]);
        InvoiceOrder::where('id', $id)->update(['status' => "1",'updated_at' => Carbon::now()]);
        return response()->json(['message' => 'Inspection request created successfully.', 'error' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inspectioncollections = InspectionRequest::where('invoice_order_id', $id)->get();
        
        return InspectionRequestResource::collection($inspectioncollections);
    }
}
