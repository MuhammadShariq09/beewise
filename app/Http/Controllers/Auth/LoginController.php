<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //dd("test");
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
    }
    
    /*public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        // Attempt to log the user in
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            //$role = Auth::user()->is_admin; 
            // if inactive user
            dd(auth()->user());
            if(auth()->user()->is_active == '0') {
                auth()->logout();
                return redirect()->route('login')->with(['error' => 'Your Account is deactivated']);
            } else if (auth()->user()->is_admin == "1") {
                return redirect()->route('dashboard');
            }
            else {
                auth()->logout();
                return redirect()->route('login')->with(['error' => 'You do not have right to access admin panel']);
            }
            
        } 
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput(['error' => 'Your credentials are incorrect']);
    }*/

    public function login(Request $request) {
       
        $this->validateLogin($request);

        /*if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }*/

        if($this->guard()->validate($this->credentials($request))) {
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin' => '1' ], $request->remember)) {
                 return redirect()->route('dashboard');
            }  else {
                auth()->logout();
                return redirect()->route('login')->with(['error' => 'You do not have right to access admin panel']);
            }
        } else {
            // dd('ok');
            //$this->incrementLoginAttempts($request);
            auth()->logout();
            return redirect()->route('login')->with(['error' => 'Credentials are invalid']);
        }
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect()->to('/login');
    }

}
