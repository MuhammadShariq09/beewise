<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\PaypalModel;
use App\Payment;
use App\Coupon;
use App\UserCoupon;
use Carbon\Carbon;
use App\InspectionRequest;
use App\InspectionPayment;
use App\Order;
use App\InvoiceOrder;
use App\Quotation;
use App\Http\Resources\InspectionPaymentResource;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PaymentResource::collection(Payment::with(['order','user'])->whereHas('user', function ($q)
        {
            $q->where('user_id', request()->user()->id);
        })->paginate(10));
    }
    
    
    public function applyCoupon(Request $request, $id) {
        
        $validator = Validator::make($request->all(), [
            'coupon_code' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }
        
        $coupon = Coupon::where('code', $request->coupon_code)->first();
        if($coupon != null) {
            $coupon_uses = UserCoupon::where('coupon_id', $coupon->id)->count();
        }
        //dd($coupon);
        if($coupon == null) {
            return response()->json(['error' => true, 'message' => 'Invalid coupon code']);
        }
        else if($coupon->status  == "0"){
                return response()->json(['error' => true, 'message' => 'Coupon is In-active']);  
        } else if (($coupon->start_date  >= (date('Y-m-d').' 00:00:00')) || ($coupon->end_date  <= (date('Y-m-d').' 00:00:00'))){
            return response()->json(['error' => true, 'message' => 'Coupon Expired (or) In-active']);  
        } else if($coupon->max_uses < $coupon_uses){
            return response()->json(['error' => true, 'message' => 'Coupon consumed']); 
        }
        else {
            $amount = Order::find($id)->total;
            //dd($request->user()->id);
            if($coupon->type == "0") {
                $discount = ($coupon->discount_amount*$amount)/100;
            } else {
                $discount = $coupon->discount_amount;
            }
            $user_coupon = UserCoupon::create([
                'user_id' => $request->user()->id,
                'coupon_id' => $coupon->id,
                'redeemed_at' => Carbon::now()
            ]);
            //dd($user_coupon);
            Order::find($id)->update(["discount_amount" => $discount ]);
            $data = array(
                "type" => $coupon->type,
                "discount_amount" => $coupon->discount_amount
            );
            return response()->json(['error' => false, 'data' => $data,  'message' => 'coupon applied successfully']);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'card_no' => 'required|min:16',
            'cvc' => 'required|min:3|max:4',
            'expiration_month' => 'required',
            'expiration_year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }
        
        $amount = Order::find($id)->total;
        $discount = Order::find($id)->discount_amount;
        
        $discounted_amount = $amount - $discount;
        $paypalParams = array(
            'paymentAction' => 'Sale',
            'amount' => $discounted_amount,
            'currencyCode' => 'GBP',
            'creditCardType' => '',
            'creditCardNumber' => trim(preg_replace('/\s+/', '', $request->card_no)),
            'expMonth' => $request->expiration_month,
            'expYear' => $request->expiration_year,
            'cvv' => $request->cvc,
            'firstName' => $request->name,
            'lastName' => "",
            'city' => "",
            'zip'  => "",
            'countryCode' => "",
        );
        $paypal_model = new PaypalModel();
        $response = $paypal_model->paypalCall($paypalParams);

        //dd($response);
        if($response['ACK'] == "Failure"){
            $e = ($response['L_LONGMESSAGE0']) ? @$response['L_LONGMESSAGE0'] : @$response['L_LONGMESSAGE1'];
            return response()->json(['error'=>true, 'message'=>$e]);
        } elseif($response['ACK'] == "Success") {

            Payment::create([
                'user_id' => $request->user()->id,
                'order_id' => $id,
                'amount' => $discounted_amount,
                'transaction_id' => $response['TRANSACTIONID'],
            ]);
            Order::find($id)->update(["status" => "1","is_paid" => "1"]);
            return response()->json(['error'=>false, 'message'=>"Payment is successfully procceed."]);
        } else {
            return response()->json(['error'=>true, 'msg'=>"Transaction could not be performed!"]);
        }
        
    }
    
    
    public function destroy($id)
    {
        $order = Order::find($id);
        //$invoice = InvoiceOrder::find($id);
        if($order) {
            // Find and update inspection payment
            $inspectionPayment = InspectionPayment::find($order->inspection_payment_id);
            
            // Find and update inspection request
            InspectionRequest::find($inspectionPayment->inspection_request_id)->update(['status' => "2"]);
            $inspectionRequest = InspectionRequest::find($inspectionPayment->inspection_request_id);
            
            // find and update invoice order
            InvoiceOrder::find($inspectionRequest->invoice_order_id)->update(['status' => "2"]);
            $invoice = InvoiceOrder::find($inspectionRequest->invoice_order_id);
            
            // find and update quotation
            Quotation::find($invoice->quotation_id)->update(['status' => "2"]);
        
            // find and update order
            Order::find($id)->update(['status' => "2"]);
            return response()->json(['error' => false, 'message' => 'Request deleted successfully']);
        } else {
           return response()->json(['error' => true, 'message' => 'Failed to delete request']);
        }
    }

   
}
