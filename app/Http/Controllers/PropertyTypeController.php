<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyType;
use App\Http\Resources\PropertyTypeResource;
class PropertyTypeController extends Controller
{


    public function index()
    {
        return PropertyTypeResource::collection(PropertyType::all());
    }

}
