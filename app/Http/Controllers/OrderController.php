<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Http\Resources\OrderResource;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         //=> function($q){ $q->with(['request'  => function($q){ $q->with(['request.invoiceOrder.quotationInvoice' ]); } ]); }
        return OrderResource::collection(Order::with(['items','orderInvoice','InspectionPayment'=> function($q){ $q->with(['inspectionRequest' => function($q){ $q->with(['invoiceOrder.quotationInvoice.images']); }]); } ])->whereHas('InspectionPayment', function ($q)
        {
            $q->where('user_id', request()->user()->id);
        })->orderBy('id', 'DESC')->get());
    }

    public function validateOrder($id) {
        Order::find($id)->update(["status" => "2" ]);
        return response()->json(['error' => false,  'message' => 'Order validated']);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function show(Order $order)
    {
        return new OrderResource($order);
    }*/

}
