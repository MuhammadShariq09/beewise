<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\PaypalModel;
use App\InspectionPayment;
use App\InspectionRequest;
use App\InvoiceOrder;
use App\Quotation;
use App\Http\Resources\InspectionPaymentResource;

class InspectionPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InspectionPaymentResource::collection(InspectionPayment::with(['order','user'])->whereHas('user', function ($q)
        {
            $q->where('user_id', request()->user()->id);
        })->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'card_no' => 'required|min:16',
            'cvc' => 'required|min:3|max:4',
            'expiration_month' => 'required',
            'expiration_year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }
        
        $amount = InspectionRequest::find($id)->amount;
        //dd($amount);
        
        $paypalParams = array(
            'paymentAction' => 'Sale',
            'amount' => $amount,
            'currencyCode' => 'GBP',
            'creditCardType' => '',
            'creditCardNumber' => trim(preg_replace('/\s+/', '', $request->card_no)),
            'expMonth' => $request->expiration_month,
            'expYear' => $request->expiration_year,
            'cvv' => $request->cvc,
            'firstName' => $request->name,
            'lastName' => "",
            'city' => "",
            'zip'  => "",
            'countryCode' => "",
        );
        $paypal_model = new PaypalModel();
        $response = $paypal_model->paypalCall($paypalParams);

        if($response['ACK'] == "Failure"){
            $e = ($response['L_LONGMESSAGE0']) ? @$response['L_LONGMESSAGE0'] : @$response['L_LONGMESSAGE1'];
            return response()->json(['error'=>true, 'message'=>$e]);
        } elseif($response['ACK'] == "Success") {

            InspectionPayment::create([
                'user_id' => $request->user()->id,
                'inspection_request_id' => $id,
                'amount' => $amount,
                'transaction_id' => $response['TRANSACTIONID'],
            ]);
            InspectionRequest::find($id)->update(["is_paid" => "1"]);
            return response()->json(['error'=>false, 'message'=>"Payment is successfully procceed."]);
        } else {
            return response()->json(['error'=>true, 'msg'=>"Transaction could not be performed!"]);
        }
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inspectionRequest = InspectionRequest::find($id);
        //$invoice = InvoiceOrder::find($id);
        if($inspectionRequest) {
            InvoiceOrder::find($inspectionRequest->invoice_order_id)->update(['status' => "2"]);
            $invoice = InvoiceOrder::find($inspectionRequest->invoice_order_id);
            Quotation::find($invoice->quotation_id)->update(['status' => "2"]);
        
            InspectionRequest::find($id)->update(['status' => "2"]);
            return response()->json(['error' => false, 'message' => 'Request deleted successfully']);
        } else {
           return response()->json(['error' => true, 'message' => 'Failed to delete request']);
        }
    }
}
