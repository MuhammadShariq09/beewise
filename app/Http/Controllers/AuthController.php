<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Feedback;
use App\Page;
use App\Coupon;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\Mail\NotifyNewUser;
use App\Mail\ForgotPasswordMail;
use App\Mail\FeedbackUser;
use App\Mail\NotifyFeedbackadmin;

class AuthController extends Controller
{


	public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login', 'register', 'forgot','resetPassword','contactUs', 'verifyCode','getPageData']]);
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => 'required|min:8',
            'phone' => ['required', 'string', 'min:14'],
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'address' => $request->address,
            'country' => $request->country,
            'state' => $request->state,
            'city' => $request->city,
            'postal_code' => $request->postal_code,
            'is_admin' => "0",
        ]);

        $admin = User::select('email')->where('is_admin', "1")->first();
        Mail::to($request->email)->send(new WelcomeMail($request));
        Mail::to($admin->email)->send(new NotifyNewUser());
        //$token = auth('api')->login($user);
        //return $this->respondWithToken($token);
        return response()->json(['message' => 'Successfully created user!', 'error' => false]);
    }

	public function login(Request $request) {
	    $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8']
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }
        $credentials = request(['email', 'password']);
        $token       = auth('api')->attempt($credentials);
        if (!$token) {
            return response()->json(['message' => 'Email or Password do not matched', 'error' => true]);
        }
        return $this->respondWithToken($token, ['message' => 'User Login Successfull', 'error' => false]);
    }

    public function logout()
    {
        auth('api')->logout();
        return response()->json(['message' => 'Successfully logged out', 'error' => false]);
    }

    public function forgot(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255']
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }

        if (User::where('email', $request->email)->exists()) {
            $token = $this->forgotPasswordToken($request->email);
            return response()->json(['error'=>false, 'message'=>"A confirmation email has been sent to change your password."]);
        } else {
            return response()->json(['error'=>true, 'message'=>"No such email found !"]);
        }
    }

    public function verifyCode() {
        $validator = Validator::make(request()->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }    
        $whereArray = [
            'email' => request('email'),
            'forgot_token' => request('token')
        ];
        if (User::where($whereArray)->exists()) {
            return response()->json(['error'=>false, 'message'=>"Token accepted successfully"]);
        } else {
            return response()->json(['error'=>true, 'message'=>"Token mis-matched"]);
        }    

    }
    
    
    public function resetPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => 'required|min:8'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }    
        $whereArray = [
            'email' => $request->email,
            'forgot_token' => $request->token
        ];
        if (User::where($whereArray)->exists()) {
            User::where('email', $request->email)->update([
                'password' => Hash::make($request->password),
                'forgot_token' => ''
            ]);
            return response()->json(['error'=>false, 'message'=>"Password reset successfully"]);
        } else {
            return response()->json(['error'=>true, 'message'=>"Token mis-matched"]);
        }    

    }

    protected function respondWithToken($token, $additionalInformation = [])
    {
        $data = [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ];
        
        return response()->json(array_merge($data, $additionalInformation));
    }


    protected function forgotPasswordToken($email) {
        $token = str_random(4);
        User::where('email', $email)->update(["forgot_token" => $token]);
        Mail::to($email)->send(new ForgotPasswordMail($token));
        return $token;

    }

    public function changePasswordRequest(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required|string|min:8',
            'new_password' => 'required|string|min:8|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:8'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        } 

        $user = User::find(Auth::id());

        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json(['error' => true, 'message'=> 'Current password does not match']);
        }

        $user->password = Hash::make($request->new_password);
        $user->save();
        return response()->json(['error'=>false, 'message'=>"Password updated successfully"]);

    }
    
    /*
    Name = contactus()
    Purpose = Feedback Request
    */
    public function contactUs(Request $request) { 
    
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'subject' => ['required', 'string', 'max:255'],
            'message' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()]);
        }
        
        $feedback = Feedback::create([
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message,
        ]);
        
        if($feedback){
            $admin = User::select('email')->where('is_admin', "1")->first();
            //Mail::to($request->email)->send(new FeedbackUser($request));
            //Mail::to($admin->email)->send(new NotifyFeedbackAdmin());
            return response()->json(['error'=>false, 'message'=>"Thank you for submitting your query\n Our team response you back very soon"]);
        }
        else{
            return response()->json(['error'=>true, 'message'=>"Failed to submit query. Please try later!"]);
        }
    }
    
    
    /*
    Name = getPageData()
    Purpose = Feedback Request
    */
    public function getPageData() { 
    
        $page = Page::orderBy('id', 'DESC')->get();
        //dd($page);
        $coupon = Coupon::where('status',"1")->inRandomOrder()->first();
        //dd($coupon);
        
        return response()->json(['error'=>false, 'data'=> ['page' => $page, 'coupon' => $coupon]]);
        
    }
}