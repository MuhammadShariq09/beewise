<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Quotation;
use App\InvoiceOrder;
use App\Coupon;
use App\Order;
use App\OrderItem;
use App\OrderDetail;
use App\InspectionPayment;
use App\PropertyType;
use App\InspectionRequest;
use App\Page;
use Session;
use File;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\InviteRequest;
/*use App\Mail\UserStatusMail;*/
use Notification;
use App\Notifications\ProfileUpdated;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('admin.dashboard');
    }

    public function dashboard() {
        return view('admin.dashboard');
    }

    /*
    *   Name : editProfile()
    *   Purpose : Edit Admin profile detail
    */
    public function editProfile(Request $request) {

        $id = Auth::user()->id;
        $user = User::find($id);
        return view('admin.edit-profile',['user' => $user]);
    }


    /*
    *   Name : updateProfile()
    *   Purpose : Update Admin profile detail
    */
    public function updateProfile(Request $request) {

        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'min:14'],
            'address' => 'required'
        ]);

        $user_id = Auth::user()->id;

        $user = User::find($user_id);
        $user->fill($request->except(['password']));

        if($request->password) {
            $user->password = Hash::make($request->password);
        }

        // Upload Image
        if($request->file('user_image')) {
            $images = User::find($user_id);
            $userImage = public_path().$images->image; // get previous image from folder

            if (File::exists($userImage)) {
                File::delete($userImage);
            }
            $image = $request->file('user_image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $path = '/uploads/users/';
            $destinationPath = public_path($path);
            $image->move($destinationPath, $input['imagename']);
            $user->image = $path.$input['imagename'];
        }
        $user->save();

        Session::flash('message','Your profile is updated');
        return redirect('/admin/edit-profile');

    }


    /* ========================= Users =============================*/

    /*
    *   Name : getUsersListing()
    *   Purpose : get all user listing
    */
    public function getUsersListing() {
        $usersData = User::where('is_admin', "0")->orderBy('id', 'DESC')->get();
        //print_r($usersData); exit;
        return view('admin.users.user-listing',['users' => $usersData]);
    }

    /*
    *   Name : addUser()
    *   Purpose : Load Add user view
    */
    public function addUser() {
        return view('admin.users.add-user');
    }

    /*
    * Name : submitUser()
    * Purpose : Submit user details from admin
    */
    public function submitUser(Request $request) {
        $validator = \Validator::make($request->all(),[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'min:14'],
            'address' => 'required'
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $user = new User($request->except(['password','image']));

        $password = str_random(8);
        $user->password = Hash::make($password);

        // Upload Image
        if($request->file('user_image')) {
            $image = $request->file('user_image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $path = '/uploads/users/';
            $destinationPath = public_path($path);
            $image->move($destinationPath, $input['imagename']);
            $user->image = $path.$input['imagename'];
        }
        $user->save();

        $admin = Auth::user();
        Mail::to($request->email)->send(new InviteRequest($request->first_name,$request->last_name,$request->email,$password,$admin->first_name,$admin->last_name));

        return response()->json(['success'=>'User added successfully']);
        // Session::flash('message','User created successfully');
        // return redirect('/admin/users');

    }

    /*
    * Name : updateUserStatus()
    * Purpose : Update user status from admin
    */
    public function updateUserStatus(Request $request)
    {
        $user = User::findOrFail($request->target);
        $user->toggleActivation();
        $user->save();
        $user->notify(new ProfileUpdated());
        return response()->json(['status' => 200, 'msg' => 'Status updated successfully']);
    }

    /*
    NAme = editUser()
    Purpose = Load Edit User view
    */
    public function editUser($id){
        $user = User::find($id);
        return view('admin.users.edit-user',['user' => $user]);
    }


    /*
    NAme = viewUser()
    Purpose = Load View User view
    */
    public function viewUser($id){
        $user = User::find($id);
        return view('admin.users.partials.view_modal',['user' => $user]);
    }


    /*
    *   Name : updateProfile()
    *   Purpose : Update Admin profile detail
    */
    public function updateUser(Request $request) {

        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'min:14'],
            'address' => 'required'
        ]);

        $user_id = $request->user_id;

        $user = User::find($user_id);
        $user->fill($request->except(['email','password']));


        // Upload Image
        if($request->file('user_image')) {
            $images = User::find($user_id);
            $userImage = public_path().$images->image; // get previous image from folder

            if (File::exists($userImage)) {
                File::delete($userImage);
            }
            $image = $request->file('user_image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $path = '/uploads/users/';
            $destinationPath = public_path($path);
            $image->move($destinationPath, $input['imagename']);
            $user->image = $path.$input['imagename'];
        }
        $user->save();

        Session::flash('message','User updated successfully');
        return redirect('/admin/users');
    }


    public function deleteUser($id) {
        $user = User::find($id);
        $userImage = public_path().$user->image;
        if(File::exists($userImage)){  // check if the image indeed exists
          File::delete($userImage);
        }

        if(count((array)$user)){
            User::where('id', $id)->delete();
            return response()->json(['status' => 200, 'msg' => 'User deleted successfully']);
        } else {
            return response()->json(['status' => 500, 'msg' => 'Failed to delete user']);
        }
    }


    /* ========================= Properties =============================*/

    /*
    *   Name : getPropertiesListing()
    *   Purpose : get all properties listing
    */
    public function getPropertiesListing() {
        $properties = PropertyType::orderBy('id', 'DESC')->get();
        //dd($orders);
        return view('admin.properties.property-listing',['properties' => $properties]);
    }

    /*
    *   Name : addProperty()
    *   Purpose : Load Add Property view
    */
    public function addProperty() {
        return view('admin.properties.add-property');
    }


    /*
    * Name : submitProperty()
    * Purpose : Submit Property details
    */
    public function submitProperty(Request $request) {
        $request->validate([
            'name' => ['required', 'string'],
            'property_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        //dd($request->all());
        $property = new PropertyType;
        $property->fill($request->all());

        // Upload Image
        if($request->file('property_image')) {
            $image = $request->file('property_image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $path = '/uploads/properties/';
            $destinationPath = public_path($path);
            $image->move($destinationPath, $input['imagename']);
            $property->image = $path.$input['imagename'];
        }
        $property->save();
        Session::flash('message','Property created successfully');
        return redirect('/admin/properties');
    }

    /*
    NAme = editProperty()
    Purpose = Load Edit Property view
    */
    public function editProperty($id){
        $property = PropertyType::find($id);
        return view('admin.properties.edit-property',['property' => $property]);
    }

    /*
    NAme = updateProperty()
    Purpose = Update Property Details
    */
    public function updateProperty(Request $request){

        $request->validate([
            'name' => ['required', 'string'],
        ]);
        $property = PropertyType::find($request->property_id);
        $property->fill($request->all());

        // Upload Image
        if($request->file('property_image')) {
            $pImages = PropertyType::find($request->property_id);
            $propertyImage = public_path().$pImages->image; // get previous image from folder

            if (File::exists($propertyImage)) {
                File::delete($propertyImage);
            }
            $image = $request->file('property_image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $path = '/uploads/properties/';
            $destinationPath = public_path($path);
            $image->move($destinationPath, $input['imagename']);
            $property->image = $path.$input['imagename'];
        }
        $property->save();
        Session::flash('message','Property update successfully');
        return redirect('/admin/properties');
    }

    /*
    Name = propertyUpdateStatus()
    Purpose = Update Property Status
    */
    public function propertyUpdateStatus(Request $request){
        $status = $request->value;
        $id = $request->target;
        $res = PropertyType::where('id', $id)->update(['status' => $status,'updated_at' => Carbon::now()]);
        if($res){
            return response()->json(['status' => 200, 'msg' => 'Status updated successfully']);
        } else{
            return response()->json(['status' => 500, 'msg' => 'Failed to update status']);
        }
    }


    public function deleteProperty($id) {
        $property = PropertyType::find($id);
        $propertyImage = public_path().$property->image;
        if(File::exists($propertyImage)){  // check if the image indeed exists
          File::delete($propertyImage);
        }

        if(count((array)$property)){
            PropertyType::where('id', $id)->delete();
            return response()->json(['status' => 200, 'msg' => 'Property deleted successfully']);
        } else {
            return response()->json(['status' => 500, 'msg' => 'Failed to delete property']);
        }
    }

    /* ========================= Quotations =============================*/

    /*
    *   Name : getQuotationsListing()
    *   Purpose : get all quotation listing
    */
    public function getQuotationsListing() {
        $quotations = Quotation::with(['images','user'])->orderBy('id', 'DESC')->get();
        return view('admin.quotations.quotation-listing',['quotations' => $quotations]);
    }

    /*
    NAme = editQuotation()
    Purpose = Load Edit User view
    */
    public function editQuotation($id){
        $quotation = Quotation::with(['images','user'])->find($id);
        //dd($quotation);
        return view('admin.quotations.edit-quotation',['quotation' => $quotation]);
    }


    /*
    NAme = ViewQuotation()
    Purpose = Load View Quotation Request View
    */
    public function viewQuotation($id){
        $quotation = Quotation::with(['images','user'])->find($id);
        return view('admin.quotations.view-quotation',['quotation' => $quotation]);
    }

    public function createQuotationInvoice($id) {
        $quotation = Quotation::with(['images','user'])->find($id);
        return view('admin.quotations.create-quotation',['quotation' => $quotation]);
    }

    public function submitQuotationInvoice(Request $request) {
        //dd($request->all());
        $data = array();
        $data = $request->only(['quotation_id', 'total','estimated_to','estimated_from']);
        $data['total'] = str_replace('£', '', $request->total);
        $order = InvoiceOrder::firstOrNew(['quotation_id' => $request->quotation_id]);
        $order->fill($data);
        $order->items()->delete();
        $order->invoiceOrderDetails()->delete();
        $order->save();
        
        foreach ($request->items as $item) {
            $order->items()->save(new \App\InvoiceOrderItem($item));
        }
        
        foreach ($request->details as $detail) {
            $order->invoiceOrderDetails()->save(new \App\InvoiceOrderDetail($detail));
        }
        Quotation::where('id', $request->quotation_id)->update(['status' => $request->status,'updated_at' => Carbon::now()]);
        Session::flash('message','Quotation invoice generated successfully');
        return redirect('/admin/quotations');
    }

    // public function createQuotationItems(Request $request) {

    //     $request->validate([
    //         'item.*.quantity' => 'required|numeric',
    //         'item.*.item' => 'required',
    //     ]);

    //     $order = InvoiceOrder::find(['quotation_id' => $request->quotation_id]);
    //     foreach ($request->item as $item) {
    //        // dd($item);
    //         $order->items()->save(new \App\InvoiceOrderItem($item));
    //     }

    //     Quotation::where('id', $request->quotation_id)->update(['status' => $request->status,'updated_at' => Carbon::now()]);

    //      return Redirect()->back()->with(['message' => 'Quotation Items generated']);
    // }


    /*======================= Inspection Request ====================*/

    /*
    *   Name : inspectionRequestListing()
    *   Purpose : get all Inspection Request listing
    */
    public function inspectionRequestListing() {
        $inspections = InspectionRequest::with(['invoiceOrder.quotationInvoice' => function($q){ $q->with(['user']); }])->orderBy('id', 'DESC')->get();
        //dd($inspections);
        return view('admin.inspections.inspection-listing',['inspections' => $inspections]);
    }

    /*
    NAme = editInspectionRequest()
    Purpose = Load Edit Inspection Request view
    */
    public function editInspectionRequest($id){
        //$inspection = InspectionRequest::with(['invoiceOrder.quotationInvoice' => function($q){ $q->with(['user']); }])->where('id',$id)->orderBy('id', 'DESC')->get();
        
        $inspection = InspectionRequest::with(['invoiceOrder' => function($q){ 
            $q->with([
                'quotationInvoice' => function($q) { 
                    $q->with(['user','images']);
                }
            ]);
        }])->where('id',$id)->orderBy('id', 'DESC')->get();
        
        //dd($inspection);
        return view('admin.orders.edit-inspection-request',['inspection' => $inspection]);
    }


    /*
    NAme = editInspectionRequest()
    Purpose = Load Edit Inspection Request view
    */
    public function viewPendingInspectionRequest($id){
        //$inspection = InspectionRequest::with(['invoiceOrder.quotationInvoice' => function($q){ $q->with(['user']); }])->where('id',$id)->orderBy('id', 'DESC')->get();
        
        $inspection = InspectionRequest::with(['payment','invoiceOrder' => function($q){ 
            $q->with([
                'invoiceOrderDetails' => function($q) { 
                }
            ]);
            $q->with([
                'items' => function($q) { 
                }
            ]);
            $q->with([
                'quotationInvoice' => function($q) { 
                    $q->with(['user','images']);
                }
            ]);
        }])->where('id',$id)->orderBy('id', 'DESC')->get();
        
        //dd($inspection);
        return view('admin.orders.view-order-details',['inspection' => $inspection]);
    }

    public function generateOrderRequest(Request $request){
        //dd($request->all());
        $data = array();
        $data = $request->only(['inspection_payment_id', 'service_charges','sub_total','total','estimated_to','estimated_from']);
        $data['sub_total'] = str_replace('£', '', $request->sub_total);
        $order = Order::firstOrNew(['inspection_payment_id' => $request->inspection_payment_id]);
        $order->fill($data);
        $order->items()->delete();
        $order->orderInvoice()->delete();
        
        $order->save();
        
        foreach ($request->items as $item) {
            $order->items()->save(new \App\OrderItem($item));
        }
        
        foreach ($request->details as $detail) {
            $order->orderInvoice()->save(new \App\OrderDetail($detail));
        }
        Order::where('id', $request->inspection_payment_id)->update(['status' => $request->status,'updated_at' => Carbon::now()]);
        Session::flash('message','Order generated successfully');
        return redirect('/admin/invoice-orders');
    }

    /*
    NAme = updateInspection()
    Purpose = Update Property Details
    */
    public function updateInspection(Request $request){
        //dd($request->all());
        $request->validate([
            'amount' => 'required',
            'description' => 'required'
        ]);

        $inspection = InspectionRequest::find($request->inspection_id);
        $inspection->fill($request->all());
        $inspection->save();
        return response()->json(['success'=>'Inspection request updated successfully']);
        // Session::flash('message','Inspection request updated successfully');
        // return redirect('/admin/inspection-requests');
    }

    /*
    Name = propertyUpdateStatus()
    Purpose = Update Property Status
    */
    public function inspectionUpdateStatus(Request $request){
        $status = $request->value;
        $id = $request->target;
        $res = InspectionRequest::where('id', $id)->update(['status' => $status,'updated_at' => Carbon::now()]);
        if($res){
            return response()->json(['status' => 200, 'msg' => 'Status updated successfully']);
        } else{
            return response()->json(['status' => 500, 'msg' => 'Failed to update status']);
        }
    }



    /* ============================ Inspection Payment ========================== */

    /*
    *   Name : getPaymentListing()
    *   Purpose : get all Payment listing
    */
    public function getInspectionPaymentListing() {
        $payments = InspectionPayment::orderBy('id', 'DESC')->get();
        return view('admin.inspection-payments.payment-listing',['payments' => $payments]);
    }

    /* ============================ Page Data ========================== */

    /*
    *   Name : getPaymentListing()
    *   Purpose : get all Payment listing
    */
    public function getPageData() {
        $pages = Page::orderBy('id', 'DESC')->get();
        return view('admin.pages.page-listing',['pages' => $pages]);
    }

    /*
    NAme = editPage()
    Purpose = Load Edit Page view
    */
    public function editPage($id){
        $page = Page::find($id);
        return view('admin.pages.edit-page',['page' => $page]);
    }

    /*
    NAme = updatePage()
    Purpose = Update Page Details
    */
    public function updatePage(Request $request){

        $request->validate([
            'title' => ['required', 'string'],
            'title' => ['required'],
        ]);
        $page = Page::find($request->page_id);
        $page->fill($request->all());

        // Upload Image
        if($request->file('page_image')) {
            $images = Page::find($request->page_id);
            $pageImage = public_path().$images->image; // get previous image from folder

            if (File::exists($pageImage)) {
                File::delete($pageImage);
            }
            $image = $request->file('page_image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $path = '/uploads/pages/';
            $destinationPath = public_path($path);
            $image->move($destinationPath, $input['imagename']);
            $page->image = $path.$input['imagename'];
        }
        $page->save();
        Session::flash('message','Page update successfully');
        return redirect('/admin/page');
    }



    /* ========================= Orders =============================*/

    /*
    *   Name : getOrderListing()
    *   Purpose : get all order listing
    */
    public function getOrdersListing() {
        //$orders = Order::with(['quotationInvoice' => function($q){ $q->with(['user' => function($q){ $q->select('id', 'first_name', 'last_name', 'image', 'email'); }]); }])->orderBy('id', 'DESC')->get();
        $inspections = InspectionRequest::with(['invoiceOrder.quotationInvoice' => function($q){ $q->with(['user']); }])->where('status',"0")->orderBy('id', 'DESC')->get();
        $pendingInspections = InspectionRequest::with(['invoiceOrder.quotationInvoice' => function($q){ $q->with(['user']); }])->where([['status','=',"1"],['is_paid','=', "1"]])->orderBy('id', 'DESC')->get();
        $orders =  Order::with(['items','orderInvoice','InspectionPayment'=> function($q){ 
            $q->with(['inspectionRequest' => function($q){ 
                $q->with(['invoiceOrder.quotationInvoice.user']); }]); } ])->where('status',"1")->orderBy('id', 'DESC')->get();
        // $orders = InvoiceOrder::with(['quotationInvoice' => function($q){ $q->with(['user']); }])->orderBy('id', 'DESC')->get();
        //dd($orders);
        return view('admin.orders.order-listing',['orders' => $orders,'inspections' => $inspections, 'pendingInspections' => $pendingInspections]);
    }


    /* ================================ Coupons ==============================*/

    public function generateCoupons($count, $length = 6)
    {
        $coupons = [];
        while(count($coupons) < $count) {
            do {
                $coupon = strtoupper(str_random($length));
            } while (in_array($coupon, $coupons));
            $coupons[] = $coupon;
        }
        $existing = Coupen::whereIn('code', $coupons)->count();
        if ($existing > 0)
            $coupons += $this->generateCoupons($existing, $length);
        return (count($coupons) == 1) ? $coupons[0] : $coupons;
    }


    /*
    *   Name : getCouponListing()
    *   Purpose : get all Coupon listing
    */
    public function getCouponListing() {
        /*$orders = Order::with(['quotationInvoice' => function($q){ $q->with(['user' => function($q){ $q->select('id', 'first_name', 'last_name', 'image', 'email'); }]); }])->orderBy('id', 'DESC')->get();*/
        $coupons = Coupon::orderBy('id', 'DESC')->get();
        //dd($orders);
        return view('admin.coupons.coupon-listing',['coupons' => $coupons]);
    }

    /*
    *   Name : addCoupon()
    *   Purpose : Load Add Coupen view
    */
    public function addCoupon() {
        return view('admin.coupons.add-coupon');
    }


    /*
    * Name : submitCoupon()
    * Purpose : Submit Coupon details
    */
    public function submitCoupon(Request $request) {
        $request->validate([
            'title' => ['required', 'string'],
            'code' => ['required', 'string', 'unique:coupons'],
            'description' => 'required',
            'discount_amount' => 'required',
            'max_uses' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        //dd($request->all());
        $coupon = new Coupon;
        $coupon->fill($request->all());
        $coupon->save();
        Session::flash('message','Coupon created successfully');
        return redirect('/admin/coupons');
    }

    /*
    NAme = editCoupon()
    Purpose = Load Edit Coupon view
    */
    public function editCoupon($id){
        $coupon = Coupon::find($id);
        //dd($quotation);
        return view('admin.coupons.edit-coupon',['coupon' => $coupon]);
    }

    /*
    NAme = updateCoupon()
    Purpose = Update Coupon Details
    */
    public function updateCoupon(Request $request){

        $request->validate([
            'title' => ['required', 'string'],
            'description' => 'required',
            'discount_amount' => 'required',
            'max_uses' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        //dd($request->all());
        $coupon_id = $request->coupon_id;
        $coupon = Coupon::find($coupon_id);
        $coupon->fill($request->all());
        $coupon->save();
        Session::flash('message','Coupon update successfully');
        return redirect('/admin/coupons');
    }

    /*
    Name = categoryUpdateStatus()
    Purpose = Update Category Status
    */
    public function couponUpdateStatus(Request $request){
        $status = $request->value;
        $id = $request->target;
        //echo "status : ".$status; echo "<br/>Id : ".$id; exit;
        $res = Coupon::where('id', $id)->update(['status' => $status,'updated_at' => Carbon::now('Canada/Mountain')]);
        if($res){
            return response()->json(['status' => 200, 'msg' => 'Status updated successfully']);
        } else{
            return response()->json(['status' => 500, 'msg' => 'Failed to update status']);
        }
    }

    /*
    Name = deleteCoupon()
    Purpose = Delete Coupon
    */
    public function deleteCoupon($id) {
        $coupon = Coupon::find($id);
        //print_r(count((array)$category)); exit;
        if($coupon){

            return response()->json(['status' => 200, 'msg' => 'Coupon deleted successfully']);
        } else {
            return response()->json(['status' => 401, 'msg' => 'Failed to delete Coupon']);
        }

    }



    public function updateAmount(Request $request){
        DB::table('inspection_amount')
            ->where('id', $request->id)
            ->update(['amount' => $request->amount]);
        return response()->json(['success'=>'Inspection Amopunt Updated Successfully']);
    }




}
