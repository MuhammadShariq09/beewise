<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvoiceOrder;
use App\InvoiceOrderDetail;
use App\InvoiceOrderItem;
use App\Quotation;
use App\QuotationImage;
use App\Http\Resources\InvoiceOrderResource;

class InvoiceOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InvoiceOrderResource::collection(InvoiceOrder::with(['quotationInvoice','items','invoiceOrderDetails'])->whereHas('quotationInvoice', function ($q)
        {
            $q->where('user_id', request()->user()->id);
        })->paginate(10));
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return new InvoiceOrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {
        //
    }
*/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = InvoiceOrder::find($id);
        if($invoice) {
            Quotation::find($invoice->quotation_id)->update(['status' => "2"]);
        
            InvoiceOrder::find($id)->update(['status' => "2"]);
            return response()->json(['error' => false, 'message' => 'Request deleted successfully']);
        } else {
           return response()->json(['error' => true, 'message' => 'Failed to delete request']);
        }
    }
}
