<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PaypalModel;
use DB;
use Session;
use Redirect;
/*use App\Mail\PaidOrderMail;
*/use Helper;
/*use Mail;
use \Illuminate\Support\Facades\Notification;
use App\Notifications\MessageNotification;
use App\Notifications\TaxIntegrityNotification;*/
use App\Order;
use App\User;
use Auth;

class PaypalPaymentController extends Controller
{

    public function index(Request $request){

      $card = trim(preg_replace('/\s+/', '', $request->cardNumber));
      if($request->cardNumber == ''){
          return response()->json(['status'=>500, 'msg'=>'Card Number is invalid']);
          //Session::flash('error','Card Number is invalid');
          return redirect()->back();
      }
      if($request->cardExpiry == ''){
        return response()->json(['status'=>500, 'msg'=>'Card Expiry is invalid']);
        //Session::flash('error','Card Expiry is invalid');
        return redirect()->back();
      }
      if($request->cardCVC == ''){
        return response()->json(['status'=>500, 'msg'=>'Card CVC is invalid']);
        //Session::flash('error','Card CVC is invalid');
        return redirect()->back();
      }
      if(strlen($card) != '16'){
        return response()->json(['status'=>500, 'msg'=>'Card is invalid']);
        //Session::flash('error','Card is invalid');
        return redirect()->back();
      }
      if(strlen($request->cardExpiry) != '9'){
        return response()->json(['status'=>500, 'msg'=>'Card Expiry is invalid']);
        //Session::flash('error','Card Expiry is invalid');
        return redirect()->back();
      }
      if(strlen($request->cardCVC) != '3'){
        return response()->json(['status'=>500, 'msg'=>'Card CVC is invalid']);
        //Session::flash('error','Card CVC is invalid');
        return redirect()->back();
      }
      if($request->oi == '' || !$request->oi){
        return response()->json(['status'=>500, 'msg'=>'Transaction can not be performed']);
        //Session::flash('error','Transaction can not be performed');
        return redirect()->back();
      }


      $inv = DB::table('invoice')->where('order_id',$request->oi)->get();
      $amount = 0;
      foreach ($inv as $key => $value) {
        $amount += $value->price;
      }

      $cardNumber = trim(preg_replace('/\s+/', '', $card));
      $cardExpiry = trim(preg_replace('/\s+/', '', $request->cardExpiry));
      $cardCVC = trim(preg_replace('/\s+/', '', $request->cardCVC));
      $exp = explode('/',$cardExpiry);
      $month = trim($exp[0]);
      $year = trim($exp[1]);
      /*
        dummy card
        'creditCardNumber' => '4080 7124 5906 1839',
        'expMonth' => '10',
        'expYear' => '2026',
        'cvv' => '561',
      */
        $paypalParams = array(
            'paymentAction' => 'Sale',
            'amount' => $amount,
            'currencyCode' => 'USD',
            'creditCardType' => '',
            'creditCardNumber' => $cardNumber,
            'expMonth' => $month,
            'expYear' => $year,
            'cvv' => $cardCVC,
            'firstName' => "",
            'lastName' => "",
            'city' => "",
            'zip'  => "",
            'countryCode' => "",
        );
        $paypal_model = new PaypalModel();
        $response = $paypal_model->paypalCall($paypalParams);

        if($response['ACK'] == "Failure"){
          //Session::flash('error', $response['L_LONGMESSAGE0']);
          //Session::flash('error1', @$response['L_LONGMESSAGE1']);
          $e = ($response['L_LONGMESSAGE0']) ? @$response['L_LONGMESSAGE0'] : @$response['L_LONGMESSAGE1'];
          return response()->json(['status'=>500, 'msg'=>$e]);
          return redirect()->back();
        }
        else  if($response['ACK'] == "Success"){
          DB::table('order_status')->where('order_id',$request->oi)->update(['isPaid' => 1]);
          //Session::flash('message', "Payment is successfully proccessed");

          $o = DB::table('orders')->where('id',$request->oi)->get();
          $ud = Helper::getUserDetails($o[0]->user_id);
          Mail::to($ud->email)->send(new PaidOrderMail($request->oi));
          $admin = User::where('is_admin',1)->get();
          foreach ($admin as $a) {
              Mail::to($a->email)->send(new PaidOrderMail($request->oi, 1));
          }

          $url = url('order/detail/'.$request->oi);
          $obj = array(
                'url' => $url,
                'title' => Auth::user()->name.' has paid for an order #'.$request->oi,
                'by' => Auth::user()->name 
            ); 
          Notification::send($admin, new TaxIntegrityNotification($obj));



          return response()->json(['status'=>200, 'msg'=>"Payment is successfully proccessed. Will Reload in 3 Seconds"]);
          return redirect()->back();

        }
        else{
          return response()->json(['status'=>500, 'msg'=>"Transaction could not be performed !"]);
          //Session::flash('error', "Transaction could not be performed !");
          return redirect()->back();
        }
    }
}
