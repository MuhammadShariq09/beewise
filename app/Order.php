<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'inspection_payment_id', 'service_charges', 'sub_total', 'total', 'discount_amount', 'status', 'is_paid'
    ];
    
    public function InspectionPayment()
    {
      return $this->belongsTo(InspectionPayment::class, 'inspection_payment_id', 'id');
    }
    
    public function orderInvoice() {
    	return $this->hasMany(OrderDetail::class);
    }
    
    public function items() {
    	return $this->hasMany(OrderItem::class);
    }
    
    public function payment() {
        return $this->hasOne(Payment::class);
    }
}
