<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceOrderItem extends Model
{
    protected $fillable = [
        'invoice_order_id', 'item', 'quantity','price'
    ];

    // protected $guarded = [];


    public function invoiceOrder()
    {
      return $this->belongsTo(InvoiceOrder::class);
    }
}