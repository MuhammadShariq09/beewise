<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'title', 'code', 'type', 'discount_amount', 'description', 'max_uses', 'start_date', 'end_date', 'status'
    ];
    
    protected $casts = [
        'start_date'  => 'date:d F Y',
        'end_date' => 'datetime:d F Y',
    ];

    public function vouchers()
    {
      return $this->hasMany(UserCoupon::class);
    }

}
