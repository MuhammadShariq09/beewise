<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserStatusMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $first_name;
    private $last_name;
    private $email;
    public $status;
    public $statusTxt;
    public $subject = "Your Beewise Move account has been ";

    public function __construct($first_name = null, $last_name = null, $email = null, $status = null, $url = null)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->status = $status;
        if($this->status == "0"){
            $this->statusTxt = "Deactivated";
        } else {
            $this->statusTxt = "Activated";
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user-activation')->subject($this->subject.' '.$this->statusTxt)->with([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'status' => $this->status,
            'base_url' => env('APP_URL')
            ]);
    }
}
