<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyNewUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $first_name;
    private $last_name;
    private $email;
    public $subject = "New user has been added on AirCraft Work Job Portal";

    public function __construct()
    {
        $this->first_name = request('first_name');
        $this->last_name = request('last_name');
        $this->email = request('email');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.newuser-notify')->subject($this->subject)->with([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'base_url' => env('APP_URL')
            ]);
    }
}
