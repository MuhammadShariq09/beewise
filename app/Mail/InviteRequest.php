<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $first_name;
    private $last_name;
    private $email;
    private $password;
    private $owner_first_name;
    private $owner_last_name;
    public $subject;

    public function __construct($first_name = null, $last_name = null, $email = null, $password = null, $owner_first_name = null, $owner_last_name = null)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->password = $password;
        $this->owner_first_name = $owner_first_name;
        $this->owner_last_name = $owner_last_name;   
        $this->subject = "Invite you to build profile on ".env('APP_NAME');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.invite')->subject($this->subject)->with([
                    'first_name' => $this->first_name,
                    'last_name' => $this->last_name,
                    'email' => $this->email,
                    'password' => $this->password,
                    'owner_first_name' => $this->owner_first_name,
                    'owner_last_name' => $this->owner_last_name,
                    'base_url' => env('APP_URL')
                    ]);
    }
}
