<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationImage extends Model
{
    protected $fillable = [
        'quotation_id','image'
    ];

    public function quotation()
    {
      return $this->belongsTo(Quotation::class);
    }
}
