<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    protected $fillable = [
        'name','image','status'
    ];

    public function quotationProperties()
    {
      return $this->hasMany(Quotation::class);
    }
}
