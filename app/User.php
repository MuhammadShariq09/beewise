<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','image', 'email', 'password','phone', 'address', 'city', 'state', 'postal_code', 'country', 'is_active', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function enable()
    {
        $this->is_active = "1";
    }

    public function disable()
    {
        $this->is_active = "0";
    }

    public function toggleActivation()
    {
        if($this->is_active)
            $this->disable();
        else
            $this->enable();
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function quotation()
    {
      return $this->hasMany(Quotation::class);
    }

    public function userCoupen()
    {
      return $this->hasMany(UserCoupen::class);
    }

    public function userPayment()
    {
      return $this->hasMany(Payment::class);
    }


}
