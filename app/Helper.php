<?php
namespace App;
use DB;
use Session;
use DateTime;
use DateTimeZone;
use Auth;

class Helper {

	public static function getQuotationInvoice($quotation_id) {
        $whereData = [
            ['quotation_id', $quotation_id]
        ];
        $orders = InvoiceOrder::where($whereData)->get();
        //print_r($orders); exit;
        return $orders;
    }
    
    public static function getQuotationItems($quotation_id) {
        $whereData = [
            ['quotation_id', $quotation_id]
        ];
        $orders = InvoiceOrder::whereHas('items')->with(['items'])->where($whereData)->get();
        // echo "<pre>";print_r($orders); exit;
        //dd($orders);
        return $orders;
    }

    public static function getQuotationDetails($quotation_id) {
        $whereData = [
            ['quotation_id', $quotation_id]
        ];
        $orders = InvoiceOrder::whereHas('invoiceOrderDetails')->with(['invoiceOrderDetails'])->where($whereData)->get();
        //print_r($orders); exit;
        return $orders;
    }

    public static function getInspectionAmount(){
        $amount = DB::table('inspection_amount')->get();
        return $amount;
    }
}