<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InspectionPayment extends Model
{
    protected $fillable = [
        'user_id', 'inspection_request_id', 'amount', 'transaction_id'
    ];


    public function invoiceOrder()
    {
      return $this->belongsTo(InvoiceOrder::class);
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }
    
    public function inspectionRequest() {
        return $this->belongsTo(InspectionRequest::class);
    }
    
    public function finalOrder()
    {
      return $this->hasOne(Order::class);
    }

}
