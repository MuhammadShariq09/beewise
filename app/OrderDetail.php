<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id', 'item', 'quantity', 'price'
    ];
    
    public function order()
    {
      return $this->belongsTo(Order::class);
    }
}
