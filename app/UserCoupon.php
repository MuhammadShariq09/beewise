<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCoupon extends Model
{
    protected $fillable = [
        'user_id', 'coupon_id', 'redeemed_at'
    ];

    public function users()
    {
      return $this->belongsTo(User::class,'user_id', 'id');
    }
    
    
    public function coupens()
    {
      return $this->belongsTo(Coupen::class,'coupon_id', 'id');
    }
}