<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceOrder extends Model
{
    protected $fillable = [
        'quotation_id', 'total', 'estimated_to','estimated_from', 'status'
    ];


    public function quotationInvoice()
    {
      return $this->belongsTo(Quotation::class, 'quotation_id', 'id');
    }

    public function invoiceOrderDetails() {
    	return $this->hasMany(InvoiceOrderDetail::class);
    }
    
    public function items() {
    	return $this->hasMany(InvoiceOrderItem::class);
    }

    public function inspectionPayment() {
        return $this->hasOne(InspectionPayment::class);
    }
    
    public function inspection() {
       return $this->hasOne('App\InspectionRequest');
    }
}
