<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InspectionRequest extends Model
{
    protected $fillable = [
        'invoice_order_id', 'date', 'time', 'description', 'amount', 'status', 'is_paid'
    ];
    
    
    public function invoiceOrder()
    {
      return $this->belongsTo(InvoiceOrder::class);
    }
    
    public function payment() {
        return $this->hasOne(InspectionPayment::class);
    }
}
