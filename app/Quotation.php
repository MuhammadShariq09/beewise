<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{

    protected $fillable = [
        'order_num','user_id', 'address_from', 'state_from', 'city_from', 'country_from', 'zipcode_from', 'property_type_from', 'floors_from', 'rooms_from', 'address_to', 'state_to', 'city_to', 'country_to', 'zipcode_to', 'property_type_to', 'floors_to', 'moving_date', 'packaging_items', 'fragile_items', 'note', 'status'
    ];


    public function user()
    {
      return $this->belongsTo(User::class,'user_id', 'id');
    }

    public function images()
    {
      return $this->hasMany(QuotationImage::class);
    }

    public function quotationOrder()
    {
      return $this->hasOne(InvoiceOrder::class);
    }

    public function propertiesTo()
    {
      return $this->belongsTo(PropertyType::class);
    }

    public function propertiesFrom()
    {
      return $this->belongsTo(PropertyType::class);
    }
}
